<?php

function getColor($which) {

    $swa = simplexml_load_file($which);
	
	$getColor = array();
	
	foreach($swa as $sw){
	
		$kol = $sw['Color'];
		
		array_push($getColor, $kol);
		
	}
	return $getColor;

}

function hsv2rgb($hsv){

        $H = $hsv[0];
        $S = $hsv[1];
        $V = $hsv[2];

        // HSV values = 0 Ã· 1
        if ($S == 0) {
            $R = $V * 255;
            $G = $V * 255;
            $B = $V * 255;
        } else {
            $var_h = $H * 6;
            // H must be < 1
            if ( $var_h == 6 ) {
                $var_h = 0;
            }
            // Or ... $var_i = floor( $var_h )
            $var_i = floor( $var_h );
            $var_1 = $V * ( 1 - $S );
            $var_2 = $V * ( 1 - $S * ( $var_h - $var_i ) );
            $var_3 = $V * ( 1 - $S * ( 1 - ( $var_h - $var_i ) ) );

            switch($var_i) {
                case 0:
                    $var_r = $V;
                    $var_g = $var_3;
                    $var_b = $var_1;
                    break;
                case 1:
                    $var_r = $var_2;
                    $var_g = $V;
                    $var_b = $var_1;
                    break;
                case 2:
                    $var_r = $var_1;
                    $var_g = $V;
                    $var_b = $var_3;
                    break;
                case 3:
                    $var_r = $var_1;
                    $var_g = $var_2;
                    $var_b = $V;
                    break;
                case 4:
                    $var_r = $var_3;
                    $var_g = $var_1;
                    $var_b = $V;
                    break;
                default:
                    $var_r = $V;
                    $var_g = $var_1;
                    $var_b = $var_2;
            }

            //RGB results = 0 Ã· 255
            $R = $var_r * 255;
            $G = $var_g * 255;
            $B = $var_b * 255;
        }

        return array( 'r' => $R, 'g' => $G, 'b' => $B );
}

$palette = array('palette.orig.hsv.xml');

foreach($palette as $pal){

	$color = getColor($pal);
	
	for($i = 0; $i < count($color); $i++){
		
		$col = $color[$i];
			
		$rgb = hsv2rgb($col);
		
		$rgbStr = $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'];
		
		$output = ' ' .$rgbStr. ' ';
		
		 echo $output;	
	}
	
}