<?php
/* settings */
$texturesDir = 'assets/textures/';

/* params */
$s = (isset($_GET['s'])) ? $_GET['s'] : 'ruffntuff';
$sub = '';

/* target dir */
switch ($s) {

    case 'amourcoat' : $sub = 'amourcoat';
    break;

    case 'creations' : $sub = 'creations';
    break;

    case 'marbles' : $sub = 'marbles';
    break;

    case 'metallica' : $sub = 'metallica';
    break;

    case 'quartz' : $sub = 'quartz';
    break;

    case 'ruffntuff' : $sub = 'ruffntuff';
    break;

    case 'variations' : $sub = 'variations';
    break;

    default : echo '404 - Not Found'; exit;
    break;

}
$dir = $texturesDir . $sub . '/';
$thumb = $texturesDir . 'thumb-' . $sub . '/';

if ($handle = opendir($dir)) {

    while (false !== ($entry = readdir($handle))) {

        if ($entry != "." && $entry != "..") {

            echo "<div class='box-t'><a href='#'><img src='$thumb" . "$entry' data-src='$dir" . "$entry'></a></div>";

        }

    }

    closedir($handle);

}