<?php
require_once 'src/Google_Client.php'; 

require_once 'src/contrib/Google_PlusService.php';

require_once 'src/contrib/Google_Oauth2Service.php';

@session_start();

$client = new Google_Client();

$client->setApplicationName("Crown Color Visualiser"); 

$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me')); 

$client->setClientId('327993140559.apps.googleusercontent.com'); 

$client->setClientSecret('opUPN50E4W14oPFkYS2r1V_v'); 

$client->setRedirectUri('http://www.crownit.co.ke/glog.php'); 

$client->setDeveloperKey('AIzaSyDxPGk0Y8LrCRhZ31-QrnFsit_BA3vNZLQ'); 

$plus 		= new Google_PlusService($client);

$oauth2 	= new Google_Oauth2Service($client); 

if(isset($_GET['code'])) {
	$client->authenticate(); // Authenticate
	$_SESSION['access_token'] = $client->getAccessToken();
	
}

if(isset($_SESSION['access_token'])) {

	$client->setAccessToken($_SESSION['access_token']);
}

if ($client->getAccessToken()) {
	$user 		 = $oauth2->userinfo->get();
	$me 		 = $plus->people->get('me');
	$optParams 	 = array('maxResults' => 100);
	$activities  = $plus->activities->listActivities('me', 'public',$optParams);
	$_SESSION['access_token'] 		= $client->getAccessToken();
	$email 							= filter_var($user['email'], FILTER_SANITIZE_EMAIL); // get the USER EMAIL ADDRESS using OAuth2
} else {
	$authUrl = $client->createAuthUrl();
}
	
?>