<?php

    @session_start();
    $logged_in = (isset($_SESSION['id'])) ? 'true' : 'false' ;

    $imageDir = 'assets/';
    $req = (isset($_REQUEST['p'])) ? $_REQUEST['p'] : 'living-room-01.png';

    $image = $imageDir . $req;

?>
<?php
	include 'includes/dao/config.php';
	
	include 'google.php';
	
	if (array_key_exists("login",$_GET)) {

    $fboauth = $_GET['op'];

    if ($fboauth == 'facebook') {

        echo ("<script type='text/javascript'>window.location.href='facebook/example/login-facebook.php';</script>");

    }

}
?>	
<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
<title>Crown Color Visualizer</title>

<meta name="author" content="Francis Muturi, Simon Ngunjiri Muraya"/>
<meta name="copyright" content="Copyright - 2015"/>
<meta name="keywords" content="Crown paints, crown color visualizer"/>

<meta property="og:title" content="Crown Color Visualiser">
<meta property="og:image"  content="http://www.crownit.co.ke/assets/<?php echo $req ?>" />
<meta property="og:url"    content="http://www.crownnit.co.ke/ccv.php?p=<?php echo $req ?>" />
<meta property="og:description"  content="Crown Color Visualizer">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link rel="stylesheet" href="css/jquery.jscrollpane.css">
<link rel="stylesheet" href="css/miniTip.min.css">
<link href="style.css" media="screen" rel="stylesheet">
<!-- Bootstrap css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/ccv-screen.css" media="screen" rel="stylesheet">
<link href="style.css" media="screen" rel="stylesheet">
<link href="screen.css" media="screen" rel="stylesheet">
<link href="css/introjs.css" media="screen" rel="stylesheet">

<!-- custom CSS -->
<link rel="stylesheet" href="custom.css">

<!-- favicon.ico and apple-touch-icon.png -->
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>
if (!window.jQuery) {
    document.write('<script src="js/libs/jquery.min.js"><\/script>');
}
</script>
<!-- CCV scripts -->
<script type="text/javascript" src="js/jquery.miniTip.min.js"></script>
<script type="text/javascript" src="js/jquery.jscrollpane.min.js" ></script>
<script type="text/javascript" src="js/jquery.mousewheel.js" ></script>
<script type="text/javascript" src="js/jquery.cookie.js" ></script>
<script type="text/javascript" src="js/kinetic-v4.6.0.min.js"></script>
<script type="text/javascript" src="js/helpers.color.js" ></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- typeahead -->
<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
<!-- JsPDF -->
<script type="text/javascript" src="js/jspdf.min.js"></script>
<!-- Intro JS -->
<script type="text/javascript" src="js/intro.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript">

$(document).ready( function() {

    $.removeCookie('ccvtut');

    /** App settings **/
	var settings = {
    	app : { panel : $('.panel'),indicator : $('.indicator'),boxa: $('a.box'),boxi: $('.box img'),box: $('.box'),dropper: $('#dropper'),debug: '',palletteSelect: $('#charttypes'),baseURL: '',palletteScript : 'data.php',dataDir: 'points',polyDir: 'polys',image: <?php echo "'" . $image . "'"; ?>,username: <?php echo @(isset($_SESSION["username"])) ? '"' . $_SESSION['username'] . '"' : '""' ; ?> },
    	jscrollpane : { showArrows: true,autoReinitialise: true,autoReinitialiseDelay: 100,animateScroll: true,animateDuration: 300,animateEase: 'linear' },
    	kinetic : { container: 'app-canvas',width: 740,height: 500,listening: true }
    };

    /** Initialise the app **/
    app.initialise(settings);

    /** search autocomplete **/
    $('.search').autoComplete({ minChars: 1,source: function(term, response) { try { xhr.abort(); } catch(e) { } var xhr = $.getJSON('walk.php',{ q: term},
        function(data) { response(data); }); 
    },delay: 1, cache: 0, renderItem: function(item, search) { var name = item.name; var code = item.code;
        var ret = '<a href="#" id="box" class="box" data-name="' + name + '" data-code="' + code + '" style="background-color: rgb(' + item.rgb + '); color: black !important; text-shadow: 1px 0px 2px white !important;">' + name + ' ' + code + '</a>';
        return ret;
        }
    });

    $('#login').click( function(){ loadPopupBox(); });
    $('#reges').click( function(){ loadPopupBox(); });
    $('#gallery').click( function(){ loadPopupBox(); });
    $('#ovv').click( function() { loadPopupBox(); });
    $('#prov').click( function() { loadPopupBox(); });

    function unloadPopupBox() { $('#popup_box').fadeOut("slow"); $(".body-wrapper").css({ "opacity": "1" }); }
    function loadPopupBox() { $('#popup_box').fadeIn("slow"); $(".body-wrapper").css({ "z-index": "0.4" , "filter" : "alpha(opacity=60)", "z-index" : "5" }); }
    function loginDialog() { $('.modal-title').html('Login'); $('.modal-body').html('<div style="width:60%; margin: 0 auto;"><form class="form-horizontal" method="post" action="includes/scripts/crownlogin.php"><div class="control-group"><div class="controls"><input id="username" name="username" type="text" placeholder="username" class="input-xlarge" required=""></div></div><div class="control-group"><div class="controls"><input id="password" name="password" type="password" placeholder="password" class="input-xlarge" required=""></div></div><div class="control-group"><div class="controls"><button class="btn btn-success" type="submit" name="login" id="login" value="Login">Login</button><a style="padding: 0 0 0 130px;" href="password-reset.php">Forgot Password?</a></div></div></form></div><div style=" width:100%; padding:10px 0 10px 0; margin: 0 auto;"><hr></div><div><?php if(isset($authUrl)) { echo '<a class="btn btn-block btn-warning" style="color:white;" href=" '.$authUrl.' ">Sign in with Google</a>'; } ?><div style="padding-top:10px;"></div><a class="btn btn-block btn-primary" style="color:white;" href="?login&op=facebook">Sign in with Facebook</a></div>'); $('.modal-footer').html('<button id="reges" type="button" class="btn btn-primary" style="opacity: 1; ">Register</button><button type="button" class="btn btn-default" data-dismiss="modal" style="opacity: 1; ">Cancel</button>'); $('#error-modal').modal('show'); }
    $('#login').click(function(){ loginDialog(); });
    $('#loginupload').click( function() { loginDialog(); $('input#login-next').attr('value', 'upload'); });
    function regesDialog() { $('.modal-title').html('Register'); $('.modal-body').html('<div><form class="form-signin" action="includes/scripts/crownreg.php" method="post" enctype="multipart/form-data"><p><div class="control-group"><input type="text" pattern=".{5,}" title="Minimum of 5 Characters" class="input-large" name="username" placeholder="Username" required /></p><p><input type="text" class="input-large" name="fname" placeholder="First Name" required /></p><p><input type="text" class="input-large" name="lname" placeholder="Last Name" required /></p><p><input type="text" pattern="[^ @]*@[^ @]*" title="**@example.com" class="input-large" name="email" placeholder="Email Address" required /></p><p><select class="row-fluid" name="gender" required ><option value="">*SELECT GENDER*</option><option value="male">Male</option><option value="female">Female</option></select></p><p><span style="color:green;">***optional</span><input type="text" pattern="([0-9])" title="Numbers Only" class="input-large" name="phone" placeholder="Phone Number"></p><p><input type="password" pattern=".{8,}" title="Minimum of 8 Characters" class="input-large" name="password"  placeholder="Password" required /><p><input type="password" pattern=".{8,}" title="Minimum of 8 Characters" class="input-large" name="password"  placeholder="Confirm Password" required /></p></p><button class="btn btn-success" type="submit" value="Login">Register</button><p></form></div>'); $('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal" style="opacity: 1; ">Cancel</button>'); $('#error-modal').modal('show'); } 
    $('#reges').live('click', function(){ regesDialog(); });
    function galleryDialog() { $('.modal-title').html('Select Your Condition'); $('.modal-body').html('<center><div><img src="images/warm-cool-color-wheel.png" height="50%" width="50%"/></center><center><a class="btn btn-mini" href="conCool.php">Cool Colors</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-mini" href="conNeutral.php">Neutral Colors</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-mini" href="conWarm.php">Warm Colors</a></center>'); $('#error-modal').modal('show'); }
    $('#gallery').click(function(){ galleryDialog(); });
    function ovv() { $('.modal-title').html('Tutorial'); $('.modal-body').html('<div><video poster="images/poster.png" width="530" height="350" controls><source src="http://www.crownit.co.ke/vidtut/crown.webm" type="video/webm"><source src="http://www.crownit.co.ke/vidtut/crown.mp4" type="video/mp4"><source src="http://www.crownit.co.ke/vidtut/crown.ogv" type="video/ogv"></video></div>'); $('#error-modal').modal('show'); }
    $('#ovv').click(function(){ ovv(); });
    function prov() { $('.modal-title').html('Tutorial'); $('.modal-body').html('<div><video poster="images/poster.png" width="530" height="350" controls><source src="http://www.crownit.co.ke/vidtut/crown_profile.mp4" type="video/mp4"></video></div>'); $('#error-modal').modal('show'); }
    $('#prov').click(function(){ prov(); });

    var ccv = {
    steps: [{ element: '.step1', intro: 'This Is The Color Visualizer Panel. <br/>', position: 'top' },{ element:'.step2', intro: 'This Is The Stage. <br/> This Is Where The Scene You Select Is Loaded.', position: 'right' },
    { element:'.step3', intro: 'This Is The Color Palette. <br/> Scroll Down To View More Colors. <br/> Now Click On A Color To Select It. ', position: 'left' },
    { element:'.step4', intro: 'This Is The Color You Just Selected.', position: 'top' },
    { element:'.step5', intro: 'This Is The Name Of The Color', position: 'top' },{ element:'.step6', intro: 'This Is The Code Of The Color', position: 'top' },
    { element:'.step7', intro: 'This Is The Recent Colors Panel. <br/> It Contains The Last Five Colors You Select.', position: 'top' },
    { element:'.step8', intro: 'Back to the stage. <br/> Move your mouse cursor around in the stage. <br/> Notice that some sections are highlighted. <br/> You can only tranform these sections. <br/> Click on a section to transform it.', position: 'right' },
    { element:'.step9', intro: 'This Is The Refresh Tool. <br/> Select It To Clear Any Tranformations Done To The Scene Loaded On The Stage.', position: 'left' },
    { element:'.step10', intro: 'The Explore Button <br/> Select To Go To Explore The Site', position: 'bottom' },
    { element:'.step11', intro: 'The Join Our Network Button <br/> Select To Sign Up Or Sign In', position: 'bottom' },
	{ element:'.step12', intro: 'The Color Remover Button <br/> Select To Clear The Colors From The Image', position: 'left' }]
    };

    function startIntro(options) {
        var intro = introJs();
        intro.setOption('skipLabel', 'Quit');
        intro.setOption('doneLabel', 'Okay, Got It');
        intro.setOptions(options);
        intro.oncomplete(function() {
        // set cookie
        $.removeCookie('ccvtut');
        $.cookie('ccvtut', 'done', { expires: 30, path: '/' });;
        });
            intro.start();
       }
    function initIntro(){
    (function(){
        var d1 = window.location.pathname.split('/');
        var d2 = d1[d1.length -1].split('.');
        var ind = d2[0].toLowerCase();

        switch(ind)
        {
            case 'ccv' : startIntro(ccv);
            break;
            default: startIntro(ccv);
            break;
        }

    })();
    };

    var ccv_cookie = $.cookie('ccvtut');
	   
    if(!ccv_cookie) { initIntro(); }

    $('#start-tutorial').click(function(){ initIntro(); });
	   
    /** share popover **/
    var config = {
        title: 'Share via Social Media',html: true,placement: 'bottom',
        content: '<div style="width:175px;">' + '<div class="btn-toolbar"><div class="btn-group"><a class="btn share-action" id="facebook" data-target="#" ><img src="images/social/facebook.png" width="18" /></a><a class="btn share-action" id="twitter" data-target="#" ><img src="images/social/twitter.png" width="18" /></i></a><a class="btn share-action" id="linkedin" data-target="#"><img src="images/social/linkedin.png" width="18" /></a><a class="btn share-action" id="google" data-target="#"><img src="images/social/googleplus.png" width="18" /></a></div></div>' + '</div>' };

        $('.action-share').popover(config);

    /** account popover **/
    var config = {
        title: "<?php echo (isset($_COOKIE['CNM'])) ? $_COOKIE['CNM'] : 'Account' ; ?>", html: true, placement: 'bottom',
        content: '<div style="width:126px;"><a class="btn action-save-profile" style="text-align: none;" data-target="#"><i class="icon-star"></i> Favorite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br><a class="btn" style="text-align: none; margin-top:8px;" href="profile.php"><i class="icon-user"></i> Profile Page</a></div>'
    };
    if( $('.action-fav').attr('disabled') ) {
        return false;
    } else {
        $('.action-fav').popover(config);
    }
});	
</script>
</head>
<body>
  <!-- FB script -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=412524842210283";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <!-- end of FB -->

    <div class="body-wrapper">
        <div class="white-background">
            <div class="container">
                <div class="header">
                <div class="header-nav box_white">
                    <!--Top Menu-->
                    <div id="topmenu"><div class="header-title-content">
                    <!--logo-->
                    <div class="logo"><a href="index.php"><img src="images/ccvv.png" alt="" /></a></div>
                    <!--/logo-->
                    </div></div>
                    <!--/top menu-->
                </div>
                <!--Let's Do It--><!--/let's do it-->
                <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <!--/header-->
        
<div class="black-background">
	<div class="container">
        <!--header slider-->
		<div class="header-title-content text-left box_bleck" >
		
		<div style="padding:5px;">
			<a class="btn btn-mini" href="index.php">Home</a>
			<a class="btn btn-mini" id="gallery">Color Inspiration</a>
			<a class="btn btn-mini btn-info text-white text-extrabold step10" href="explore.php">Explore Visualizer</a>
			<?php		
			$page = (isset($_COOKIE['CDM'])) ? 'admin.php' : 'profile.php'; 
			$name = (isset($_COOKIE['CDM'])) ? 'admin' : 'profile';
			echo (isset($_COOKIE['CNM']) || isset($_SESSION['oau'])) ? '<a class="btn btn-mini" style="float:right;" href="' . $page . '">' . $name . '</a>' : '<a class="btn btn-mini btn-success text-extrabold step11" style="float:right;" id="login">JOIN OUR NETWORK</a>';
			
			/* upload your photo */
			echo (isset($_COOKIE['CNM']) || isset($_SESSION['oau'])) ? '<a class="btn btn-mini btn-warning" style="float:right; margin:0 10px 0 0;" href="profile.php#/upload">Upload Your Image</a>' : '<a class="btn btn-mini btn-warning text-extrabold" style="float:right; margin: 0 10px 0 0;" id="loginupload">Upload Your Image</a>';
			?>
		</div>
		
		</div>
        <div class="header-title-content text-left" >				
			<div style="padding:5px;">
				<h1 style="font-size:16px; font-family: 'Open Sans' !important; color:white; padding: 5px; text-align: center !important; font-weight: bold; margin-left: 0px !important; width: 100% !important;">Choose A Color From The Palette, Then Select A Wall To Transform</h1>
			</div>
		</div>
    </div>
</div>

<!-- middle -->
<div class="black-background">
    <div class="container mid">
    <div id="mid" class="full_width step1">
            <!-- content ccv -->
            <div class="content" role="main" style="position: relative;">
				<div style="position:absolute; top:15px; right:-200px"></div>
            <div class="app">
            <div class="indicator">
                <div></div>
				<span>loading</span>
            </div>
            <div id="left-pane">
                <div class="colorbar-menu">
                    <div class="btn-group">
                        <a class="btn action boxd" style="background:red;" data-palette="0-15" title="Red Color" ></a>
                        <a class="btn action boxd" style="background:orange;" data-palette="16-35" title="Orange Color" ></a>
                        <a class="btn action boxd" style="background:yellow;" data-palette="36-70" title="Yellow Color" ></a>
                        <a class="btn action boxd" style="background:green;" data-palette="71-162" title="Green Color" ></a>
                        <a class="btn action boxd" style="background:blue;" data-palette="163-252" title="Blue Color" ></a>
                        <a class="btn action boxd" style="background:indigo;" data-palette="253-268" title="Indigo Color" ></a>
                        <a class="btn action boxd" style="background:violet;" data-palette="269-279" title="Violet Color" ></a>
                        <a class="btn action boxd" style="background:pink;" data-palette="280-318" title="Pink Color" ></a>
                        <a class="btn action boxd" style="background:rgb(252, 0, 149);" data-palette="319-360" title="Magenta Color" ></a>
                        <a class="btn action boxd" style="background:rgb(253, 245, 237);" data-palette="white" title="White Color" ></a>
                        <a class="btn action boxd" style="background:rgb(253, 244, 221);" data-palette="cream" title="Cream Color" ></a>
                        <a class="btn action boxd" style="background:gray;" data-palette="gray" title="Gray Color" ></a>
                        <a class="btn action boxd" style="background:black;" data-palette="black" title="Black Color" ></a>
                    </div>
                </div>
                <div id="app-canvas" class="step2 step8" data-num="step8"></div>
            </div>
            <div id="right-pane">
            	<div class="selector">	
                    <div class="btn-toolbar" style="margin:4px;">
						<div class="btn-group" style="position:relative;">
							<a class="btn action-print" data-target="#" title="Print to PDF"><i class="icon-print"></i></a>
							<a class="btn action-fav" data-target="#" data-toogle="popover" <?php echo ($logged_in == 'true') ? 'data-user-id="' . $_SESSION['id'] . '" ' . 'data-username="' . $_SESSION['username'] . '" ' : 'disabled="disabled"'; ?> ><i class="icon-user"></i></a>
							<a class="btn action-share" data-target="#" data-toggle="popover" title="Share via Social Media"><i class="icon-share"></i></a>
						</div>
						<div class="btn-group">
							<a class="btn action-refresh step9" data-target="#" title="Refresh the stage"><i class="icon-refresh"></i></a>
						</div>
						<div class="btn-group">
							<a class="btn action-clear step12" title="Remove Colors From Image"><i class="icon-cog"></i></a>
						</div>
					</div>
					<input type="text" class="input search" placeholder="Find Colors" autofocus>
            	</div>
                <div class="color-selector-box" style="background:#f2f2f2;">
                    <p style="border-bottom:1px solid #bfc0bd; padding: 3px;">Select A Shade</p>
                    <div class="clea"></div>
				</div>

            	<div class="panel step3"></div>

                <div class="recent step7">
                    <p>Recent colors</p>
                    <div class="mini-box-cont">
                        <div id="box" class="mini-box-0"></div>
                        <div id="box" class="mini-box-1" ></div>
                        <div id="box" class="mini-box-2"></div>
                        <div id="box" class="mini-box-3"></div>
                        <div id="box" class="mini-box-4"></div>
                    </div>
                </div>

            	<div class="app-panel">
                    <div class="sample">
                        <p class="sample-color step5">color name</p>
                        <div id="dropper-min-dark" class="box"></div>
                        <div id="dropper" class="step4"></div>
                        <div id="dropper-min-light" class="box"></div>
                        <div class="clear"></div>
                        <p class="sample-code step6">color code</p>
                    </div>
               </div>
            </div>

            <div class="clear"></div>
            </div>
				
			<div class="ccv-disclaimer">Due to individual computer monitor limitations, colors seen here may slightly vary from Crown Paints Kenya Ltd &copy; paint colors. To confirm your color choices, visit any of the Crown Paints stores and refer to our in-store color cards.</div>

            <!-- main footer -->
            <div class="our-work" style="margin-top:10px;box-shadow: 0px 0px 0.5px rgb(245, 245, 245);">
                <div class="content">
                    <div class="row" style="width:960px;">
                        <div class="col col_1_4"><a href="Interiors.php">
                                <div class="inner"> <h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico1.jpg" alt="" />
                                    <h2 class="text-bold text-white">Interiors</h2>
                                    <p>Transform your Interior</p>
                                </div>
                            </a></div>

                        <div class="col col_1_4">
                            <a href="Exteriors.php">
                                <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico2.jpg" alt="" />
                                    <h2 class="text-bold text-white">Exteriors</h2>
                                    <p>Transform your Exterior</p>
                                </div>
                            </a>
                        </div>

                        <div class="col col_1_4">
                            <a href="textures.page.php">
                                <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico3.jpg" alt="" />
                                    <h2 class="text-bold text-white">Textures</h2>
                                    <p>Transform with Textures</p>
                                </div>
                            </a>
                        </div>

                        <div class="col col_1_4">
                            <a href="offices.page.php">
                                <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico4.jpg" alt="" />
                                    <h2 class="text-bold text-white">Offices</h2>
                                    <p>Transform your Office</p>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>
           </div>
           <!--/ main footer -->
            
            </div>
            </div>
            <!--/ content ccv -->
           
            <div class="clear"></div>
        </div>
</div>

<?php require 'includes/footer.php'; ?>