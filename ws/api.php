<?php
/**
 * Profile social features web services
 */

/* start the session */
@session_start();

/* db connection */
include '../includes/dao/config.php';

/* logged out */
if (count($_SESSION) == 0) {

    $status['status'] = 0;
    $status['message'] = 'Please log in';
    $status['payload'] = array();

    echo json_encode($status);
    exit;

}

/* do */
$do = (isset($_REQUEST['do'])) ? $_REQUEST['do'] : 'NULL';

/* params */
$user_id = (isset($_SESSION['id'])) ? $_SESSION['id'] : 'NULL';
$project_id = (isset($_REQUEST['project_id'])) ? mysql_real_escape_string($_REQUEST['project_id']) : 'NULL';
$following_id = (isset($_REQUEST['following_id'])) ? mysql_real_escape_string($_REQUEST['following_id']) : 'NULL';
$comment_text = (isset($_REQUEST['comment_text'])) ? mysql_real_escape_string($_REQUEST['comment_text']) : 'NULL';
$comment_id = (isset($_REQUEST['comment_id'])) ? mysql_real_escape_string($_REQUEST['comment_id']) : 'NULL';
$userid = (isset($_REQUEST['userid'])) ? mysql_real_escape_string($_REQUEST['userid']) : 'NULL';
$last_id = (isset($_REQUEST['last_id'])) ? mysql_real_escape_string($_REQUEST['last_id']) : 'NULL';
$limit = (isset($_REQUEST['limit'])) ? mysql_real_escape_string($_REQUEST['limit']) : 'NULL';
$kw = (isset($_REQUEST['kw'])) ? mysql_real_escape_string($_REQUEST['kw']) : 'NULL';

/* Routes */
switch ($do) {

    case 'list-liked-projects' : listLikedProjects($user_id) ;
    break;

    case 'like-project' : likeProject($user_id, $project_id) ;
    break;

    case 'unlike-project' : unlikeProject($user_id, $project_id) ;
    break;

    case 'get-like-count' : getlikeCount($user_id) ;
    break;

    case 'list-following' : listFollowing($user_id) ;
    break;

    case 'list-followers' : listFollowers($user_id) ;
    break;

    case 'list-pending' : listPending($user_id) ;
    break;

    case 'follow-user' : followUser($user_id, $following_id) ;
    break;

    case 'unfollow-user' : unfollowUser($user_id, $following_id) ;
    break;

    case 'confirm-request' : confirmFollowRequest($user_id, $following_id) ;
    break;

    case 'get-following-count' : getFollowingCount($user_id) ;
    break;

    case 'get-followers-count' : getFollowersCount($user_id) ;
    break;

    case 'list-comments' : listComments($project_id) ;
    break;

    case 'add-comment' : addComment($user_id, $project_id, $comment_text) ;
    break;

    case 'edit-comment' : editComment($user_id, $comment_id, $comment_text) ;
    break;

    case 'remove-comment' : removeComment($user_id, $comment_id) ;
    break;

    case 'get-comments-count' : getCommentsCount($project_id) ;
    break;

    case 'get-pending-count' : getPendingFollowRequestCount($user_id) ;
    break;

    case 'get-user-feed' : getUserFeed($user_id, $last_id, $limit) ;
    break;

    case 'get-user-data' : getUserData($userid, 'echo') ;
    break;

    case 'get-user-projects' : getUserProjects($userid) ;
    break;

    case 'get-user-liked-projects' : getUserLikesFeed($userid, $last_id, $limit);
    break;

    case 'find' : findUser($kw);
    break;

}

/* Like project - functions */

/**
 * List projects liked by user
 * @param $user_id
 */
function listLikedProjects($user_id) {

    $sql = "SELECT tbl_images.id, tbl_images.user_id, tbl_images.image_filename, tbl_images.image_timestamp FROM tbl_likes, tbl_images WHERE tbl_images.image_privacy = 1 AND tbl_images.id = tbl_likes.project_id AND tbl_likes.user_id = $user_id ORDER BY id DESC";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');

            $return[] = array(
                "id" => $data['id'],
                "image_filename" => $data['image_filename'],
                "image_timestamp" => timeAgo(strtotime($data['image_timestamp'])),
                "user_id" => $user_data[0]['id'],
                "username" => $user_data[0]['username'],
                "full_name" => $user_data[0]['full_name'],
                "image" => $user_data[0]['image']
            );

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Get the people who like a project
 * @param $project_id
 */
function listProjectLikes($project_id) {

    $sql = "SELECT tbl_images.id, tbl_images.user_id, tbl_images.image_filename, tbl_images.image_timestamp FROM tbl_likes, tbl_images WHERE tbl_images.id = tbl_likes.project_id AND tbl_likes.user_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');

            $return[] = array(
                "id" => $data['id'],
                "image_filename" => $data['image_filename'],
                "image_timestamp" => timeAgo(strtotime($data['image_timestamp'])),
                "user_id" => $user_data[0]['id'],
                "username" => $user_data[0]['username'],
                "full_name" => $user_data[0]['full_name'],
                "image" => $user_data[0]['image']
            );

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Like a project
 * @param $user_id
 * @param $project_id
 */
function likeProject($user_id, $project_id) {

    $sql = "INSERT IGNORE INTO tbl_likes (user_id, project_id) VALUES ($user_id, $project_id)";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Unlike a project
 * @param $user_id
 * @param $project_id
 */
function unlikeProject($user_id, $project_id) {

    $sql = "DELETE FROM tbl_likes WHERE user_id = $user_id AND project_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Get the number of projects the user likes
 * @param $user_id
 */
function getLikeCountAPI($user_id) {

    $sql = "SELECT count(id) as likes_count FROM tbl_likes WHERE user_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}


/**
 * Get the number of likes a project has
 * @param $project_id
 */
function getLikeCount($project_id) {

    $sql = "SELECT count(id) as likes_count FROM tbl_likes WHERE project_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

    } else {

        $return[] = 0;

    }

    return $return;

}


/* Follow / Un follow - functions */

/**
 * List people the user is following
 * @param $user_id
 */
function listFollowing($user_id) {

    $sql = "SELECT regusers.id, regusers.username, regusers.fname, regusers.lname, regusers.gender FROM regusers, tbl_following WHERE regusers.id = tbl_following.following_id AND tbl_following.following_status = 1 AND tbl_following.user_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * List a user's followers
 * @param $user_id
 */
function listFollowers($user_id) {

    $sql = "SELECT regusers.id, regusers.username, regusers.fname, regusers.lname, regusers.gender FROM regusers, tbl_following WHERE regusers.id = tbl_following.user_id AND tbl_following.following_status = 1 AND tbl_following.following_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Follow a user
 * @param $user_id
 * @param $follower_id
 */
function followUser($user_id, $following_id) {

    $sql = "INSERT IGNORE INTO tbl_following (user_id, following_id, following_status) VALUES ($user_id, $following_id, 0)";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Unfollow a user
 * @param $user_id
 * @param $follower_id
 */
function unfollowUser($user_id, $following_id) {

    $sql = "DELETE FROM tbl_following WHERE user_id = $user_id AND following_id = $following_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Confirm a users follow request
 * @param $user_id
 * @param $follower_id
 */
function confirmFollowRequest($user_id, $following_id) {
    /* 0 - not confirmed, 1 - following */
    $sql = "UPDATE tbl_following SET following_status = 1 WHERE user_id = $following_id AND following_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Get the number of people the user is following
 * @param $user_id
 */
function getFollowingCount($user_id) {

    $sql = "SELECT count(id) as following_count FROM tbl_following WHERE user_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Get the number of people that follow the user
 * @param $user_id
 */
function getFollowersCount($user_id) {

    $sql = "SELECT count(id) as followers_count FROM tbl_following WHERE following_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/* Project comments - function */

/**
 * List a project's comments
 * @param $project_id
 */
function listComments($project_id) {

    $sql = "SELECT id, comment_text, comment_timestamp, user_id FROM tbl_comments WHERE image_id = $project_id ORDER BY id ASC";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');
            $can_delete = ($user_data[0]['id'] == $_SESSION['id']) ? 'yes' : 'no';

            $return[] = array(
                'comment_id' => $data['id'],
                'comment_text' => $data['comment_text'],
                'comment_timestamp' => timeAgo(strtotime($data['comment_timestamp'])),
                'comment_user_id' => $user_data[0]['id'],
                'comment_user_fullname' => $user_data[0]['full_name'],
                'comment_user_pic' => $user_data[0]['image'],
                'can_delete' => $can_delete

            );

        }

        (count($return)) ? $return : $return[] = 'No Comments';

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Add a comment to a project
 * @param $user_id
 * @param $project_id
 * @param $comment_text
 */
function addComment($user_id, $project_id, $comment_text) {

    /* pre process comment text */

    $sql = "INSERT INTO tbl_comments (user_id, image_id, comment_text) VALUES ($user_id, $project_id, '$comment_text')";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Edit a comment
 * @param $user_id
 * @param $comment_id
 * @param $comment_text
 */
function editComment($user_id, $comment_id, $comment_text) {

    /* pre process comment text */

    $sql = "UPDATE tbl_comments SET comment_text = '$comment_text' WHERE user_id = $user_id AND id = $comment_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Remove a comment
 * @param $user_id
 * @param $comment_id
 */
function removeComment($user_id, $comment_id) {

    $sql = "DELETE FROM tbl_comments WHERE user_id = $user_id AND comment_id = $comment_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Get the number of comments a project has
 * @param $project_id
 */
function getCommentsCountAPI($project_id) {

    $sql = "SELECT count(id) as comments_count FROM tbl_comments WHERE image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);


}

/* User data - functions */

/* Get user data */
function getUserData($userid, $mode) {

    $sql = "SELECT regusers.id, regusers.username, CONCAT(regusers.fname, ' ', regusers.lname) as full_name, pro_images.image FROM regusers, pro_images WHERE regusers.id = pro_images.acc_id AND regusers.id = $userid LIMIT 1";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        if (count($return) > 0) {

            $status['status'] = 1;
            $status['message'] = 'Success';
            $status['payload'] = $return;

        } else {

            $status['status'] = 0;
            $status['message'] = 'Failure';
            $status['payload'] = array('The user does not exist');

        }

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    if ($mode == 'echo') {

        echo json_encode($status);

    } else if ($mode == 'ret') {

        return $return;
    }

}

function colors() {

$sql = "SELECT name, code, hex FROM tbl_colors";

$ret = mysql_query($sql) or die(mysql_error());

$return = array();

if ($ret) {

    while ($data = mysql_fetch_assoc($ret)) {

        $return[] = $data;

    }

    $status['payload'] = $return;

}

echo json_encode($status);

}

function hasUserLiked($user_id, $project_id) {

    $sql = "SELECT id FROM tbl_likes WHERE user_id = $user_id AND project_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = mysql_num_rows($ret);

    return $num;

}

function hasUserCommented($user_id, $project_id) {

    $sql = "SELECT id FROM tbl_comments WHERE user_id = $user_id AND image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = mysql_num_rows($ret);

    return $num;

}


/**
 * Get the number of comments a project has
 * @param $project_id
 */
function getCommentsCount($project_id) {

    $sql = "SELECT count(id) as comments_count FROM tbl_comments WHERE image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

    } else {

        $return[] = 0;

    }

    return $return;

}

/**
 * Check if a project has comments
 * @param $project_id   project id
 * @return int          1 has, 0 hasn't
 */
function hasProjectComments($project_id) {

    $user_id = $_SESSION['id'];

    $sql = "SELECT id FROM tbl_comments WHERE image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = mysql_num_rows($ret);

    return $num;

}


function getUserProjects($user_id) {

    /* their projects */
    $sql = "SELECT tbl_images.id, tbl_images.user_id, tbl_images.image_filename, tbl_images.image_colors, tbl_images.image_fav, tbl_images.image_timestamp FROM tbl_images WHERE tbl_images.user_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    $stream = '';

    $num = mysql_num_rows($ret);

    if ($num > 0) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');

            $comments_count = getCommentsCount($data['id']);

            $project_likes_count = getLikeCount($data['id']);

            $has_liked = hasUserLiked($user_id, $data['id']);
            $like_text = ($has_liked == 1) ? 'unlike' : 'like';
            $like_link = ($has_liked == 1) ? 'unlike-project' : 'like-project';

            $has_comments = hasProjectComments($data['id']);
            $comment_toggle = ($has_comments == 0) ? '' : 'none';

            $colors = fetchColors($data['image_colors']);

            $stream .=  '<div class="stream-item" data-item-id="' . $data['id'] . '">' .
                '<div class="stream-side-bar">' .				
                '<div class="stream-item-user-profile-image"><img src="pro_pics/' . $user_data[0]['image'] . '" /></div>' .
                '<div class="stream-item-colors">' . $colors .
                '</div>' .
                '</div>' .
                '<div class="stream-item-project-item">' .
                '<div class="stream-item-project-user-details">' .
                '<a href="#" data-uid="' . $user_data[0]['id'] . '" class="stream-item-user-fullname">' . $user_data[0]['full_name'] . '</a>' .
                '<span class="stream-item-user-username"> (' . $user_data[0]['username'] . ') </span>' .
                '<span class="stream-item-user-username"> &middot; ' . timeAgo(strtotime($data['image_timestamp'])) . '</span>' .
                '</div>' .
                '<div class="stream-item-project-image">' .
                '<img src="uploads/' . $data['image_filename'] . '" width="100%">' .
                '<div class="clear"></div>' .
                '<div class="stream-item-links">' .
                '<a id="' . $like_link . '" href="#" data-item-id="' . $data['id'] . '" data-item-count="' . $project_likes_count[0]['likes_count'] . '" >' . $like_text . '</a>' .
                ' &middot; ' . '<a id="comment-project" href="#" data-item-id="' . $data['id'] . '">show comments</a>' .
                '</div>' .
                '<div class="stream-item-details">' .
                '<span class="stream-item-likes-count" data-item-id="' . $data['id'] . '">' . $project_likes_count[0]['likes_count'] .' likes</span>' .
                '&nbsp;&nbsp;&nbsp;' .
                '<span class="stream-item-comments-count" data-item-id="' . $data['id'] . '">' . $comments_count[0]['comments_count'] . ' comments</span>' .
                '</div>' .
                '<div class="stream-item-comments">' .
                '<div class="stream-item-comments-list-' . $data['id'] . '"></div>' .
                '<form id="comment-post-form-' . $data['id'] . '" style="display:' . $comment_toggle . ';">' .
                '<div class="stream-comment-box">' .
                '<div class="stream-comment-user-pic" >' .
                '<img src="pro_pics/' . $user_data[0]['image'] . '" width="42" height="42">' .
                '</div>' .
                '<textarea class="stream-comment-box-textarea" data-project-id="' . $data['id'] . '" placeholder="Write a comment" aria-expanded="true"></textarea>' .
                '</div>' .
                '<input id="project-id" name="project-id" type="hidden" value="' . $data['id'] . '" >' .
                '<input id="do" name="do" type="hidden" value="' . $data['id'] . '" >' .
                '</form>' .
                '</div>' .
                '</div>' .
                '</div>' .
                '</div>';

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    /* echo json_encode($status); */
    if ($num > 0) {

        echo $stream ;//. '<div class="stream-item" id="stream-ajax-loader">' . '<a href="#" class="stream-item-project-item">load more</a>' . '</div>';

    } else {

        echo 'No projects found';

    }

}


/**
 * Fetch color details
 * @params $color String color string
 * @return $colorData Element Html color elements
 *
 */
function fetchColors($colors) {
    /*
        get an array of section info,
        we could remove the last element here, its blank.
    */
    $dat = explode(';', trim($colors) );
    /* initialize the conditional? */
    $where = '';

    /*
        loop through the section info array,
        notice that the array has one empty item,
        (count($dat) - 1)
    */
    for($i = 0; $i < (count($dat) - 1); $i++ ){
        /* split the section info item into an array */
        $code = explode(',', $dat[$i]);

        /* if the section info array has only one item */
        if( count($dat) == 2) {
            /* we're done building the conditional */
            $where .= "code = '$code[2]' ";

        } else {

            if($i == (count($dat) - 2)  ) {
                /* if its the last item in the array, we're done */
                $where .= "code = '$code[2]' ";

            } else {
                /* if its not the last item, we're not done yet. add an OR to the conditional  */
                $where .= "code = '$code[2]' OR ";

            }

        }

    }

    /* build the query and add our conditional */
    $sql = "SELECT name, code, hex FROM tbl_colors WHERE ". $where;
    /* execute the query */
    $res = mysql_query($sql);

    $color_data = '<ul>';

    /* if the query returned a resource object, */
    if ($res) {
        /* get the number of rows returned */
        $count = mysql_num_rows($res);
        /* if the number of greater than one */
        if ( $count > 0 ) {
            /* loop through the rows, and build the list element */
            while ( $row = mysql_fetch_assoc($res) ) {

                $color_data .= '<li>';
                $color_data .= '<a class="box" data-target="#" title="' . $row['name'] .  $row['code'] . '" data-name="' . $row['name'] . '" data-code="' . $row['code'] . '" style="background:#'. trim($row['hex']) . '; "></a>';
                $color_data .= '</li>';

            }

        }

    } else {

        $color_data .= '<li></li>';

    }

    return $color_data;

}


/**
 * Get the user's liked projects
 * @param $user_id
 */
function getUserLikesFeed($user_id, $last_id, $limit) {

    /* last id */
    $last_id_string = ($last_id != '') ? "AND tbl_images.id < $last_id" : '';

    /* user's following */
    $likes = listLikedProjectIds($user_id);

    /* if the user likes some projects */
    if ($likes):

    /* their projects 0 - request-pending, 1 - request-accepted */
    $sql = "SELECT tbl_images.id, tbl_images.user_id, tbl_images.image_filename, tbl_images.image_colors, tbl_images.image_fav, tbl_images.image_timestamp FROM tbl_images WHERE tbl_images.id IN ($likes) $last_id_string ORDER BY tbl_images.id DESC LIMIT $limit";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    $stream = '';

    $num = mysql_num_rows($ret);

    if ($num > 0) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');

            $comments_count = getCommentsCount($data['id']);

            $project_likes_count = getLikeCount($data['id']);

            $has_liked = hasUserLiked($user_id, $data['id']);
            $like_text = ($has_liked == 1) ? 'unlike' : 'like';
            $like_link = ($has_liked == 1) ? 'unlike-project' : 'like-project';

            $has_comments = hasProjectComments($data['id']);
            $comment_toggle = ($has_comments == 0) ? '' : 'none';


            $colors = fetchColors($data['image_colors']);


            $stream .=  '<div class="stream-item" data-item-id="' . $data['id'] . '">' .
                '<div class="stream-side-bar">' .
                '<div class="stream-item-user-profile-image"><img src="pro_pics/' . $user_data[0]['image'] . '" /></div>' .
                '<div class="stream-item-colors">' . $colors .
                '</div>' .
                '</div>' .
                '<div class="stream-item-project-item">' .
                '<div class="stream-item-project-user-details">' .
                '<a href="#" data-uid="' . $user_data[0]['id'] . '" class="stream-item-user-fullname">' . $user_data[0]['full_name'] . '</a>' .
                '<span class="stream-item-user-username"> (' . $user_data[0]['username'] . ') </span>' .
                '<span class="stream-item-user-username"> &middot; ' . timeAgo(strtotime($data['image_timestamp'])) . '</span>' .
                '</div>' .
                '<div class="stream-item-project-image">' .
                '<img src="uploads/' . $data['image_filename'] . '" width="100%">' .
                '<div class="clear"></div>' .
                '<div class="stream-item-links">' .
                '<a id="' . $like_link . '" href="#" data-item-id="' . $data['id'] . '" data-item-count="' . $project_likes_count[0]['likes_count'] . '" >' . $like_text . '</a>' .
                ' &middot; ' . '<a id="comment-project" href="#" data-item-id="' . $data['id'] . '">show comments</a>' .
                '</div>' .
                '<div class="stream-item-details">' .
                '<span class="stream-item-likes-count" data-item-id="' . $data['id'] . '">' . $project_likes_count[0]['likes_count'] .' likes</span>' .
                '&nbsp;&nbsp;&nbsp;' .
                '<span class="stream-item-likes-count" data-item-id="' . $data['id'] . '">' . $comments_count[0]['comments_count'] . ' comments</span>' .
                '</div>' .
                '<div class="stream-item-comments">' .
                '<div class="stream-item-comments-list-' . $data['id'] . '"></div>' .
                '<form id="comment-post-form-' . $data['id'] . '" style="display:' . $comment_toggle . ';">' .
                '<div class="stream-comment-box">' .
                '<div class="stream-comment-user-pic" >' .
                '<img src="pro_pics/' . $user_data[0]['image'] . '" width="42" height="42">' .
                '</div>' .
                '<textarea class="stream-comment-box-textarea" data-project-id="' . $data['id'] . '" placeholder="Write a comment" aria-expanded="true"></textarea>' .
                '</div>' .
                '<input id="project-id" name="project-id" type="hidden" value="' . $data['id'] . '" >' .
                '<input id="do" name="do" type="hidden" value="' . $data['id'] . '" >' .
                '</form>' .
                '</div>' .
                '</div>' .
                '</div>' .
                '</div>';

            $stream_id = $data['id'];

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        if (!$likes) {

            $stream .= '<p style="margin:50px 35% 0 35%;">You do not like any projects yet</p>';

        } else {

            $stream .= '<p style="margin:50px 35% 0 35%;">No public projects have been found</p>';

        }

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo $stream;

    endif;

    /* if the user doesn't like any projects, show popular */
    if (!$likes):

        echo '<p style="margin:50px 35% 0 35%;">You do not like any projects yet</p>';

    endif;

}

/**
 * List projects the user likes
 * @param $user_id
 */
function listLikedProjectIds($user_id) {

    $sql = "SELECT tbl_images.id FROM tbl_likes, tbl_images WHERE tbl_images.id = tbl_likes.project_id AND tbl_likes.user_id = $user_id";
    $ret = mysql_query($sql) or die(mysql_error());

    $return = '';

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return .= "'" . $data['id'] . "',";

        }

    } else {

        $return = 'No liked Projects';

    }

    return  substr($return, 0, -1);

}

function findUser($kw) {

    $user_id = $_SESSION['id'];

    $sql = "SELECT regusers.id, regusers.username, regusers.fname, regusers.lname, pro_images.image FROM regusers, pro_images WHERE pro_images.acc_id = regusers.id AND username LIKE '%$kw%' OR fname LIKE '%$kw%' OR lname LIKE '%$kw%' GROUP BY regusers.id";

    $ret = mysql_query($sql) or die(mysqll_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $follow = checkIfFollowing($_SESSION['id'], $data['id'], 'ret');

            $return[] = array(

                'id' => $data['id'],
                'username' => $data['username'],
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'image' => $data['image'],
                'following' => $follow,
                'uid' => $user_id

            );

        }

        $status['status'] = 1;
        $status['message'] = 'Results';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Check if a user if following a person
 * @param $user_id  The user id
 * @param $following_id The subject's user id
 * @param $mode Return mode, echo -> echo, ret -> return
 * @return int Relationship id, 0 -> doesn't follow, 1 -> follows
 */
function checkIfFollowing($user_id, $following_id, $mode) {

    $sql = "SELECT id, following_status FROM tbl_following WHERE user_id = $user_id AND following_id = $following_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = '';

    if ($ret) {

        $numr = mysql_num_rows($ret);

        if ($numr > 0) {

            while ($row = mysql_fetch_assoc($ret)) {

                $num = $row['following_status'];

            }

        } else {

            $num = "2";

        }

    } else {

        /* not following */
        $num = "2";

    }

    if ($mode == 'echo') {

        echo $num;

    } else if ($mode == 'ret') {

        return $num;

    }

}


/**
 * Check if a person following the user
 * @param $user_id  The user id
 * @param $follower_id The person's user id
 * @param $mode Return mode, echo -> echo, ret -> return
 * @return int Relationship id, 0 -> doesn't follow, 1 -> follows
 */
function checkIfFollower($user_id, $follower_id, $mode) {

    $sql = "SELECT id FROM tbl_following WHERE user_id = $follower_id AND following_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    if ($ret) {

        $num =  mysql_num_rows($ret);

    } else {

        $num = 0;

    }

    if ($mode == 'echo') {

        echo $num;

    } else if ($mode == 'ret') {

        return $num;

    }

}

/**
 * Prettify time difference
 * @param $time timestamp
 * @return string pretty time
 */
function timeAgo($time) {
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();

    $difference = $now - $time;
    $tense = "ago";

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
        $periods[$j].= "s";
    }
    $return = "$difference $periods[$j] ago";
	
    if($return == "0 seconds ago"){
        return 'Just Now';
    }
    return $return;
}

/**
 * Get pending follow requests
 * @return JSON - follow notifications object
 */
function listPending($user_id) {

    $sql = "SELECT regusers.id, regusers.username, regusers.fname, regusers.lname, regusers.gender FROM regusers, tbl_following WHERE regusers.id = tbl_following.user_id AND tbl_following.following_status = 0 AND tbl_following.following_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}

/**
 * Get the number of pending follow requests
 * @param user id
 * @result JSON result object
 */
function getPendingFollowRequestCount($user_id) {

    $sql = "SELECT count(regusers.id) as pending_count FROM regusers, tbl_following WHERE regusers.id = tbl_following.user_id AND tbl_following.following_status = 0 AND tbl_following.following_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    echo json_encode($status);

}