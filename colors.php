<?php

$host = "localhost";
$db = "crownit";
$usr = "root";
$pwd = "";

/* $host = "localhost";
$db = "crownit_ccv";
$usr = "crownit_admin";
$pwd = "admin@)!#"; */

try {
	$con = new PDO("mysql:host=$host;dbname=$db", $usr, $pwd);	
}
	
catch(PDOException $e){
	echo $e->getMessage();
}

$palettes = array('0-15','16-35','36-70','71-162','163-252','253-268','269-279','280-318','319-360');
	
	foreach($palettes as $pal){
	
	if ($pal != '0-15') { $display = 'none'; } else { $display = 'inline'; }
	
	echo "<div id='shade_cont' class='$pal' style='float: left; display: $display;'>";

	$range = explode("-", $pal);
	
	$l = $range[0];
	$h = $range[1];

	$output = ""; 

	$sql = "SELECT name,code,rgb FROM tbl_v_color 
			WHERE hsv_h >= '$l' AND hsv_h <= '$h' 
			AND name NOT LIKE '%Black%'
			AND name NOT LIKE '%Holly%'
			AND name NOT LIKE '%Lizard%'
			AND name NOT LIKE '%Gray%' 
			AND name NOT LIKE '%Grey%' 
			AND name NOT LIKE '%White%' 
			AND name NOT LIKE '%Cream%'
			AND name NOT LIKE '%Scorched Earth%'
			AND name NOT LIKE '%GunMetal%'
			AND name NOT LIKE '%Gun Metal%' 
			ORDER BY `tbl_v_color`.`hsv_s` DESC";
	
	$res = $con->query($sql);
	
	foreach($res as $rs){

	$output .= '<a data-name="' . $rs['name'] . '" data-code="' . $rs['code'] . '" href="#" class="box" style="background: rgb(' . $rs['rgb'] . '); color: black !important; text-shadow: 1px 0px 2px white !important; padding: 15px; line-height:0;">'. $rs['name'] .' '.$rs['code'].'</a>';

	}
	
		echo $output;

	echo "<div style='clear:both;'></div></div>";
	
	}

	/* White Shades */

	$wht_output = "";

	$sql = "SELECT name,code,rgb FROM tbl_v_color
			WHERE name LIKE '%White%' 
			ORDER BY `tbl_v_color`.`hsv_v` DESC";

	$res = $con->query($sql);

	foreach($res as $r){
	
	$wht_output .= '<a data-name="' . $r['name'] . '" data-code="' . $r['code'] . '" href="#" class="box" style="background: rgb(' . $r['rgb'] . '); color: black !important; float:left; text-shadow: 1px 0px 2px white !important; padding: 15px; line-height:0;">'.$r['name'].' '.$r['code'].'</a>';

	}	

	echo "<div id='shade_cont' class='white' style='display: none;'>";

	echo $wht_output;

	echo "<div style='clear:both;'></div></div>";

	/* Cream Shades */

	$crm_output = "";

	$sql = "SELECT name,code,rgb FROM tbl_v_color
			WHERE name LIKE '%Cream%'
			ORDER BY `tbl_v_color`.`hsv_v` DESC";

	$res = $con->query($sql);

	foreach($res as $r){
	
	$crm_output .= '<a data-name="' . $r['name'] . '" data-code="' . $r['code'] . '" href="#" class="box" style="background: rgb(' . $r['rgb'] . '); color: black !important; float:left; text-shadow: 1px 0px 2px white !important; padding: 15px; line-height:0;">'.$r['name'].' '.$r['code'].'</a>';

	}	

	echo "<div id='shade_cont' class='cream' style='display: none;'>";

	echo $crm_output;

	echo "<div style='clear:both;'></div></div>";

	/* Grey Shades */

	$gry_output = "";

	$sql = "SELECT name,code,rgb FROM tbl_v_color
			WHERE name LIKE '%Gray%' 
			OR name LIKE '%Grey%'
			AND name NOT LIKE '%Velvet Grey%'  
			ORDER BY `tbl_v_color`.`hsv_v` DESC";

	$res = $con->query($sql);

	foreach($res as $r){
	
	$gry_output .= '<a data-name="' . $r['name'] . '" data-code="' . $r['code'] . '" href="#" class="box" style="background: rgb(' . $r['rgb'] . '); color: black !important; float:left; text-shadow: 1px 0px 2px white !important; padding: 15px; line-height:0;">'.$r['name'].' '.$r['code'].'</a>';

	}	

	echo "<div id='shade_cont' class='gray' style='display: none;'>";

	echo $gry_output;

	echo "<div style='clear:both;'></div></div>";

	/* Black Shades */

	$blk_output = "";

	$sql = "SELECT name,code,rgb FROM tbl_v_color
			WHERE name LIKE '%Lizard%' 
			OR name LIKE '%Holly%' 
			OR name LIKE '%Scorched Earth%'
			OR name LIKE '%Gun Metal%' 
			ORDER BY `tbl_v_color`.`hsv_v` DESC";

	$res = $con->query($sql);

	foreach($res as $r){
	
			$blk_output .= '<a data-name="' . $r['name'] . '" data-code="' . $r['code'] . '" href="#" class="box" style="background: rgb(' . $r['rgb'] . '); color: white !important; float:left; text-shadow: 0px 0px 1px white !important; padding: 15px; line-height:0;">'.$r['name'].' '.$r['code'].'</a>';

	}

	/* get black color only */

	$black = "";

	$sqll = "SELECT name,code,rgb FROM tbl_v_color
			WHERE name LIKE '%Black%'
			ORDER BY `tbl_v_color`.`hsv_v` DESC";

	$ress = $con->query($sqll);

	foreach($ress as $rs){

		$black .= '<a data-name="' . $rs['name'] . '" data-code="' . $rs['code'] . '" href="#" class="box" style="background: rgba(' . $rs['rgb'] . '); color: white !important; float:left; text-shadow: 0px 0px 1px white !important; padding: 15px; line-height:0;">'.$rs['name'].' '.$rs['code'].'</a>';
	
	}

	echo "<div id='shade_cont' class='black' style='display: none;'>";

	echo $blk_output;

	echo $black;

	echo "<div style='clear:both;'></div></div>";