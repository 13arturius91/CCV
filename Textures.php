﻿<?php
    require 'includes/header.php';
?>
        
        
        <div class="black-background">
            <div class="container">
            <div class="header-title-content text-left box_bleck" >				
							<div style="padding:5px;">	
							
							<a href="javascript:history.back()" class="btn btn-mini">Back</a>
							
							<a class="btn btn-mini" href="index.php">Home</a>
							  
							<a class="btn btn-mini" id="gallery">Color Inspiration</a>

                            <a class="btn btn-mini btn-info text-white text-extrabold" href="ccv.php">Launch Visualizer</a>

                            <a class="btn btn-mini" id="link">Video Tutorial</a>
							
							<a href="http://crownit.co.ke/wiki/index.php?title=Main_Page" target="_blank" class="btn btn-mini">Text Tutorial</a>
							
							<?php

                              //$page = (isset($_SESSION['isadmin'])) ? 'admin.php' : 'profile.php';
							  $page = (isset($_COOKIE['CDM'])) ? 'admin.php' : 'profile.php';
                              //$name = (isset($_SESSION['isadmin'])) ? 'admin' : 'profile';
							  $name = (isset($_COOKIE['CDM'])) ? 'admin' : 'profile';
							  
							  echo (isset($_COOKIE['CNM']) || isset($_SESSION['oau'])) ? '<a class="btn btn-mini" style="float:right;" href="' . $page . '">' . $name . '</a>' : '<a class="btn btn-mini btn-success text-extrabold" style="float:right;" id="login">JOIN OUR NETWORK</a>';

                            ?>
							 						 						
							<h1 style="font-size:16px; font-family: 'Open Sans' !important; color:white; padding-left:350px; font-weight:bold;">View Our Textures Gallery</h1>
													
							</div>		
              </div>
            <div class="tf-header-slider">
                <div class="flexslider" style="background: url(images/6.gif) center no-repeat;">
                  <ul class="slides">
                    <li>
                        <a href="#"> <img src="images/img/slide9.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2> 
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a href="#"><img src="images/img/slide10.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a href="#"><img src="images/img/slide11.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>

                            </div>
                        </div>
                        
                    </li>
                   
                     <li>
                        <a href="#"><img src="images/img/slide12.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#"><img src="images/img/slide13.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>
                     <li>
                        <a href="#"><img src="images/img/slide14.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>
                     <li>
                        <a href="#"><img src="images/img/slide15.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>                    
                     <li>
                        <a href="#"><img src="images/img/slide16.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>                    
                    <li>
                        <a href="#"><img src="images/img/slide17.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>                   
                     <li>
                        <a href="#"><img src="images/img/slide18.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>                         
                     <li>
                        <a href="#"><img src="images/img/slide19.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>                         
                     <li>
                        <a href="#"><img src="images/img/slide20.jpg" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">&nbsp;</h2>
                            </div>
                        </div>
                    </li>                         
                         
                                       
                  </ul>
                </div>
            </div>
            </div>
        </div>
        
<!-- middle -->
        <div class="white-background">
            <div class="container ">
                <div id="step2" class="our-work">
                    <div class="content">
                        <div class="row">
                            <div class="col col_1_4"><a href="Interiors.php">
                                    <div class="inner"> <h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                        <img src="images/tuts/ico1.jpg" alt="" />
                                        <h2 class="text-bold text-white">Interiors</h2>
                                        <p>
                                            Transform your Interior</p>
                                    </div>
                                </a></div>

                            <div class="col col_1_4"><a href="Exteriors.php">
                                    <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                        <img src="images/tuts/ico2.jpg" alt="" />
                                        <h2 class="text-bold text-white">Exteriors</h2>
                                        <p>Transform your Exterior</p>
                                    </div>
                                </a></div>

                            <div class="col col_1_4"><a href="Textures.php">
                                    <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                        <img src="images/tuts/ico3.jpg" alt="" />
                                        <h2 class="text-bold text-white">Textures</h2>
                                        <p>View Textures Gallery</p>
                                    </div>
                                </a></div>

                            <div class="col col_1_4"><a href="Offices.php">
                                    <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                        <img src="images/tuts/ico4.jpg" alt="" />
                                        <h2 class="text-bold text-white">Offices</h2>
                                        <p>View Office Gallery</p>
                                    </div>
                                </a></div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

<?php
    require 'includes/footer.php';
?>