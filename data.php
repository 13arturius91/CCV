<?php

@session_start();
include_once 'includes/dao/config.php';

function getPoints($file){
    $ret = '';
    $filename = "points/$file";
    if(file_exists($filename)) {
        $ret = file_get_contents($filename);
    }
    echo $ret;
}

function getPolys($file){
    $ret = '';
    $filename = "polys/$file";
    if(file_exists($filename)) {
        $ret = file_get_contents($filename);
    }
    echo $ret;
}

function getChartType($req) {
	
	$str = $scan = $cur = '';
	$textures = array("creations" => "textures/creations","marble" => "textures/marble","quartz" => "textures/quartz","ruff" => "textures/ruff");
	
		
	if($req == 't1') {
		$cur = 'marble';
		$scan = scandir($textures[$cur]);
	
	} elseif($req == 't2') {
		$cur = 'ruff';
		$scan = scandir($textures[$cur]);	

	} elseif($req == 't3') {
		$cur = 'quartz';
		$scan = scandir($textures[$cur]);	

	} elseif($req == 't4') {
		$cur = 'creations';
		$scan = scandir($textures[$cur]);	

	}
	
	for($i = 2; $i < count($scan); $i++) {
			
		$str	.= '<div class="box">' . '<a href="#"><img src="' . $textures[$cur] . '/' . $scan[$i] . '" title="' . substr($scan[$i], 0, stripos($scan[$i], '.')) . '"></div>';		
				
	}
	
	echo $str;

}

function getChartTypeCurl($req) {
	
	$url = 'http://www.crownpaints.co.ke/colourchartengine.php';
	$params = 'load_category_colours=' . $req;
	
	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	header('Content-type: text/html');
	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);
	
	//header('content: text/html');
	echo $result;
	
}

function saveImage($data) {

    try {

        $dir = 'uploads';
        $filename = md5( time() ) . '.jpg';
        $path = $dir . '/' . $filename;
        $uri = 'data://' . substr($data, 5);
        $bin = file_get_contents($uri);

        file_put_contents($path, $bin);

    } catch(Exception $e) {

        echo 'Error: ' . $e->getMessage();

    }

    $url = 'http://crownit.co.ke/' . $path;

    echo $url;

}

function saveImageToProfile($data, $file, $colors) {

    /* process image data */
    try {

        $dir = 'uploads';
        $image_id = md5( time() );
        $filename = $image_id . '.jpg';
        $path = $dir . '/' . $filename;
        $uri = 'data://' . substr($data, 5);
        $bin = file_get_contents($uri);

        file_put_contents($path, $bin);

    } catch(Exception $e) {

        echo 'Error: ' . $e->getMessage();

    }

    $image_filename = $filename; /* ($file) ? $file : $filename; */

    /* process colors */
    $image_colors = $colors;

    /* save to db */
    $res = saveImageData($image_id, $image_filename, $image_colors);

    echo $res;//'http://localhost/ccv-showroom/' . $path;

}

function saveTextureImageToProfile($data, $file, $textures) {

    /* process image data */
    try {

        $dir = 'uploads';
        $image_id = md5( time() );
        $filename = $image_id . '.jpg';
        $path = $dir . '/' . $filename;
        $uri = 'data://' . substr($data, 5);
        $bin = file_get_contents($uri);

        file_put_contents($path, $bin);

    } catch(Exception $e) {

        echo 'Error: ' . $e->getMessage();

    }

    $image_filename = $filename; /* ($file) ? $file : $filename; */

    /* process colors */
    $image_textures = $textures;

    /* save to db */
    $res = saveTextureImageData($image_id, $image_filename, $image_textures);

    echo $res;

}

function saveImageData($image_id, $image_filename, $image_colors) {
    /**
     * Save image data to the db
     *
     */

    $image_filename = mysql_real_escape_string($image_filename);
    $image_colors = mysql_real_escape_string($image_colors);

    $user_id = $_SESSION['id'];
    $ts = time();

    $sql = "INSERT INTO tbl_images (user_id, image_id, image_filename, image_colors, image_fav, image_timestamp) VALUES ('$user_id', '$image_id', '$image_filename', '$image_colors', 1, '$ts')";

    $res = mysql_query($sql) or die( mysql_error() );

    return $res;

}


$req = (isset($_REQUEST['r'])) ? $_REQUEST['r'] : '1';
$file = (isset($_REQUEST['f'])) ? $_REQUEST['f'] : 'none.js';
$data = (isset($_REQUEST['d'])) ? $_REQUEST['d'] : 'data:';
$colors = (isset($_REQUEST['c'])) ? $_REQUEST['c'] : '';

switch($req) {
	case '1': getChartTypeCurl($req);
	break;
	case '2': getChartTypeCurl($req);
	break;
	case '3': getChartTypeCurl($req);
	break;
	case 't1': getChartType($req);
	break;
	case 't2': getChartType($req);
	break;
	case 't3': getChartType($req);
	break;
	case 't4': getChartType($req);
	break;
	case 'js': getPoints($file);
    break;
	case 'poly': getPolys($file);
    break;
    case 'svimg': saveImage($data);
    break;
    case 'svprof': saveImageToProfile($data, $file, $colors);
    break;
    case 'svproft': saveTextureImageToProfile($data, $file, $textures);
    break;
	default: getChartType('t1');
	break;
}

?>