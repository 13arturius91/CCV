﻿  <div class="footer-slider">
       <div class="copyright">
        <span class="copyright-left">
        <img src="images/icons/logoss.png" alt="" border="0" />Crown Paints Kenya Ltd © All Rights Reserved.				
					<div class="social-box" style="float: right; width:; margin:0px 15px;">
            <div class="section-fb" style="float:left; height:72px; width:312px;">
                <div class="fb-like-box" data-href="https://www.facebook.com/crownpaintsltd" data-width="310" data-height="70" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>
              </div>
            <div class="section-tw" style="margin:0px 0px 0px 315px;">
            <!-- Twitter Button -->
            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-count="vertical"data-url="http://www.crownit.co.ke" >Tweet</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            <!--/ Twitter Button -->
            <!-- Plus One Button -->
            <div class="g-plusone" data-size="tall" data-href="http://www.crownit.co.ke"></div>
            <script type="text/javascript">
            (function() {
              var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
              po.src = 'https://apis.google.com/js/plusone.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
            </script>
            <!--/ Plus One Button -->
            </div>
          </div>
          <a target="nofollow" href="https://www.youtube.com/watch?v=SiQ7Ddz8ZWU" style="padding-top: 42px;font-weight: bold;color: #46ACCA;"><img src="images/youtube-rbutton.png" alt="" border="0" style="width: 6%;">View Tutorial On Youtube</a>
        </span>
      <span class="copyright-right">powered by <span class="text-orange" ><a target="nofollow" href="http://www.my-it-provider.com/" >My-IT-Provider LTD</a></span></span>
        <map name="Map">
          <area shape="circle" coords="89,13,12" href="www.facebook.co.ke" target="_blank" alt="Join us on facebook">
          <area shape="circle" coords="117,13,12" href="www.twitter.com" target="_blank" alt="follow us on twitter">
        </map>
      </span>
      <div class="clear"></div>
    </div>
  </div>
</div>
<!--/middle--></div>
       
         </div>

<!-- error modal -->
<div id="error-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="header-title-content text-left box_bleck"><div style="padding:5px;"><center>&nbsp;&nbsp;<a class="btn btn-mini" href="privacy.php" target="_blank" >Privacy Policy</a>&nbsp;&nbsp;<a class="btn btn-mini" href="termcon.php" target="_blank" >Terms & Conditions</a>&nbsp;&nbsp;</center></div></div>
<!--/ error modal -->

<!-- notify -->
<link rel="stylesheet" href="css/ui.notify.css">
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.notify.min.js"></script>
<script type="text/javascript">
$(function() {
    /* cookie life time - minutes */
    var life = 60;
    var now = new Date();
    var expires = new Date(now.getTime() + life * 60 * 1000);
    /* log in status */
    var logged_in = <?php echo @(isset($_SESSION['id'])) ? 'true' : 'false' ; ?>;
    /* current path - no notices in profile page */
    var path = $(location).attr('pathname');
    var script = path.substr( (path.lastIndexOf('/') + 1), (path.lastIndexOf('.') - path.lastIndexOf('/') - 1));
    /* check for cookie - _DND */
    var _DND = $.cookie('_DND');
    /* get username */
    var _user = <?php echo @(isset($_SESSION["username"])) ? '"' . $_SESSION['username'] . '"' : '""' ;?>;
    if (!_DND) {
        if (logged_in && (script != 'profile')) {
            $notify_container = $('#notify-container').notify();
            var notice = $notify_container.notify("create", "notify-template", {
                title: 'Hi ' + _user + ',',
                text: 'You are currently logged in.'
            },{
                expires: 'false',
                speed: 500,
                open: function() {
                    console.log('open');
                },
                close: function() {
                    /* disable notification for an hour */
                    $.removeCookie('_DND');
                    $.cookie('_DND', 'DND', { expires: expires, path: '/' });;
                },
                click: function() {
                    notice.close();
                }
            });
        }
    }
});
</script>
<div id="notify-container" style="display:none;">
    <div id="notify-template">
        <a class="ui-notify-cross ui-notify-close" href="#">x</a>
        <h1>#{title}</h1>
        <p>#{text}</p>
    </div>
</div>
<!-- notify -->

<!-- chat button -->
<a href="#" onclick='MyLiveChat_OpenDialog()' style="display: block; height: 50px; width: 150px; position: absolute; top:5px; right: 5px;">
    <!-- <img id="chat-state" src="" > -->
</a>

		<!--GALLERY-->
		<script src="source/jquery.swipebox.js"></script>
		<script type="text/javascript">
		jQuery(function($) {
			/* Basic Gallery */
			$(".swipebox").swipebox();
		});
		</script>

<!-- google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-44777554-1']);
  _gaq.push(['_setDomainName', 'crownit.co.ke']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--/ google analytics -->

<!-- chat script -->
<script type="text/javascript" src="https://mylivechat.com/chatinline.aspx?hccid=28128866"></script>
<!-- chat api -->
<script type="text/javascript" src='https://mylivechat.com/chatapi.aspx?hccid=28128866'></script>
<script type="text/javascript">
    $(function(){

        if (typeof("MyLiveChat") != "undefined") {

            if (MyLiveChat.HasReadyAgents) {

                $('img#chat-state').attr("src", "img/chat-online.gif");

            } else {

                $('img#chat-state').attr('src', "img/chat-offline.gif");

            }

        }

    });
</script>

</body>
</html>