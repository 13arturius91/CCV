<?php

include_once 'dao/config.php';

function getUsers() {

    /* the data */
    $sql = " SELECT username, fname, lname, gender, phone, email, date_nav FROM regusers WHERE 1 ";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<table class="table table-condensed table-striped"><thead>';
        $table .= '<tr><th>First Name</th><th>Last Name</th><th>Gender</th><th>Phone</th><th>Email</th><th>Last Active</th></tr>';
        $table .= '</thead><tbody>';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<tr>';
            //$table .= '<td>' . $rows['username'] . '</td>';
            $table .= '<td>' . $rows['fname'] . '</td>';
            $table .= '<td>' . $rows['lname'] . '</td>';
            $table .= '<td>' . $rows['gender'] . '</td>';
            $table .= '<td>' . $rows['phone'] . '</td>';
            $table .= '<td>' . $rows['email'] . '</td>';
            $date = date_create($rows['date_nav']);
            $dat = $date->format('d M, Y h:i a');
            $table .= '<td>' . $dat. '</td>';
            $table .= '</td>';

        }

        $table .= '</tbody></table>';

        echo $table;

    } else {

        print_r('error');

    }

}


?>
<html>
<head>
<title>User Report</title>
<link rel="stylesheet" href="http://localhost/libs/bootstrap.min.css" >
</head>
<body>

<div style="height: 63px;">
    <img src="../images/ccvv.png" style="float:left;" >
    <h2 style="line-height: 63px; margin-right:15px;" class="pull-right">Crown Color Visualizer</h2>
    <div style="clear: both;"></div>
</div>

<hr style="border:1px solid #c3c3c3;">

<div style="margin: 0 8px 15px 8px;">
    <span class="pull-left">
        <h2 style="text-decoration: underline; text-transform: uppercase;">A list of current users</h2>
    </span>
    <span class="pull-right">Generated on: <?php echo date('d M, Y h:i a') ?></span>
    <div class="clearfix"></div>
</div>

<?php getUsers() ?>

<hr style="border:1px solid #c3c3c3;">

</body>
</html>
