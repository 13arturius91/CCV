﻿<?php
//Connect to MySQL database
include "../dao/config.php";

@session_start();

if(isset($_SESSION['id']) ){
}

$allowed = array('png', 'jpg', 'gif');
$maxsize = '5MB';
$newfilename = '';

if ( isset($_FILES['upl']) && $_FILES['upl']['error'] == 0 ) {
    /* uploading error */
    if ($_FILES['upl']["error"]) {
        echo '{"status":"Error", "message" : " ' . $_FILES["file"]["error"] . ' " }';
        exit;
    }
    /* not an image file */
    $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

    if(!in_array(strtolower($extension), $allowed)){
        echo '{"status":"Error", "message":"You can only upload image files (png, jpeg, gif) "}';
        exit;
    }

    $filename = md5( time() ) . '.' . $extension;;

    // file aready uploaded$_FILES["upl"]["name"]
    if (file_exists("../../uploads/" . $filename )) {
        echo '{"status":"Error", "message" : " ' . $_FILES["upl"]["name"] . ' already exists. ' . ' " }';
        exit;
    }

        /* save file and return filename */
    if(move_uploaded_file($_FILES['upl']['tmp_name'], '../../uploads/'.$filename )) {
        echo '{"status":"Success", "message":"File upload complete", "file": "' . $filename . '"}';
        @mysql_query("INSERT INTO tbl_images (user_id, image_id, image_filename, image_colors, image_timestamp) VALUES ('". trim($_SESSION['id']) ."', '$filename', '$filename', 'NULL', now() )");
        exit;
    }
}
 
echo '{"status":"Error", "message":"Select An Image File to Upload"}';
exit;
?>