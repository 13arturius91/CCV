<?php

@session_start();

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]--->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">

<meta name="viewport" content="width=device-width,initial-scale=1">

<meta name="author" content="Francmut" />

<meta name="description"  content="crownit.co.ke - Crown Paints Manufacturers a wide range of paint products, adhesives, thinners and much more in Kenya and the rest of East Africa.Visualize your Rooms of your Homes,Offices,Exterior,Textures" />

<meta name="keywords" content="crown visualizer,crown paints visualizer,crown paints,crown visualizer,crown interior,crown exterior,crown textures,crown office,crown video tutorial,color visualizer,paint visualizer,Crown Paints, Industrial paints, Crown Berger, Wood Finishes, Automotive Paints, Adhesives, Kenya Paints, Paints in Kenya, Resins, Wood Finishes, Nairobi Industrial Area, Home Decoration, Car Paints, Protective Paint, Special Effects, Wood protection ,Automotive, interior paints,chlorinated paint,
chlorinated rubber paint, textured coating, chlorinated rubber paints, chlorinated rubber, primer or undercoat, high build coatings, primer and undercoat, chlorinated rubber coatings,
primer undercoat, wood primers, wood primer undercoat, two pack epoxy, 2 pack epoxy, 2 pack epoxy paint, rubber paint, two pack epoxy paint, primer for metal, textured paint, texture coating, red lead primer, crown varnish, berger color, polyurethane or varnish,
timber finishes" />

<title>Crown Color Visualiser</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300' rel='stylesheet' type='text/css'>
<link href="css/bootstrap.min.css" media="screen" rel="stylesheet">
<link href="style.css" media="screen" rel="stylesheet">
<link href="screen.css" media="screen" rel="stylesheet">
<link href="css/introjs.css" media="screen" rel="stylesheet">
<!--gallery CSS-->
<link rel="stylesheet" href="source/swipebox.css">

<!-- custom CSS -->
<link rel="stylesheet" href="custom.css">
<link href="css/flexslider.css" media="screen" rel="stylesheet" type="text/css">

<!-- favicon.ico and apple-touch-icon.png -->
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

<!-- defer loading of js --
<script type="text/javascript">
    
    function loadScript(src) {
         var element = document.createElement("script");
         element.src = src;
         document.body.appendChild(element);
    }


    function downloadJSAtOnload() {
        
        loadScript("js/libs/jquery.min.js");
        loadScript("js/libs/modernizr.min.js");
        loadScript("js/libs/respond.min.js");  

        loadScript("js/intro.js");
        loadScript("js/jquery.cookie.js");

        loadScript("js/jquery.easing.1.3.min.js");
        loadScript("js/hoverIntent.js");
        loadScript("js/general.js");
        loadScript("js/bootstrap.min.js");

        loadScript("js/jquery.carouFredSel.packed.js");
        loadScript("js/jquery.touchSwipe.min.js");

        loadScript("js/jquery.prettyPhoto.js");
        loadScript("js/jquery.flexslider-min.js");
        loadScript("js/search-input.js");
        loadScript("http://code.jquery.com/ui/1.10.3/jquery-ui.js");
    }

    if (window.addEventListener)
        window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent)
        window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;

</script>
-->
<!-- Mobile optimized -->
<script src="js/libs/modernizr.min.js"></script>
<script src="js/libs/respond.min.js"></script>
<script src="js/libs/jquery.min.js"></script>

<!-- Intro JS -->
<script src="js/intro.js"></script>
<script src="js/jquery.cookie.js"></script>

<!-- scripts -->
<!-- script src="js/jquery.easing.1.3.min.js"></script -->
<!-- script src="js/hoverIntent.js"></script -->
<script src="js/general.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- carousel -->
<script src="js/jquery.carouFredSel.packed.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>
<!-- lightbox prettyPhoto -->
<link rel="stylesheet" href="css/prettyPhoto.css">
<script src="js/jquery.prettyPhoto.js"></script>
<!-- top slider -->
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/search-input.js"></script>
<!-- JUI -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script type="text/javascript" charset="utf-8">
    $(window).load(function() {
      $('.tf-header-slider').flexslider({
        animation: "fade",
        maxItems: 11,
        controlNav: true,
        start: function(slider){
            $('div.flexslider').css({'background':'none'})
        }
      });
    });
    
    $(window).load(function() {
      $('.tf-work-carousel').flexslider({
        animation: "slade",
        animationLoop: false,
        itemWidth: 280,
        itemMargin: 30,
        move: 1,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
    
    $(window).load(function() {
      $('.tf-footer-carousel').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 140,
        itemMargin: 15,
        minItems: 1,
        maxItems: 6,
        move:1
      });
    });
    
    jQuery(document).ready(function($) {
				$('#work-carousel').carouFredSel({
					next : "#work-carousel-next",
					prev : "#work-carousel-prev",
					auto: false,
					circular: false,
					infinite: true,	
					width: '100%',		
					scroll: {
						items : 1					
					}		
				});
			});
  </script>
  <script type="text/javascript" language="javascript" src="js/custom.js"></script>
  <link rel="stylesheet" type="text/css" href="css/popup.css">
  <script type="text/javascript">
     $(document).ready( function() {
    
        $.removeCookie('ccvtut');
    
        $('#popupBoxClose').click( function() {            
            unloadPopupBox();
        });
        
        $('#login').click( function() {
            loadPopupBox();
        });
		
		$('#reges').click( function() {
            loadPopupBox();
        });	
		
		//Gallery PopupBox
		$('#gallery').click( function() {
            loadPopupBox();
        });	
		
		$('#link').click( function() {
            loadPopupBox();
        });
		
        function unloadPopupBox() {    // TO Unload the Popupbox
            $('#popup_box').fadeOut("slow");
            $(".body-wrapper").css({ // this is just for style        
                "opacity": "1"  
            }); 
        }    
        
        function loadPopupBox() {    // To Load the Popupbox
            $('#popup_box').fadeIn("slow");
            $(".body-wrapper").css({ // this is just for style
                "z-index": "0.4" ,
                "filter" : "alpha(opacity=60)",
                "z-index" : "5"
            }); 
        }
        /** Login Box **/
        function loginDialog() {
            $('.modal-title').html('Login');
            $('.modal-body').html('<div style="width:60%; margin: 0 auto;"><form class="form-horizontal" method="post" action="includes/scripts/crownlogin.php"><div class="control-group"><div class="controls"><input id="username" name="username" type="text" placeholder="username" class="input-xlarge" required=""></div></div><div class="control-group"><div class="controls"><input id="password" name="password" type="password" placeholder="password" class="input-xlarge" required=""></div></div><div class="control-group"><div class="controls"><button class="btn btn-success" type="submit" name="login" id="login" value="Login">Login</button><a style="padding: 0 0 0 130px;" href="password-reset.php">Forgot Password?</a></div></div></form></div><div style=" width:100%; padding:10px 0 10px 0; margin: 0 auto;"><hr></div><div><?php if(isset($authUrl)) { echo '<a class="btn btn-block btn-warning" style="color:white;" href=" '.$authUrl.' ">Sign in with Google</a>'; } ?><div style="padding-top:10px;"></div><a class="btn btn-block btn-primary" style="color:white;" href="?login&op=facebook">Sign in with Facebook</a></div>');
            $('.modal-footer').html('<button id="reges" type="button" class="btn btn-primary" style="opacity: 1; ">Register</button><button type="button" class="btn btn-default" data-dismiss="modal" style="opacity: 1; ">Cancel</button>');
            $('#error-modal').modal('show');
        }
        $('#login').click(function(){
            loginDialog();

        });
		
		$('#loginupload').click( function() {
            loginDialog();
            $('input#login-next').attr('value', 'upload');
        });
		
		/** Registration Box **/
        function regesDialog() {
            $('.modal-title').html('Register');
            $('.modal-body').html('<div><form class="form-signin" action="includes/scripts/crownreg.php" method="post" enctype="multipart/form-data"><p><div class="control-group"><input type="text" pattern=".{5,}" title="Minimum of 5 Characters" class="input-large" name="username" placeholder="Username" required /></p><p><input type="text" class="input-large" name="fname" placeholder="First Name" required /></p><p><input type="text" class="input-large" name="lname" placeholder="Last Name" required /></p><p><input type="text" pattern="[^ @]*@[^ @]*" title="**@example.com" class="input-large" name="email" placeholder="Email Address" required /></p><p><select class="row-fluid" name="gender" required ><option value="">*Gender*</option><option value="male">Male</option><option value="female">Female</option></select></p><p><select name="ag" required><option value="">*Age Group*</option><option value="A">24 & Below</option><option value="B">25-29</option><option value="C">30-34</option><option value="D">35-39</option><option value="E">40-44</option><option value="F">45-49</option><option value="G">50-54</option><option value="H">55-59</option><option value="I">60-64</option><option value="J">65 & Above</option></select></p><p><span style="color:green;">***optional</span><input type="text" title="Numbers Only" class="input-large" name="phone" placeholder="Phone Number"></p><p><input type="password" pattern=".{8,}" title="Minimum of 8 Characters" class="input-large" name="password"  placeholder="Password" required /></p><p><input type="password" pattern=".{8,}" title="Minimum of 8 Characters" class="input-large" name="password"  placeholder="Confirm Password" required /></p><p><input type="checkbox" style="width:20px; margin-right:10px;"/><a href="termcon.php" target="_blank" style="color:blue;">Accept Terms & Conditions</a></p><p><button class="btn btn-success" type="submit" value="Login">Register</button></p></form></div>');
            $('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal" style="opacity: 1; ">Cancel</button>');
            $('#error-modal').modal('show');
        }
        $('#reges').live('click', function(){
            regesDialog();

        });
		/**gallery**/
		function galleryDialog() {
            $('.modal-title').html('Select Your Condition');
            $('.modal-body').html('<center><div><img src="images/warm-cool-color-wheel.png" height="50%" width="50%"/></center><center><a class="btn btn-mini" href="conCool.php">Cool Colors</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-mini" href="conNeutral.php">Neutral Colors</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-mini" href="conWarm.php">Warm Colors</a></center>');
            $('#error-modal').modal('show');
        }
        $('#gallery').click(function(){
            galleryDialog();

        });
		
		function linkDialog() {
        $('.modal-title').html('Tutorial');
        $('.modal-body').html('<div ><video poster="images/ccvv.png" width="530" height="350" controls><source src="http://www.crownit.co.ke/vidtut/crown.webm" type="video/webm"><source src="http://www.crownit.co.ke/vidtut/crown.mp4" type="video/mp4"><source src="http://www.crownit.co.ke/vidtut/crown.ogv" type="video/ogv"></video></div>');
        $('#error-modal').modal('show');
		
		}
		 $('#link').click(function(){
            linkDialog();
		});	

        /** click to tranform **/
        $('a [ href^="ccv" ]').click(function(){
            alert('dd');
        });

        $('.fb-login-button').live('click', function(){
            FB.login(function(response){
                console.log(response);
            }, {scope: 'email,user_likes'} );
            $('#error-modal').modal('hide');
        });

        /** Facebook Login **/

        /** tutorial **/
        var index = {
            steps: [
                {
                    element:'#step1',
                    intro: 'Welcome to Crown color visualizer tutorial. <br/> Follow this tutorial to learn how to use the visualizer. <br> Click <b>Quit</b> to end the tutorial',
                    position: ''
                },
                {
                    element:'#step2',
                    intro: 'Select on one of these options. <br/> Select interiors to proceed',
                    position: 'top'
                }
            ]

        };

        var interiors = {
            steps: [
                {
                    element: '#step1',
                    intro: 'Interior scenes slider',
                    position: 'bottom'
                },
                {
                    element:'#step2',
                    intro: 'Select on one of these interior scene categories. <br/> Choose a category to proceed',
                    position: 'top'
                }
            ]
        };

        var livingrooms = {
            steps: [
                {
                    element: '.step1',
                    intro: 'Some livingroom design',
                    position: 'top'
                },
                {
                    element:'.step2',
                    intro: 'Select a livingroom design to transform it',
                    position: 'top'
                }
             ]
        };

        var bedrooms = {
            steps: [
                {
                    element: '#step1',
                    intro: 'Interior scenes slider',
                    position: 'bottom'
                },
                {
                    element:'#step2',
                    intro: 'Select on one of these bedroom scene categories. <br/> Choose a category to proceed',
                    position: 'top'
                }
             ]
       };


        var bathrooms = {
            steps: [
                {
                    element: '#step1',
                    intro: 'Interior scenes slider',
                    position: 'bottom'
                },
                {
                    element:'#step2',
                    intro: 'Select on one of these bathroom scene categories. <br/> Choose a category to proceed',
                    position: 'top'
                }
             ]
       };

        var kitchens = {
            steps: [
                {
                    element: '#step1',
                    intro: 'Interior scenes slider',
                    position: 'bottom'
                },
                {
                    element:'#step2',
                    intro: 'Select on one of these interior scene categories. <br/> Choose a category to proceed',
                    position: 'top'
                }
             ]
       };

       function startIntro(options) {
            var intro = introJs();
            intro.setOption('skipLabel', 'Quit');
            intro.setOption('doneLabel', 'Okay, Got It');
            intro.setOptions(options);
            intro.onexit(function() {
                // set cookie
                $.removeCookie('ccvtut');
                $.cookie('ccvtut', 'done', { expires: 30, path: '/' });;
            });
            intro.start();
       }
       function initIntro(){
       (function(){
            var d1 = window.location.pathname.split('/');
            var d2 = d1[d1.length -1].split('.');
            var ind = d2[0].toLowerCase();
             console.log(ind);
            switch(ind)
            {
                case 'index' : startIntro(index);
                break;
                case 'interiors' : startIntro(interiors);
                break;
                case 'livingrooms' : startIntro(livingrooms);
                break;
                case 'bedrooms' : startIntro(bedrooms);
                break;
                case 'bathrooms' : startIntro(bathrooms);
                break;
                case 'kitchens' : startIntro(kitchens);
                break;
                default: startIntro(index);
                break;

            }

       })();
       };

       var ccv_cookie = $.cookie('ccvtut');
       if(!ccv_cookie) { initIntro();}

       $('#start-tutorial').click(function(){ initIntro(); });
    });
  </script>
</head>
<body>
<div id="fb-root"></div>
<!-- FB Login API-->
<script>
  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '375132582589466',                        // App ID from the app dashboard
      channelUrl : '//www.crownit.co.ke/channel.html', // Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true                                  // Look for social plugins on the page
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!--/ FB Login API -->
    <div class="body-wrapper">
        <div class="white-background">
            <div class="container">
              <div class="header">
                <div class="header-nav box_white">

                    <!--Top Menu-->
                       <div id="topmenu">



                         <div class="header-title-content">

                             <!--logo-->
                             <div class="logo"><a href="index.php"><img src="images/ccvv.png" alt="" /></a></div>
                             <!--/logo-->

                          <!---->

                         <div class="clear"></div>


                       </div>
                       <!--/top menu-->

                  </div>
                    
                <!--Let's Do It--><!--/let's do it--></div>
                 <!--header title--><!--/header title-->
                    
            </div>
                <div class="clear"></div>
        </div>
        
        <!--/header-->