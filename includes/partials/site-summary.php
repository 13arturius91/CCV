<?php

function getWeekVisits() {

    /* the data */
    $sql = "SELECT id, DAY( date_nav ) as day, SUM( (isadmin + 1) ) as num FROM regusers WHERE WEEK( date_nav ) = WEEK( NOW() )";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<div style="border:1px solid #bababa; margin:0 auto 0 auto; background: #f9f9f9; text-align: center; width:150px; padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<h2>' . count($rows) . '</h2>';
            $table .= '<span>logins this week</span>';

        }

        $table .= '</div>';

        echo $table;

    } else {

        $table = '<div style="border:1px solid #bababa; margin:0 auto 0 auto; background: #f9f9f9; text-align: center; width:150px; padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">';
        $table .= '<h2>---</h2>';
        $table .= '<span>logins this week</span>';
        $table .= '</div>';

        echo $table;

    }

}

function getUserCount() {

    /* the data */
    $sql = "SELECT count(*) as num FROM regusers WHERE isadmin = 0";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<div style="border:1px solid #bababa; margin:0 auto 0 auto; background: #f9f9f9; text-align: center; width:150px; padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<h2>' . $rows['num'] . '</h2>';
            $table .= '<span>registered members</span>';

        }

        $table .= '</div>';

        echo $table;

    } else {

        $table = '<div style="border:1px solid #bababa; margin:0 auto 0 auto; background: #f9f9f9; text-align: center; width:150px; padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">';
        $table .= '<h2>---</h2>';
        $table .= '<span>registered members</span>';
        $table .= '</div>';

        echo $table;

    }

}

function getAgeCount() {

    /* the data */
    $sql = "SELECT regusers.age_group_id, COUNT( * ) AS num, tbl_age_group.group_range FROM regusers, tbl_age_group WHERE tbl_age_group.group_id = regusers.age_group_id AND isadmin =0 GROUP BY age_group_id ORDER BY num DESC LIMIT 1";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<div style="border:1px solid #bababa; margin:0 auto 0 auto; background: #f9f9f9; text-align: center; width:150px; padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<h2>' . $rows['group_range'] . '</h2>';
            $table .= '<span>dominant age</span>';

        }

        $table .= '</div>';

        echo $table;

    } else {

        $table = '<div style="border:1px solid #bababa; margin:0 auto 0 auto; background: #f9f9f9; text-align: center; width:150px; padding: 10px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">';
        $table .= '<h2>---</h2>';
        $table .= '<span>dominant age</span>';
        $table .= '</div>';

        echo $table;

    }

}

function getAgeDrillDown() {

    /* the data */
    $sql = "SELECT regusers.age_group_id, COUNT(*) AS num, tbl_age_group.group_range FROM regusers, tbl_age_group WHERE tbl_age_group.group_id = regusers.age_group_id AND isadmin =0 GROUP BY age_group_id ORDER BY num DESC";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<table class="table table-bordered">';
        $table .= '<thead><tr><th>Age group</th><th>Number</th></tr></thead>';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<tr>';
            $table .= '<td>' . $rows['group_range'] . '</td>';
            $table .= '<td>' . $rows['num'] . '</td>';
            $table .= '</tr>';

        }

        $table .= '</table>';

        echo $table;

    } else {

        $table .= '';

        echo $table;

    }

}

function getMaleCount() {

    /* the data */
    $sql = "SELECT count(gender) as m FROM regusers WHERE gender = 'male' AND isadmin = 0";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<tr>';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<td>Male</td>';
            $table .= '<td>' . $rows['m'] . '</td>';
        }

        $table .= '</tr>';

        echo $table;

    } else {

        print_r('error');

    }

}

function getFemaleCount() {

    /* the data */
    $sql = "SELECT count(gender) as f FROM regusers WHERE gender = 'female' AND isadmin = 0";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<tr>';

        while ($rows = mysql_fetch_assoc($res)) {

            $table .= '<td><span>Female</span></td>';
            $table .= '<td><span>' . $rows['f'] . '</span></td>';
        }

        $table .= '</tr>';

        echo $table;

    } else {

        print_r('error');

    }

}

?>
<link rel="stylesheet" href="css/bootstrap.min-full.css" >
<link rel="stylesheet" href="css/jquery.dataTables.css" >
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){



});
</script>

<span class="pull-right" style="margin:5px 10px 10px 0;"><?php echo date('d M, Y') ?></span>

<hr style="border:1px solid #f9f9f9;">

<div style="margin:0;">

    <div class="span4">
        <?php getWeekVisits() ?>
    </div>

    <div class="span4">
        <?php getUserCount() ?>
        <br>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Gender</th>
                    <th>Number</th>
                </tr>
            </thead>
            <tbody>
                <?php getFemaleCount(); getMaleCount(); ?>
            </tbody>
        </table>
    </div>

    <div class="span4" style=" -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0 10px 0 0;">
        <?php getAgeCount(); ?>
        <br>
        <?php getAgeDrillDown(); ?>
    </div>

    <div class="clearfix"></div>
</div>

<hr style="border:1px solid #f9f9f9;">