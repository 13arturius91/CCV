<?php

function getUsers() {

    /* the data */
    $sql = "SELECT regusers.username, regusers.fname, regusers.lname, regusers.gender, regusers.phone, regusers.email, regusers.age_group_id, regusers.date_nav FROM regusers WHERE isadmin != 1";

    $res = mysql_query($sql) or die( mysql_error() );

    $num = mysql_num_rows($res);

    if ($num > 0) {

        $table = '<table id="user-table" class="table table-condensed table-striped"><thead>';
        $table .= '<tr><th>First Name</th><th>Last Name</th><th>Gender</th><th>Age Group</th><th>Phone</th><th>Email</th><th>Last Login</th></tr>';
        $table .= '</thead><tbody>';

        while($rows = mysql_fetch_assoc($res)) {

            $table .= '<tr>';
            //$table .= '<td>' . $rows['username'] . '</td>';
            $table .= '<td>' . $rows['fname'] . '</td>';
            $table .= '<td>' . $rows['lname'] . '</td>';
            $table .= '<td>' . $rows['gender'] . '</td>';
            $table .= '<td>' . $rows['age_group_id'] . '</td>';
            $table .= '<td>' . $rows['phone'] . '</td>';
            $table .= '<td>' . $rows['email'] . '</td>';
            $date = date_create($rows['date_nav']);
            $dat = $date->format('d M, Y');
            $table .= '<td>' . $dat. '</td>';
            $table .= '</td>';

        }

        $table .= '</tbody></table>';

        echo $table;

    } else {

        print_r('error');

    }

}


?>
<link rel="stylesheet" href="css/bootstrap.min-full.css" >
<link rel="stylesheet" href="css/jquery.dataTables.css" >
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){

    $("#user-table").dataTable();

});
</script>

<div style="margin: 0 8px 15px 8px;">
    <span class="pull-left">
        <p style="text-decoration: underline; text-transform: uppercase;">A list of registered users</p>
    </span>
    <span class="pull-right">Generated on: <?php echo date('d M, Y h:i a') ?></span>
    <div class="clearfix"></div>
</div>

<hr style="border:1px solid #f9f9f9;">

<div>
    <a href="engine.php?do=export-users" class="btn pull-right"><i class="icon-download-alt"></i> Export as Excel</a>
    <div class="clearfix"></div>
</div>

<hr style="border:1px solid #f9f9f9;">

<?php getUsers() ?>