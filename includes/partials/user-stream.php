<?php

/* start the session */
@session_start();

/* db connection */
//include '../dao/config.php';

/* params */
$limit = (isset($_REQUEST['limit'])) ? mysql_real_escape_string($_REQUEST['limit']) : 'NULL' ;
$last_id = (isset($_REQUEST['last_id'])) ? mysql_real_escape_string($_REQUEST['last_id']) : 'NULL' ;

/* logged out */
if (count($_SESSION) == 0) {

    $status['status'] = 0;
    $status['message'] = 'Please log in';
    $status['payload'] = array();

    echo json_encode($status);
    exit;

}

/* Home page feed - functions */

/**
 * Get the projects of the people the user id following
 * @param $user_id
 */
function getUserFeed($user_id, $last_id, $limit) {

    /* last id */
    $last_id_string = ($last_id != '') ? "AND tbl_images.id < $last_id" : '';

    /* user's following */
    $following = (listFollowingIds($user_id)) ? listFollowingIds($user_id) : '0';

    /* output */
    $stream = '';

    /* if the user follows people */
    if ($following) {

    /* limit */
    $limit = (isset($limit)) ? $limit : 0;

    /* their projects */
    $sql = "SELECT tbl_images.id, tbl_images.image_privacy, tbl_images.user_id, tbl_images.image_filename, tbl_images.image_colors, tbl_images.image_fav, tbl_images.image_timestamp FROM tbl_images WHERE image_privacy = 1 AND tbl_images.user_id IN ($following) $last_id_string ORDER BY tbl_images.id DESC LIMIT $limit";

    $ret = mysql_query($sql) or die( 'getUserFeed query failed' );

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');

            $comments_count = getCommentsCount($data['id']);

            $project_likes_count = getLikeCount($data['id']);

            $has_liked = hasUserLiked($user_id, $data['id']);
            $like_text = ($has_liked == 1) ? 'unlike' : 'like';
            $like_link = ($has_liked == 1) ? 'unlike-project' : 'like-project';

            $has_comments = hasProjectComments($data['id']);
            $comment_toggle = ($has_comments == 0) ? '' : 'none';


            $colors = fetchColorsP($data['image_colors']);

            $project_privacy = ($data['image_privacy'] == 0) ? '<i class="icon-lock"></i>' : '';
            $stream .=  '<div class="stream-item" data-item-id="' . $data['id'] . '">' .
                            '<div class="stream-side-bar">' .
                                '<div class="stream-item-user-profile-image"><img src="pro_pics/' . $user_data[0]['image'] . '" /></div>' .
                                '<div class="stream-item-colors">' . $colors .
                                '</div>' .
                            '</div>' .
                            '<div class="stream-item-project-item">' .
                                '<div class="stream-item-project-user-details">' .
                                    '<a href="#" data-uid="' . $user_data[0]['id'] . '" class="stream-item-user-fullname">' . $user_data[0]['full_name'] . '</a>' .
                                    '<span class="stream-item-user-username"> (' . $user_data[0]['username'] . ') </span>' .
                                    '<span class="stream-item-user-username"> &middot; ' . timeAgo(strtotime($data['image_timestamp'])) . '</span>' .
                                    '<span class="pull-right">' . $project_privacy . '</span>' .
                                '</div>' .
                                '<div class="stream-item-project-image">' .
                                    '<img src="uploads/' . $data['image_filename'] . '" width="100%">' .
                                    '<div class="clear"></div>' .
                                    '<div class="stream-item-links">' .
                                        '<a id="' . $like_link . '" href="#" data-item-id="' . $data['id'] . '" data-item-count="' . $project_likes_count[0]['likes_count'] . '" >' . $like_text . '</a>' .
                                        ' &middot; ' . '<a id="comment-project" href="#" data-item-id="' . $data['id'] . '">show comments</a>' .
                                    '</div>' .
                                    '<div class="stream-item-details">' .
                                        '<span class="stream-item-likes-count" data-item-id="' . $data['id'] . '">' . $project_likes_count[0]['likes_count'] .' likes</span>' .
                                        '&nbsp;&nbsp;&nbsp;' .
                                        '<span class="stream-item-comments-count" data-item-id="' . $data['id'] . '">' . $comments_count[0]['comments_count'] . ' comments</span>' .
                                    '</div>' .
                                    '<div class="stream-item-comments">' .
                                        '<div class="stream-item-comments-list-' . $data['id'] . '"></div>' .
                                        '<form id="comment-post-form-' . $data['id'] . '" style="display:' . $comment_toggle . ';">' .
                                        '<div class="stream-comment-box">' .
                                            '<div class="stream-comment-user-pic" >' .
                                                '<img src="pro_pics/' . $user_data[0]['image'] . '" width="42" height="42">' .
                                            '</div>' .
                                            '<textarea class="stream-comment-box-textarea" data-project-id="' . $data['id'] . '" placeholder="Write a comment" aria-expanded="true"></textarea>' .
                                        '</div>' .
                                        '<input id="project-id" name="project-id" type="hidden" value="' . $data['id'] . '" >' .
                                        '<input id="do" name="do" type="hidden" value="' . $data['id'] . '" >' .
                                        '</form>' .
                                    '</div>' .
                                '</div>' .
                            '</div>' .
                        '</div>';

        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        if (!$following) {

            $stream .= '<p style="margin:50px 35% 0 35%;">You are not following anyone yet</p>';

        } else {

            $stream .= '<p style="margin:50px 35% 0 35%;">No public projects have been found</p>';

        }

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    } else {

        $stream .= '<p style="margin:50px 35% 0 35%;">No public projects have been found</p>';

    }

    echo $stream;

}

function getUserProjects($user_id) {

    /* their projects */
    $sql = "SELECT tbl_images.id, tbl_images.user_id, tbl_images.image_filename, tbl_images.image_colors, tbl_images.image_fav, tbl_images.image_timestamp FROM tbl_images WHERE tbl_images.user_id = $user_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    $stream = '';

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $user_data = getUserData($data['user_id'], 'ret');

            $comments_count = getCommentsCount($data['id']);

            $project_likes_count = getLikeCount($data['id']);

            $has_liked = hasUserLiked($user_id, $data['id']);
            $like_text = ($has_liked == 1) ? 'unlike' : 'like';
            $like_link = ($has_liked == 1) ? 'unlike-project' : 'like-project';

            $has_comments = hasProjectComments($data['id']);
            $comment_toggle = ($has_comments == 0) ? '' : 'none';


            $colors = fetchColorsP($data['image_colors']);

            $stream .=  '<div class="stream-item" data-item-id="' . $data['id'] . '">' .
                '<div class="stream-side-bar">' .
                '<div class="stream-item-user-profile-image" style="background: url(pro_pics/' . $user_data[0]['image'] . ')" ></div>' .
                '<div class="stream-item-colors">' . $colors .
                '</div>' .
                '</div>' .
                '<div class="stream-item-project-item">' .
                '<div class="stream-item-project-user-details">' .
                '<span class="stream-item-user-fullname">' . $user_data[0]['full_name'] . '</span>' .
                '<span class="stream-item-user-username"> (' . $user_data[0]['username'] . ') </span>' .
                '<span class="stream-item-user-username"> &middot; ' . timeAgo(strtotime($data['image_timestamp'])) . '</span>' .
                '</div>' .
                '<div class="stream-item-project-image">' .
                '<img src="uploads/' . $data['image_filename'] . '" width="100%">' .
                '<div class="clear"></div>' .
                '<div class="stream-item-links">' .
                '<a id="' . $like_link . '" href="#" data-item-id="' . $data['id'] . '" data-item-count="' . $project_likes_count[0]['likes_count'] . '" >' . $like_text . '</a>' . ' &middot; ' .
                '<a id="comment-project" href="#" data-item-id="' . $data['id'] . '" data-item-count="' . $comments_count[0]['comments_count'] . '">comment</a>' .
                '</div>' .
                '<div class="stream-item-details">' .
                '<span class="stream-item-likes-count" data-item-id="' . $data['id'] . '">' . $project_likes_count[0]['likes_count'] .' likes</span>' .
                '&nbsp;&nbsp;&nbsp;' .
                '<span class="stream-item-comments-count" data-item-id="' . $data['id'] . '">' . $comments_count[0]['comments_count'] . ' comments</span>' .
                '</div>' .
                '<div class="stream-item-comments">' .
                    '<div class="stream-item-comments-list-' . $data['id'] . '"></div>' .
                                        '<form id="comment-post-form-' . $data['id'] . '" style="display:' . $comment_toggle . ';">' .
                                        '<div class="stream-comment-box">' .
                                            '<div class="stream-comment-user-pic" >' .
                                                '<img src="pro_pics/' . $user_data[0]['image'] . '" width="42" height="42">' .
                                            '</div>' .
                                            '<textarea class="stream-comment-box-textarea" data-project-id="' . $data['id'] . '" placeholder="Write a comment" aria-expanded="true"></textarea>' .
                                        '</div>' .
                                        '<input id="project-id" name="project-id" type="hidden" value="' . $data['id'] . '" >' .
                                        '<input id="do" name="do" type="hidden" value="' . $data['id'] . '" >' .
                                        '</form>' .
                '</div>' .
                '</div>' .
                '</div>' .
                '</div>';


        }

        $status['status'] = 1;
        $status['message'] = 'Success';
        $status['payload'] = $return;

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    /* echo json_encode($status); */
    echo $stream . '<div class="stream-item" id="stream-ajax-loader">' . '<a href="#" class="stream-item-project-item">load more</a>' . '</div>';

}

/* Get user data */
function getUserData($userid, $mode) {

    $sql = "SELECT regusers.id, regusers.username, CONCAT(regusers.fname, ' ', regusers.lname) as full_name, pro_images.image FROM regusers, pro_images WHERE regusers.id = pro_images.acc_id AND regusers.id = $userid ";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

        if (count($return) > 0) {

            $status['status'] = 1;
            $status['message'] = 'Success';
            $status['payload'] = $return;

        } else {

            $status['status'] = 0;
            $status['message'] = 'Failure';
            $status['payload'] = array('The user does not exist');

        }

    } else {

        $status['status'] = 0;
        $status['message'] = 'Failure';
        $status['payload'] = $return;

    }

    if ($mode == 'echo') {

        echo json_encode($status);

    } else if ($mode == 'ret') {

        return $return;
    }

}

/**
 * List people the user is following
 * @param $user_id
 */
function listFollowingIds($user_id) {

    $sql = "SELECT regusers.id FROM regusers, tbl_following WHERE regusers.id = tbl_following.following_id AND tbl_following.user_id = $user_id";

    $ret = mysql_query($sql) or die('listFollowingIds query failed ');

    $return = '';

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return .= "'" . $data['id'] . "',";

        }

    } else {

        $return = 'No following Ids,';

    }

    return  substr($return, 0, -1);

}

/**
 * Get the number of likes a project has
 * @param $project_id
 */
function getLikeCount($project_id) {

    $sql = "SELECT count(id) as likes_count FROM tbl_likes WHERE project_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

    } else {

        $return[] = 0;

    }

    return $return;

}

/**
 * Get the number of comments a project has
 * @param $project_id
 */
function getCommentsCount($project_id) {

    $sql = "SELECT count(id) as comments_count FROM tbl_comments WHERE image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $return = array();

    if ($ret) {

        while ($data = mysql_fetch_assoc($ret)) {

            $return[] = $data;

        }

    } else {

        $return[] = 0;

    }

    return $return;

}

/**
 * Fetch color details
 * @params $color String color string
 * @return $colorData Element Html color elements
 *
 */
function fetchColorsP($colors) {
    /*
        get an array of section info,
        we could remove the last element here, its blank.
    */
    $dat = explode(';', trim($colors) );
    /* initialize the conditional? */
    $where = '';

    /*
        loop through the section info array,
        notice that the array has one empty item,
        (count($dat) - 1)
    */
    for($i = 0; $i < (count($dat) - 1); $i++ ){
        /* split the section info item into an array */
        $code = explode(',', $dat[$i]);

        /* if the section info array has only one item */
        if( count($dat) == 2) {
            /* we're done building the conditional */
            $where .= "code = '$code[2]' ";

        } else {

            if($i == (count($dat) - 2)  ) {
                /* if its the last item in the array, we're done */
                $where .= "code = '$code[2]' ";

            } else {
                /* if its not the last item, we're not done yet. add an OR to the conditional  */
                $where .= "code = '$code[2]' OR ";

            }

        }

    }

    /* build the query and add our conditional */
    $sql = "SELECT name, code, hex FROM tbl_colors WHERE ". $where;
    /* execute the query */
    $res = mysql_query($sql);

    $color_data = '<ul>';

    /* if the query returned a resource object, */
    if ($res) {
        /* get the number of rows returned */
        $count = mysql_num_rows($res);
        /* if the number of greater than one */
        if ( $count > 0 ) {
            /* loop through the rows, and build the list element */
            while ( $row = mysql_fetch_assoc($res) ) {

                $color_data .= '<li>';
                $color_data .= '<a class="box" data-target="#" title="' . $row['name'] .  $row['code'] . '" data-name="' . $row['name'] . '" data-code="' . $row['code'] . '" style="background:#'. trim($row['hex']) . '; "></a>';
                $color_data .= '</li>';

            }

        }

    } else {

        $color_data .= '<li></li>';

    }

    return $color_data;

}

/**
 * Check if a user has liked a project
 * @param $user_id      user id
 * @param $project_id   project id
 * @return int          1 has, 0 hasn't
 */
function hasUserLiked($user_id, $project_id) {

    $sql = "SELECT id FROM tbl_likes WHERE user_id = $user_id AND project_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = mysql_num_rows($ret);

    return $num;

}

/**
 * Check if a user has commented on a project
 * @param $user_id      user id
 * @param $project_id   project id
 * @return int          1 has, 0 hasn't
 */
function hasUserCommented($user_id, $project_id) {

    $sql = "SELECT id FROM tbl_comments WHERE user_id = $user_id AND image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = mysql_num_rows($ret);

    return $num;

}

/**
 * Check if a project has comments
 * @param $project_id   project id
 * @return int          1 has, 0 hasn't
 */
function hasProjectComments($project_id) {

    $user_id = $_SESSION['id'];

    $sql = "SELECT id FROM tbl_comments WHERE image_id = $project_id";

    $ret = mysql_query($sql) or die(mysql_error());

    $num = mysql_num_rows($ret);

    return $num;

}

/**
 * Prettify time difference
 * @param $time timestamp
 * @return string pretty time
 */
function timeAgo($time) {
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60","60","24","7","4.35","12","10");

    $now = time();

    $difference = $now - $time;
    $tense = "ago";

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
        $periods[$j].= "s";
    }
    $return = "$difference $periods[$j] ago";
    if($return=="0 seconds ago"){
        return 'Just Now';
    }
    return $return;
}


/* change tbl_images image_timestamp -> timestamp */
?>

