<?php

include_once 'includes/dao/config.php';


/**
 * Fetches a user's saved image data
 * @return $data mysql result object
 */
function fetchData() {

    $user_id = ( isset($_SESSION['id']) ) ?  $_SESSION['id'] : '0' ;

    $sql = "SELECT id, image_id, image_filename, image_colors, image_timestamp FROM tbl_images WHERE user_id = '$user_id' AND image_fav = 1 ORDER BY id DESC";

    $res = mysql_query($sql);
    $count = mysql_num_rows($res);

    if ($count > 0) {

        return $res;

    } else {

        $data = 'no data';
        return $data;
    }

}

/**
 * Fetch color details
 * @params $color String color string
 * @return $colorData Element Html color elements
 *
 */
function fetchColors($colors) {
    /*
        get an array of section info,
        we could remove the last element here, its blank.
    */
    $dat = explode(';', trim($colors) );
    /* initialize the conditional? */
    $where = '';

    /*
        loop through the section info array,
        notice that the array has one empty item,
        (count($dat) - 1)
    */
    for($i = 0; $i < (count($dat) - 1); $i++ ){
        /* split the section info item into an array */
        $code = explode(',', $dat[$i]);

        /* if the section info array has only one item */
        if( count($dat) == 2) {
            /* we're done building the conditional */
            $where .= "code = '$code[2]' ";

        } else {

            if($i == (count($dat) - 2)  ) {
                /* if its the last item in the array, we're done */
                $where .= "code = '$code[2]' ";

            } else {
                /* if its not the last item, we're not done yet. add an OR to the conditional  */
                $where .= "code = '$code[2]' OR ";

            }

        }

    }

    /* build the query and add our conditional */
    $sql = "SELECT name, code, hex FROM tbl_colors WHERE ". $where;
    /* execute the query */
    $res = mysql_query($sql);
    /* if the query returned a resource object, */
    if ($res) {
        /* get the number of rows returned */
        $count = mysql_num_rows($res);
        /* if the number of greater than one */
        if ( $count > 0 ) {
            /* loop through the rows, and build the list element */
            while ( $row = mysql_fetch_assoc($res) ) {

                echo '<li>';
                echo '<a class="box" data-target="#" title="' . $row['name'] .  $row['code'] . '" data-name="' . $row['name'] . '" data-code="' . $row['code'] . '" style="background:#'. trim($row['hex']) . '; "></a>';
                echo '</li>';

            }

        }

    } else {
        /* if no resource object was returned, the colors are not in the db */
        $data = '<li><span>No Colors Found</span></li>';
        return $data;

    }

}

/**
 * Renders the data to html
 * @param $data mysql result object
 */
function renderHtml($data) {

    /* if no data is passed, the user has no favorites */
    if ( $data ==  'no data' ) {

        echo '<br><center><p>You have no favorites yet</p></center>';

    } else {

        /* loop through the results... */
        while ( $row = mysql_fetch_assoc($data) ) {
            /* ...and construct the html to be inserted into the page  */
            echo '<div class="data-cont">';
            echo '<div class="image-cont">';
            echo '<div class="menu-cont hidden">';
            echo '<a id="delete-image" data-target="#" data-image-id="' . $row['image_filename'] . '" class="image-trash"><i class="icon-white icon-trash"></i>Delete</a>';
            echo '</div>';
            echo '<a data-target="#"><img src="' . 'uploads/' . $row['image_filename'] .'" height="245" /></a>';
            echo '</div>';
            echo '<div class="details-cont">';
            echo '<span>colors</span>';
            echo '<ul>';
            echo fetchColors( $row['image_colors'] );
            echo '<div class="clearfix"></div>';
            echo '</ul>';
            echo '</div>';
            echo '<div class="clearfix"></div>';
            echo '</div>';
        }

    }

}

$data = fetchData();
renderHtml($data);

?>
<style type="text/css">

</style>
