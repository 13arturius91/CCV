﻿<?php

    @session_start();
    $logged_in = (isset($_SESSION['id'])) ? 'true' : 'false' ;

    $imageDir = 'assets/';
    $req = (isset($_REQUEST['p'])) ? $_REQUEST['p'] : $imageDir . 'living-room-01.png';

    $image = $req;
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
<title>Crown Color Visualizer</title>

<meta name="author" content="Francis Muturi, Simon Ngunjiri Muraya" >
<meta name="copyright" content="Copyright - 2015"/>
<meta name="keywords" content="Crown paints, crown color visualizer">

<meta property="og:title" content="Crown Color Visualiser">
<meta property="og:image"  content="http://www.crownit.co.ke/assets/<?php echo $req ?>" >
<meta property="og:url"    content="http://www.crownnit.co.ke/ccv.php?p=<?php echo $req ?>" />
<meta property="og:description"  content="Crown Color Visualizer">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link rel="stylesheet" href="css/jquery.jscrollpane.css">
<link rel="stylesheet" href="css/miniTip.min.css">
<link href="style.css" media="screen" rel="stylesheet">
<!-- Bootstrap css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/ccvv-screen.css" media="screen" rel="stylesheet">
<link href="style.css" media="screen" rel="stylesheet">
<link href="screen.css" media="screen" rel="stylesheet">
<link href="css/introjs.css" media="screen" rel="stylesheet">

<!-- custom CSS -->
<link rel="stylesheet" href="custom.css">

<!-- favicon.ico and apple-touch-icon.png -->
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>
if (!window.jQuery) {
    document.write('<script src="js/libs/jquery.min.js"><\/script>');
}
</script>
<!-- CCV scripts -->
<script type="text/javascript" src="js/jquery.miniTip.min.js"></script>
<script type="text/javascript" src="js/jquery.jscrollpane.min.js" ></script>
<script type="text/javascript" src="js/jquery.mousewheel.js" ></script>
<script type="text/javascript" src="js/jquery.cookie.js" ></script>
<script type="text/javascript" src="js/kinetic-v4.6.0.min.js"></script>
<script type="text/javascript" src="js/helpers.color.js" ></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- typeahead -->
<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
<!-- JsPDF -->
<script type="text/javascript" src="js/jspdf.min.js"></script>
<!-- Intro JS -->
<script type="text/javascript" src="js/intro.js"></script>
<script type="text/javascript" src="js/appv.js"></script>
<script type="text/javascript">
$(function() {
	/** App settings **/
    	var settings = {
    		app : {
    			panel : $('.panel'),
    			indicator : $('.indicator'),
    			boxa: $('a.box'),
                boxi: $('.box img'),
                box: $('.box'),
                dropper: $('#dropper'),
                debug: '',
    			palletteSelect: $('#charttypes'),
                baseURL: '',
    			palletteScript : 'data.php',
                dataDir: 'points',
                polyDir: 'polys',
                image: <?php echo "'" . $image . "'"; ?>,
                username: <?php echo @(isset($_SESSION["username"])) ? '"' . $_SESSION['username'] . '"' : '""' ; ?>
    		},
    		jscrollpane : {
    			showArrows: true,
    			autoReinitialise: true,
    			autoReinitialiseDelay: 100
    		},
    		kinetic : {
    			container: 'app-canvas',
       		    width: 740,
       		    height: 500,
       		    listening: true
       	}
       };

       /** Initialise the app **/
       app.initialise(settings);

       /** search autocomplete **/
    $('.search').autoComplete({
        minChars: 1,
        source: function(term, response) {
            try { xhr.abort(); } catch(e) { }
            var xhr = $.getJSON('walk.php', 
                { q: term},
                function(data) {
                    response(data);
                });
        },
        delay: 1,
        cache: 0,
        renderItem: function(item, search) {
            
            var name = item.name;
            var code = item.code;

            var ret = '<a href="#" id="box" class="box" data-name="' + name + '" data-code="' + code + '" style="background-color: rgb(' + item.rgb + '); color: black !important; text-shadow: 1px 0px 2px white !important;">' + 
                name + ' ' + code + '</a>';

            return ret;
        }
    });

        /** tutorial **/
        var ccv = {
            steps: [
                {
                    element: '.step1',
                    intro: 'This is the Color Visualizer panel. <br/>',
                    position: 'top'
                },
                {
                    element:'.step2',
                    intro: 'This is the stage. <br/> This is where the scene you select is loaded.',
                    position: 'right'
                },
                {
                    element:'.step3',
                    intro: 'This is the color palette. <br/> Scroll down to view more colors. <br/> Now click on a color to select it. ',
                    position: 'left'
                },
                {
                    element:'.step4',
                    intro: 'This is the color you just selected.',
                    position: 'top'
                },
                {
                    element:'.step5',
                    intro: 'This is the name of the color',
                    position: 'top'
                },
                {
                    element:'.step6',
                    intro: 'This is the code of the color',
                    position: 'top'
                },
                {
                    element:'.step7',
                    intro: 'This is the recent colors panel. <br/> It contains the last five colors you select.',
                    position: 'top'
                },
                {
                    element:'.step2',
                    intro: 'Back to the stage. <br/> Move your mouse cursor around in the stage. <br/> Notice that some sections are highlighted. <br/> You can only tranform these sections. <br/> Click on a section to transform it.',
                    position: 'right'
                },
                {
                    element:'.step8',
                    intro: 'This is the refresh tool. <br/> Select it to clear any tranformations done to the scene loaded on the stage.',
                    position: 'left'
                },
                {
                    element:'.step9',
                    intro: 'The Home button <br/> Select to go to the homepage',
                    position: 'bottom'
                },
                {
                    element:'.step10',
                    intro: 'The back button <br/> Select to navigate backwards',
                    position: 'bottom'
                }
            ]
        };
		
		$('#login').click( function() {
            loadPopupBox();
        });
		
		$('#reges').click( function() {
            loadPopupBox();
        });	
		
		//Gallery PopupBox
		$('#gallery').click( function() {
            loadPopupBox();
        });	
		
		$('#ovv').click( function() {
            loadPopupBox();
        });
		
		$('#prov').click( function() {
            loadPopupBox();
        });
		
		function unloadPopupBox() {    // TO Unload the Popupbox
            $('#popup_box').fadeOut("slow");
            $(".body-wrapper").css({ // this is just for style        
                "opacity": "1"  
            }); 
        }    
        
        function loadPopupBox() {    // To Load the Popupbox
            $('#popup_box').fadeIn("slow");
            $(".body-wrapper").css({ // this is just for style
                "z-index": "0.4" ,
                "filter" : "alpha(opacity=60)",
                "z-index" : "5"
            }); 
        }
		/** Login Box **/
        function loginDialog() {
            $('.modal-title').html('Login');
            $('.modal-body').html('<div style="width:60%; margin: 0 auto;"><form class="form-horizontal" method="post" action="includes/scripts/crownlogin.php"><div class="control-group"><div class="controls"><input id="username" name="username" type="text" placeholder="username" class="input-xlarge" required=""></div></div><div class="control-group"><div class="controls"><input id="password" name="password" type="password" placeholder="password" class="input-xlarge" required=""></div></div><div class="control-group"><div class="controls"><button class="btn btn-success" type="submit" name="login" id="login" value="Login">Login</button><a style="padding: 0 0 0 130px;" href="password-reset.php">Forgot Password?</a></div></div></form></div><div style=" width:100%; padding:10px 0 10px 0; margin: 0 auto;"><hr></div><div><?php if(isset($authUrl)) { echo '<a class="btn btn-block btn-warning" style="color:white;" href=" '.$authUrl.' ">Sign in with Google</a>'; } ?><div style="padding-top:10px;"></div><a class="btn btn-block btn-primary" style="color:white;" href="?login&op=facebook">Sign in with Facebook</a></div>');
            $('.modal-footer').html('<button id="reges" type="button" class="btn btn-primary" style="opacity: 1; ">Register</button><button type="button" class="btn btn-default" data-dismiss="modal" style="opacity: 1; ">Cancel</button>');
            $('#error-modal').modal('show');
        }
        $('#login').click(function(){
            loginDialog();

        });
		
		/** Registration Box **/
        function regesDialog() {
            $('.modal-title').html('Register');
            $('.modal-body').html('<div><form class="form-signin" action="includes/scripts/crownreg.php" method="post" enctype="multipart/form-data"><p><div class="control-group"><input type="text" pattern=".{5,}" title="Minimum of 5 Characters" class="input-large" name="username" placeholder="Username" required /></p><p><input type="text" class="input-large" name="fname" placeholder="First Name" required /></p><p><input type="text" class="input-large" name="lname" placeholder="Last Name" required /></p><p><input type="text" pattern="[^ @]*@[^ @]*" title="**@example.com" class="input-large" name="email" placeholder="Email Address" required /></p><p><select class="row-fluid" name="gender" required ><option value="">*Gender*</option><option value="male">Male</option><option value="female">Female</option></select></p><p><select name="ag" required><option value="">*Age Group*</option><option value="A">24 & Below</option><option value="B">25-29</option><option value="C">30-34</option><option value="D">35-39</option><option value="E">40-44</option><option value="F">45-49</option><option value="G">50-54</option><option value="H">55-59</option><option value="I">60-64</option><option value="J">65 & Above</option></select></p><p><span style="color:green;">***optional</span><input type="text" title="Numbers Only" class="input-large" name="phone" placeholder="Phone Number"></p><p><input type="password" pattern=".{8,}" title="Minimum of 8 Characters" class="input-large" name="password"  placeholder="Password" required /></p><p><input type="password" pattern=".{8,}" title="Minimum of 8 Characters" class="input-large" name="password"  placeholder="Confirm Password" required /></p><p><input type="checkbox" style="width:20px; margin-right:10px;"/><a href="termcon.php" target="_blank" style="color:blue;">Accept Terms & Conditions</a></p><p><button class="btn btn-success" type="submit" value="Login">Register</button></p></form></div>');
            $('.modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal" style="opacity: 1; ">Cancel</button>');
            $('#error-modal').modal('show');
        }
        $('#reges').live('click', function(){
            regesDialog();

        });
		/**gallery**/
		function galleryDialog() {
            $('.modal-title').html('Select Your Condition');
            $('.modal-body').html('<center><div><img src="images/warm-cool-color-wheel.png" height="50%" width="50%"/></center><center><a class="btn btn-mini" href="conCool.php">Cool Colors</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-mini" href="conNeutral.php">Neutral Colors</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-mini" href="conWarm.php">Warm Colors</a></center>');
            $('#error-modal').modal('show');
        }
        $('#gallery').click(function(){
            galleryDialog();

        });
		
		function ovv() {
        $('.modal-title').html('Tutorial');
        $('.modal-body').html('<div><video poster="images/poster.png" width="530" height="350" controls><source src="http://www.crownit.co.ke/vidtut/crown.webm" type="video/webm"><source src="http://www.crownit.co.ke/vidtut/crown.mp4" type="video/mp4"><source src="http://www.crownit.co.ke/vidtut/crown.ogv" type="video/ogv"></video></div>');
        $('#error-modal').modal('show');
		
		}
		 $('#ovv').click(function(){
            ovv();
		});
		
		function prov() {
        $('.modal-title').html('Tutorial');
        $('.modal-body').html('<div><video poster="images/poster.png" width="530" height="350" controls><source src="http://www.crownit.co.ke/vidtut/crown_profile.mp4" type="video/mp4"></video></div>');
        $('#error-modal').modal('show');
		
		}
		 $('#prov').click(function(){
            prov();
		});
		
        function startIntro(options) {
            var intro = introJs();
            intro.setOption('skipLabel', 'Quit');
            intro.setOption('doneLabel', 'Okay, Got It');
            intro.setOptions(options);
            intro.oncomplete(function() {
                // set cookie
                $.removeCookie('ccvtut');
                $.cookie('ccvtut', 'done', { expires: 30, path: '/' });;
            });
            intro.start();
            intro.start();
       }
       function initIntro(){
       (function(){
            var d1 = window.location.pathname.split('/');
            var d2 = d1[d1.length -1].split('.');
            var ind = d2[0].toLowerCase();

            switch(ind)
            {
                case 'ccv' : startIntro(ccv);
                break;
                default: startIntro(ccv);
                break;

            }

       })();
       };

       var ccv_cookie = $.cookie('ccvtut');
       if(!ccv_cookie) {

            initIntro();

       }

       $('#start-tutorial').click(function(){

           initIntro();

       });

       /** share popover **/
       var config = {
            title: 'Share via Social Media',
            html: true,
            placement: 'bottom',
            content: '<div style="width:175px;">' +
                '<div class="btn-toolbar"><div class="btn-group"><a class="btn share-action" id="facebook" data-href="#" ><img src="images/social/facebook.png" width="18" /></a><a class="btn share-action" id="twitter" data-target="#"><img src="images/social/twitter.png" width="18" /></i></a><a class="btn share-action" id="linkedin" data-target="#"><img src="images/social/linkedin.png" width="18" /></a><a class="btn share-action" id="google" data-target="#"><img src="images/social/googleplus.png" width="18" /></a></div></div>' +
                '</div>'
       };
       $('.action-share').popover(config);

        /** account popover **/
        var config = {
            title: "<?php echo (isset($_SESSION['username'])) ? $_SESSION['username'] : 'Account' ; ?>",
            html: true,
            placement: 'bottom',
            content: '<div style="width:126px;"><a class="btn action-save-profile" style="text-align: none;" data-target="#"><i class="icon-star"></i> Favorite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br><a class="btn" style="text-align: none; margin-top:8px;" href="profile.php"><i class="icon-user"></i> Profile Page</a></div>'
        };
        if ( $('.action-fav').attr('disabled') ) {
            return false;
        } else {
            $('.action-fav').popover(config);
        }

});
</script>
</head>
<body onbeforeunload='javascript: if (app.savedflag != "ok") { return "Crown Color Visualizer \r\n You have not saved your work yet. \r\n You can log in or register an account to save." } ;'>
  <!-- FB script -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <!-- end of FB -->

    <div class="body-wrapper">
        <div class="white-background">
            <div class="container">
                <div class="header">
                    <div class="header-nav box_white">

                    <!--Top Menu-->
                      <div id="topmenu">
							
                         <div class="header-title-content">

                             <!--logo-->
                             <div class="logo"><a href="index.php"><img src="images/ccvv.png" alt="" /></a></div>
                             <!--/logo-->

                         </div>
                    
                       </div>


                      </div>
                       <!--/top menu-->
                  </div>
                    
                    <!--Let's Do It--><!--/let's do it-->
                  <div class="clear"></div>
                    
                   
                </div>
                 <!--header title-->
                   
                    <!--/header title-->
            </div>
                <div class="clear"></div>
        </div>
        <!--/header-->
        
<div class="black-background">
    <div class="container">
        <!--header slider-->
        <div class="header-title-content text-left" >				
		<div style="padding:5px;">
		<a href="javascript:history.back()" class="btn btn-mini">Back</a>
			<a class="btn btn-mini" href="http://www.crownit.co.ke">Home</a>
			<a class="btn btn-mini" id="gallery">Color Inspiration</a>
<?php
$page = (isset($_COOKIE['CDM'])) ? 'admin.php' : 'profile.php';
$name = (isset($_COOKIE['CDM'])) ? 'admin' : 'profile';
echo (isset($_COOKIE['CNM']) || isset($_SESSION['oau'])) ? '<a class="btn btn-mini" style="float:right; "href="/includes/scripts/logout.php">Logout</a> <a class="btn btn-mini" style="float:right; margin-right:4px;" href="' . $page . '">' . $name . '</a>' : '<a class="btn btn-mini btn-success text-extrabold" style="float:right;" id="login">JOIN OUR NETWORK</a>';
?>
        <!--/header slider-->
        </div>
		</div>
        <div class="header-title-content text-left" >               
            <div style="padding:5px;">
                <h1 style="font-size:16px; font-family: 'Open Sans' !important; color:white; padding: 5px; text-align: center !important; font-weight: bold; margin-left: 0px !important; width: 100% !important;">Choose A Color From The Palette, Then Select A Wall To Transform</h1>
            </div>
        </div>	
    </div>
</div>

<!-- middle -->
<div class="white-background">
    <div class="container mid">
    <div id="mid" class="full_width step1">
            <!-- content ccv -->
            <div class="content" role="main" style="position: relative;">

            <div class="app">
            <div class="indicator">
                <div></div>
                <span>loading</span>
            </div>
            <div id="left-pane">
                <!-- toolbar menu + colorbar -->
                <div class="toolbar-menu">
                    <div class="btn-group">
                        <a class="btn action-undo" data-target="#" title="Undo last action" disabled="disabled">
                            <img src="images/undo-icon.png" width="18">
                        </a>
                        <a class="btn action-redo" data-target="#" title="Redo last action" disabled="disabled">
                            <img src="images/redo-icon.png" width="18">
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn action-replace-tool" data-target="#" title="Replace Color">
                            <img src="images/replace.png" width="18">
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn action-polygon-tool" data-target="#" title="Draw a Section">
                            <img src="images/plot.png" width="18">
                        </a>
                        <a class="btn action-clear-polygon" data-target="#" title="Clear The Section" disabled="disabled">
                            <img src="images/clear.png" width="18">
                        </a>
                        <a class="btn action-paint-polygon" data-target="#" title="Paint The Section" disabled="disabled">
                            <img src="images/paint.png" width="18">
                        </a>
                    </div>
                    <div class="btn-group">
                        <a class="btn action boxd" style="background:red;" data-palette="0-15" title="Red Color" ></a>
                        <a class="btn action boxd" style="background:orange;" data-palette="16-35" title="Orange Color" ></a>
                        <a class="btn action boxd" style="background:yellow;" data-palette="36-70" title="Yellow Color" ></a>
                        <a class="btn action boxd" style="background:green;" data-palette="71-162" title="Green Color" ></a>
                        <a class="btn action boxd" style="background:blue;" data-palette="163-252" title="Blue Color" ></a>
                        <a class="btn action boxd" style="background:indigo;" data-palette="253-268" title="Indigo Color" ></a>
                        <a class="btn action boxd" style="background:violet;" data-palette="269-279" title="Violet Color" ></a>
                        <a class="btn action boxd" style="background:pink;" data-palette="280-318" title="Pink Color" ></a>
                        <a class="btn action boxd" style="background:rgb(252, 0, 149);" data-palette="319-360" title="Magenta Color" ></a>
                        <a class="btn action boxd" style="background:rgb(253, 245, 237);" data-palette="white" title="White Color" ></a>
                        <a class="btn action boxd" style="background:rgb(253, 244, 221);" data-palette="cream" title="Cream Color" ></a>
                        <a class="btn action boxd" style="background:gray;" data-palette="gray" title="Gray Color" ></a>
                        <a class="btn action boxd" style="background:black;" data-palette="black" title="Black Color" ></a>
                    </div>
                </div>
                <!--/ toolbar menu + colorbar -->

                <div id="app-canvas" class="step2"></div>

            </div>
            <div id="right-pane">
            	<div class="selector">
                    <div class="btn-toolbar" style="margin:4px;">
                        <div class="btn-group" style="position:relative;">
                            <a class="btn action-print" data-target="#" title="Print to PDF">
                                <i class="icon-print"></i>
                            </a>
                            <a class="btn action-fav" data-target="#" data-toogle="popover" <?php echo ($logged_in == 'true') ? 'data-user-id="' . $_COOKIE['CID'] . '" ' . 'data-username="' . $_COOKIE['CNM'] . '" ' : 'disabled="disabled"'; ?> >
                                <i class="icon-user"></i>
                            </a>
                            <a class="btn action-share" data-target="#" data-toggle="popover" title="Share via Social Media">
                                <i class="icon-share"></i>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a class="btn action-refresh" data-target="#" title="Refresh the stage">
                                <i class="icon-refresh"></i>
                            </a>
                        </div>
                        <div class="btn-group hidden">
                            <a class="btn action-adjust" data-target="#" title="Adjust the image for painting">
                                <i class="icon-adjust"></i>
                            </a>
                        </div>
                    </div>
                    <!-- a class="stage-reset step8" title="Refresh" href="javascript:;">Refresh</a -->
                    <!--span style="color: #000; padding:0px 1px; font-size: 12px;">Scroll the palette to view more colors</span-->
                    <input type="text" class="input search" placeholder="Find Colors" autofocus>
            	</div>

                <div class="color-selector-box" style="background:#f2f2f2;">
                    <p style="border-bottom:1px solid #bfc0bd; padding:3px;">Select A Shade</p>
                    <div class="clea"></div>

                </div>

            	<div class="panel step3">
            	</div>

                <div class="recent step7">
                    <p>Recent colors</p>
                    <div class="mini-box-cont">
                        <div id="box" class="mini-box-0"></div>
                        <div id="box" class="mini-box-1"></div>
                        <div id="box" class="mini-box-2"></div>
                        <div id="box" class="mini-box-3"></div>
                        <div id="box" class="mini-box-4"></div>
                    </div>
                </div>

            	<div class="app-panel">
                    <div class="sample">
                        <p class="sample-color step5">color name</p>
                        <div id="dropper-min-dark" class="box"></div>
                        <div id="dropper" class="step4"></div>
                        <div id="dropper-min-light" class="box"></div>
                        <p class="sample-code step6">color code</p>
                        <div class="clear"></div> 
                    </div>
               </div>

            </div>

            <div class="clear"></div>
            </div>
				
			<div class="ccv-disclaimer">
		        Due to individual computer monitor limitations, colors seen here may slightly vary from Crown Paints Kenya Ltd &copy; paint colors.
				To confirm your color choices, visit any of the Crown Paints stores and refer to our in-store color cards.
            </div>

            <!-- main footer -->
            <div class="our-work" style="margin-top:10px; height:auto;">
                <div class="content">
                    <div class="row" style="width:960px;">
                        <div class="col col_1_4">
                            <a href="Interiors.php">
                                <div class="inner">
                                    <h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico1.jpg" alt="" />
                                    <h2 class="text-bold text-white">Interiors</h2>
                                    <p>Transform your Interior</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col_1_4">
                            <a href="Exteriors.php">
                                <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico2.jpg" alt="" />
                                    <h2 class="text-bold text-white">Exteriors</h2>
                                    <p>Transform your Exterior</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col_1_4">
                            <a href="textures.page.php">
                                <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico3.jpg" alt="" />
                                    <h2 class="text-bold text-white">Textures</h2>
                                    <p>Transform with Textures</p>
                                </div>
                            </a>
                        </div>
                        <div class="col col_1_4">
                            <a href="offices.page.php">
                                <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:15px;"> click to view</h1>
                                    <img src="images/tuts/ico4.jpg" alt="" />
                                    <h2 class="text-bold text-white">Offices</h2>
                                    <p>Transform your Office</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <!--/ main footer -->
            
            </div>
            </div>
            <!--/ content ccv -->
           
            <div class="clear"></div>
        </div>
</div>


                <?php
                    require 'includes/footer.php';
                ?>