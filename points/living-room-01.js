{
        "one": {
            "points": [
                [180, 336,616, 335,615, 52,180, 49,180,336]
            ],
            "stroke": "black",
            "strokeWidth": 2,
            "opacity": 0.5,
            "name":"point-01"
        },
        "two": {
            "points": [
                [0,418,33,402,33,62,37,61,137,118,137,354,179,335,179,51,181,48,137,0,0,0,0,418]
            ],
            "stroke": "black",
            "strokeWidth": 2,
            "opacity": 0.5,
            "name":"point-02"
        },
        "three": {
            "points": [
                [628,344,616,339,616,335,615,334,615,52,614,50,676,0,711,0,628,60,628,344]
            ],
            "stroke": "black",
            "strokeWidth": 2,
            "opacity": 0.5,
            "name":"point-03"
        },
       "four": {
            "points": [
                [739,384,726,379,726,0,739,0,739,384]
            ],
            "stroke": "black",
            "strokeWidth": 2,
            "opacity": 0.5,
            "name":"point-03"
       },
      "five": {
            "points": [
                [137,0,181,49,614,51,676,0,137,0]
            ],
            "stroke": "black",
            "strokeWidth": 2,
            "opacity": 0.5,
            "name":"point-03"
       }
}