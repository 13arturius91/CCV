{
    "one": {
        "points": [
            [-1,-1,74,0,183,71,189,71,190,342,0,467,-1,-1 ]
         ],
         "stroke": "black",
         "strokeWidth": 2,
         "opacity": 0.5
     },
    "two": {
        "points": [
            [337,70,621,69,621,292,337,290,337,70]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "three": {
        "points": [
            [621,68,739,69,739,337,621,291,621,68]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "four": {
        "points": [
            [189,101,189,283,264,255,266,255,266,138,257,135,221,116,196,104,189,101]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "five": {
        "points": [
            [266,138,266,255,337,255,337,138,266,138]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    }
}