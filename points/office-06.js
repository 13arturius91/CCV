{
    "one": {
        "points": [
            [-1, 7, 342, 46, 343, 170, 335, 169, 329, 185, 325, 185, 322, 185, 311, 185, 309, 185, 305, 185, 300, 191, 297, 197, 297, 201, 297, 206, 297, 208, 297, 210, 287, 211, 286, 215, 279, 213, 277, 199, 247, 202, 246, 199, 244, 197, 276, 195, 276, 192, 277, 190, 274, 108, 239, 107, 240, 130, 233, 134, 228, 127, 220, 126, 214, 126, 214, 129, 214, 133, 210, 136, 208, 135, 203, 136, 201, 135, 198, 137, 201, 141, 195, 142, 191, 71, -1, 55, -1, 7],
            [77,22,79,22,81,22,83,23,85,23,88,23,91,24,92,24,95,24,97,24,100,25,103,25,106,24,108,25,109,27,110,30,109,32,109,35,110,37,110,39,110,40,110,42,110,44,111,47,110,49,110,53,112,55,112,59,109,61,106,60,104,59,100,60,98,59,96,59,93,59,91,59,88,58,86,58,83,59,80,58,78,55,78,53,79,51,79,48,78,46,78,45,78,43,78,42,77,40,77,39,77,36,77,34,77,32,77,29,76,26,76,24,76,23,77,22],
            [0,0]
         ],
         "stroke": "black",
         "strokeWidth": 2,
         "opacity": 0.5
     },
    "two": {
        "points": [
            [341,47,360,40,398,45,398,150,380,152,380,152,380,231,345,226,343,211,347,210,346,198,348,188,345,186,348,178,350,173,350,170,343,170,341,47],
            [0,0]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "three": {
        "points": [
            [455,3,448,245,620,282,641,0,593,0,591,19,590,21,489,46,481,44,477,42,471,35,470,26,471,13,473,4,475,-1,460,-1,455,3],
            [476,63,504,57,501,112,475,114,476,63],
            [512,55,544,47,541,107,510,111,512,55 ],
            [553,45,592,37,587,102,551,106,553,45],
            [489,119,519,116,518,173,489,172,489,119],
            [528,114,563,113,559,174,525,171,528,114],
            [438,265,446,267,446,274,615,321,616,312,738,344,738,498,719,499,488,385,490,331,496,326,497,325,498,314,495,310,488,305,480,302,471,300,461,298,450,297,442,296,437,295,437,280,438,265]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "four": {
        "points": [
            [-1,61,186,76,193,233,178,227,171,92,116,96,124,217,19,230,4,71,-1,68,-1,61],
            [63,247,125,236,129,297,70,316,63,247]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "five": {
        "points": [
            [180,249,195,259,194,262,192,261,183,267,195,277,198,332,184,317,180,249]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "six": {
        "points": [
            [131,108,171,106,176,211,137,210,131,108 ]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    },
    "seven": {
        "points": [
            [139,234,177,227,179,281,151,290,142,288,139,234 ]
        ],
        "stroke": "black",
        "strokeWidth": 2,
        "opacity": 0.5
    }
}