<?php
	
	include 'includes/dao/config.php';		
	
	include 'google.php';
		
	if(array_key_exists("login",$_GET)){
	
		$fboauth = $_GET['op'];
		
			if($fboauth == 'facebook'){
				
				echo ("<script language='JavaScript'>
				window.location.href='facebook/example/login-facebook.php';
				</script>");
			}
		}	
				
    require 'includes/header.php';
	
?>
        
        <div class="black-background">
            <div class="container">
			<div class="header-title-content text-left box_bleck" >				
							<div style="padding:5px;">	
							
							<a class="btn btn-mini" href="http://www.crownit.co.ke">Home</a>
							  
							<a class="btn btn-mini" id="gallery">Color Inspiration</a>
							 
							<a class="btn btn-mini" id="link">Video Tutorial</a>
							 
							<a href="http://crownit.co.ke/wiki/index.php?title=Main_Page" target="_blank" class="btn btn-mini">Text Tutorial</a>
							 
							<?php

							  $page = (isset($_COOKIE['CDM'])) ? 'admin.php' : 'profile.php';
							  
							  $name = (isset($_COOKIE['CDM'])) ? 'admin' : 'profile';
							  
							  echo (isset($_COOKIE['CNM']) || isset($_SESSION['oau'])) ? '<a class="btn btn-mini" style="float:right;" href="' . $page . '">' . $name . '</a>' : '<a class="btn btn-mini btn-success text-extrabold" style="float:right;" id="login">JOIN OUR NETWORK</a>';

                            ?>
							 
							</div>		
              </div>
            </div>
        </div>
        
<!-- middle -->
        <div class="white-background">
            <div class="container ">
			<div style="padding-top:20px;">
            <h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">1. Introduction</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">The Terms of Use of the Crown Color Visualiser website include these Terms and Conditions, the Privacy Notice and any other terms and conditions that appear in or are linked to the Crown Color Visaliser website (Additional Terms and Conditions).</span></span></p><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">The Additional Terms and Conditions that appear on the Crown Color Visualiser website will govern your use of, and access to, certain sections of the Web Site where they appear. Since these Additional Terms and Conditions form part of the Terms of Use you are bound by them and should review them wherever they are relevant to you when using the Crown Color Visualiser website.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">2. Website content and access</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">While Crown Color Visualiser endeavours to take reasonable care in preparing and maintaining the information on this website we do not warrant the accuracy, reliability, adequacy or completeness of any of the website content. You acknowledge and accept that the website content may include technical inaccuracies and typographical errors. The website content is subject to change at any time without notice and may not necessarily be up to date or accurate at the time you view it.</span></span></p><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">It is your responsibility to enquire with us directly to ensure the accuracy and currency of the material or information you seek to rely upon. To the extent permitted by Law Crown Color Visualiser disclaims all liability for loss directly or indirectly arising from your use of or reliance on the website and the website content.</span></span></p><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">Crown Color Visualiser does not guarantee that access to the website will be uninterrupted or that the website is free from viruses or anything else which may damage any computer which accesses the website or any data on such a computer.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">3. Limitation of liability</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">To the maximum extent permitted by Law Crown Color Visualiser excludes completely all liability whatsoever for any loss or damage of any kind (including special, indirect or consequential loss and including loss of business profits) however caused (including negligence) arising out of or in connection with the website content and the use or performance of the Crown Color Visualiser website.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">4. Intellectual property</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">The materials displayed on this website, including without limitation all editorial materials, information, photographs, illustrations and other graphic materials, and names, logos and trade marks, are the property of Crown Color Visualiser and Crown Paints Kenya and are protected by copyright, trade mark and other intellectual property laws. Any such content may be displayed and printed solely for your personal, non-commercial use within your organisation only provided that any copyright notice on such a display or page is not removed. You agree not to reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate any such material to any third party without the express prior written consent of Crown Color Visualiser.</span></span></p><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">Save for the above, and unless expressely granted, Crown Color Visualiser does not grant any licence or right in, or assign all or part of, its intellectual property rights in the content or applications incorporated into the Crown Color Visualiser website or in the user interface of the Crown Color Visualiser website.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">5. Submissions</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">Any material you send to us on or via the Crown Color Visualiser website will be deemed to be non-confidential and non-proprietary. This includes any data, questions, comments, suggestions, ideas or other information. We will be entitled to use such material for any purpose without compensation to you.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">6. Third party websites</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">The Crown Color Visualiser Website may contain links to other websites operated, controlled or produced by third parties. Unless otherwise indicated, Crown Color Visualiser does not control, endorse, sponsor or approve any such third party websites or their content nor does Crown Color Visualiser provide any warranty or take any responsibility whatsoever for any aspect of those websites or their content.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">7. Links to Crown Color Visualiser website</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">If you wish to establish a&nbsp;link to this website, you must first seek approval from Crown Paints. To seek approval, please contact your account manager. If you don&#39;t have one or are unsure, please email by clicking -> <a href="http://www.my-it-provider.com" target="_blank">My-IT-Provider</a>.</span></span></p><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">The following information will be required to assess your request:</span></span></p><br><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">(1)the URL of the website that you seek to establish a link from</span></span></br><br><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">(2)a brief description of your website</span></span></br><br><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">(3)the reason that you wish to establish a link.</span></span></br></br><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">If Crown Paints agrees to your proposed link, you must comply with any terms and conditions imposed by Crown Color Visualiser as a condition of such agreement. If the nature and/or content of your website changes in any significant way, you must contact Crown Paints and provide a new description of your website.</span></span></p><h3><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">8. Interference with website</span></span></span></h3><p><span style="font-family:comic sans ms,cursive"><span style="font-size:16px">You must not attempt to change, add to, remove, deface, hack or otherwise interfere with this website or any material or content displayed on the Crown Color Visualiser website.</span></span></p><h3><span style="font-size:16px"><span style="color:#0000FF"><span style="font-family:comic sans ms,cursive">9. Jurisdiction</span></span></span></h3><p><span style="font-size:16px"><span style="font-family:comic sans ms,cursive"><span style="background-color:rgb(255, 255, 255)">These Terms and Conditions shall be interpreted and governed by the laws in force in Kenya. Each party hereby agrees to submit to the jurisdiction of the Kenyan courts and to waive any objections based upon venue.</span></span></span></p>
			</div>

        <?php
            require 'includes/footer.php';
        ?>