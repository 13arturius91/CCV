{
    "one": {
        "outer": [{ "x":2, "y":0},{ "x":266, "y":101},{ "x":490, "y":0},{ "x":2, "y":0}],
        "poly" : [
            [0,0]
        ],
        "inner": [
            [{ "x":253, "y":68},{ "x":247, "y":68},{ "x":235, "y":64},{ "x":232, "y":61},{ "x":231, "y":58},{ "x":233, "y":55},{ "x":237, "y":53},{ "x":241, "y":52},{ "x":247, "y":52},{ "x":251, "y":52},{ "x":254, "y":54},{ "x":258, "y":56},{ "x":261, "y":60},{ "x":261, "y":64},{ "x":254, "y":67},{ "x":251, "y":67},{ "x":253, "y":68}]
        ]
    },
    "two": {
        "outer": [{ "x":0, "y":1},{ "x":4, "y":1},{ "x":266, "y":101},{ "x":271, "y":293},{ "x":267, "y":294},{ "x":264, "y":297},{ "x":262, "y":299},{ "x":262, "y":293},{ "x":261, "y":291},{ "x":257, "y":289},{ "x":257, "y":289},{ "x":256, "y":285},{ "x":259, "y":283},{ "x":259, "y":254},{ "x":249, "y":254},{ "x":242, "y":254},{ "x":235, "y":254},{ "x":235, "y":269},{ "x":231, "y":263},{ "x":229, "y":259},{ "x":227, "y":258},{ "x":227, "y":257},{ "x":227, "y":255},{ "x":227, "y":254},{ "x":225, "y":252},{ "x":222, "y":252},{ "x":223, "y":263},{ "x":225, "y":263},{ "x":226, "y":262},{ "x":226, "y":260},{ "x":227, "y":259},{ "x":229, "y":262},{ "x":232, "y":267},{ "x":235, "y":273},{ "x":235, "y":285},{ "x":238, "y":285},{ "x":239, "y":289},{ "x":241, "y":293},{ "x":242, "y":295},{ "x":245, "y":302},{ "x":246, "y":307},{ "x":247, "y":308},{ "x":243, "y":309},{ "x":243, "y":312},{ "x":238, "y":314},{ "x":235, "y":315},{ "x":232, "y":319},{ "x":230, "y":322},{ "x":231, "y":325},{ "x":239, "y":318},{ "x":238, "y":322},{ "x":238, "y":329},{ "x":240, "y":329},{ "x":243, "y":322},{ "x":244, "y":323},{ "x":246, "y":323},{ "x":246, "y":329},{ "x":225, "y":333},{ "x":225, "y":329},{ "x":221, "y":331},{ "x":221, "y":335},{ "x":164, "y":344},{ "x":58, "y":360},{ "x":15, "y":365},{ "x":11, "y":367},{ "x":0, "y":369},{ "x":0, "y":1}],
        "poly" : [
            [{ "x":345, "y":285},{ "x":387, "y":283},{ "x":385, "y":173},{ "x":354, "y":163},{ "x":366, "y":160},{ "x":366, "y":162},{ "x":369, "y":163},{ "x":375, "y":164},{ "x":382, "y":162},{ "x":382, "y":160},{ "x":381, "y":156},{ "x":389, "y":154},{ "x":389, "y":156},{ "x":392, "y":158},{ "x":399, "y":159},{ "x":403, "y":158},{ "x":405, "y":299},{ "x":401, "y":299},{ "x":398, "y":300},{ "x":395, "y":303},{ "x":382, "y":303},{ "x":379, "y":302},{ "x":374, "y":302},{ "x":345, "y":302},{ "x":345, "y":285}]
        ],
        "inner": [
            [{ "x":5, "y":56},{ "x":220, "y":121},{ "x":222, "y":251},{ "x":223, "y":263},{ "x":223, "y":290},{ "x":4, "y":301},{ "x":5, "y":56} ],
            [{ "x":259, "y":220},{ "x":259, "y":250},{ "x":235, "y":249},{ "x":235, "y":219},{ "x":259, "y":220}]
        ]
    },
    "three": {
        "outer": [{ "x":265, "y":101},{ "x":490, "y":0},{ "x":738, "y":0},{ "x":738, "y":499},{ "x":678, "y":499},{ "x":676, "y":369},{ "x":680, "y":368},{ "x":680, "y":367},{ "x":680, "y":364},{ "x":681, "y":359},{ "x":682, "y":352},{ "x":682, "y":343},{ "x":683, "y":339},{ "x":682, "y":316},{ "x":681, "y":314},{ "x":681, "y":291},{ "x":678, "y":220},{ "x":676, "y":172},{ "x":674, "y":101},{ "x":674, "y":82},{ "x":340, "y":164},{ "x":343, "y":300},{ "x":341, "y":301},{ "x":341, "y":315},{ "x":330, "y":316},{ "x":327, "y":316},{ "x":320, "y":315},{ "x":313, "y":304},{ "x":310, "y":300},{ "x":308, "y":298},{ "x":306, "y":297},{ "x":306, "y":296},{ "x":307, "y":295},{ "x":307, "y":293},{ "x":306, "y":292},{ "x":305, "y":291},{ "x":303, "y":293},{ "x":303, "y":294},{ "x":303, "y":296},{ "x":303, "y":297},{ "x":301, "y":299},{ "x":298, "y":301},{ "x":298, "y":304},{ "x":298, "y":305},{ "x":296, "y":307},{ "x":295, "y":308},{ "x":294, "y":306},{ "x":291, "y":304},{ "x":290, "y":303},{ "x":284, "y":306},{ "x":280, "y":305},{ "x":276, "y":304},{ "x":274, "y":303},{ "x":271, "y":303},{ "x":270, "y":302},{ "x":274, "y":297},{ "x":273, "y":295},{ "x":272, "y":293},{ "x":270, "y":293},{ "x":270, "y":293},{ "x":265, "y":101}],
        "poly" : [
            [{ "x":302, "y":316},{ "x":300, "y":314},{ "x":299, "y":310},{ "x":299, "y":307},{ "x":299, "y":305},{ "x":300, "y":303},{ "x":300, "y":301},{ "x":302, "y":300},{ "x":304, "y":299},{ "x":305, "y":298},{ "x":307, "y":299},{ "x":309, "y":301},{ "x":311, "y":302},{ "x":312, "y":306},{ "x":313, "y":308},{ "x":313, "y":312},{ "x":313, "y":314},{ "x":311, "y":314},{ "x":310, "y":316},{ "x":306, "y":316},{ "x":305, "y":314},{ "x":302, "y":316},{ "x":302, "y":316}],
            [{ "x":359, "y":137},{ "x":358, "y":137},{ "x":354, "y":134},{ "x":354, "y":130},{ "x":356, "y":129},{ "x":358, "y":133},{ "x":359, "y":137}],
            [{ "x":382, "y":130},{ "x":379, "y":127},{ "x":379, "y":126},{ "x":378, "y":126},{ "x":377, "y":122},{ "x":378, "y":120},{ "x":380, "y":123},{ "x":381, "y":125},{ "x":382, "y":126},{ "x":382, "y":130}],
            [{ "x":407, "y":120},{ "x":403, "y":118},{ "x":404, "y":114},{ "x":406, "y":116},{ "x":407, "y":120}]
        ],
        "inner": [
            [{ "x":346, "y":156},{ "x":345, "y":153},{ "x":348, "y":138},{ "x":348, "y":137},{ "x":352, "y":134},{ "x":352, "y":131},{ "x":352, "y":129},{ "x":354, "y":127},{ "x":357, "y":127},{ "x":360, "y":130},{ "x":360, "y":131},{ "x":362, "y":137},{ "x":362, "y":139},{ "x":363, "y":141},{ "x":365, "y":143},{ "x":368, "y":143},{ "x":370, "y":142},{ "x":370, "y":139},{ "x":370, "y":137},{ "x":370, "y":135},{ "x":370, "y":130},{ "x":372, "y":128},{ "x":374, "y":126},{ "x":375, "y":127},{ "x":375, "y":121},{ "x":378, "y":118},{ "x":379, "y":118},{ "x":383, "y":122},{ "x":384, "y":124},{ "x":384, "y":132},{ "x":386, "y":131},{ "x":387, "y":131},{ "x":389, "y":131},{ "x":394, "y":133},{ "x":395, "y":130},{ "x":396, "y":127},{ "x":396, "y":123},{ "x":396, "y":121},{ "x":400, "y":118},{ "x":400, "y":113},{ "x":402, "y":111},{ "x":404, "y":111},{ "x":408, "y":113},{ "x":410, "y":117},{ "x":408, "y":127},{ "x":410, "y":133},{ "x":410, "y":138},{ "x":412, "y":140},{ "x":408, "y":142},{ "x":402, "y":143},{ "x":397, "y":143},{ "x":394, "y":143},{ "x":393, "y":142},{ "x":394, "y":140},{ "x":394, "y":138},{ "x":393, "y":139},{ "x":391, "y":141},{ "x":388, "y":142},{ "x":386, "y":141},{ "x":385, "y":141},{ "x":384, "y":141},{ "x":385, "y":146},{ "x":383, "y":149},{ "x":378, "y":150},{ "x":373, "y":151},{ "x":371, "y":150},{ "x":369, "y":149},{ "x":369, "y":146},{ "x":366, "y":145},{ "x":363, "y":144},{ "x":361, "y":142},{ "x":359, "y":140},{ "x":358, "y":140},{ "x":360, "y":147},{ "x":361, "y":153},{ "x":360, "y":154},{ "x":356, "y":156},{ "x":352, "y":157},{ "x":348, "y":156},{ "x":346, "y":156},{ "x":346, "y":156}],
            [{ "x":514, "y":109},{ "x":514, "y":102},{ "x":515, "y":98},{ "x":516, "y":93},{ "x":516, "y":89},{ "x":517, "y":87},{ "x":518, "y":87},{ "x":518, "y":85},{ "x":522, "y":81},{ "x":522, "y":75},{ "x":524, "y":73},{ "x":525, "y":71},{ "x":528, "y":71},{ "x":529, "y":74},{ "x":530, "y":76},{ "x":531, "y":79},{ "x":531, "y":84},{ "x":532, "y":85},{ "x":532, "y":86},{ "x":533, "y":89},{ "x":534, "y":93},{ "x":538, "y":94},{ "x":542, "y":94},{ "x":546, "y":93},{ "x":550, "y":92},{ "x":553, "y":90},{ "x":555, "y":90},{ "x":556, "y":89},{ "x":557, "y":85},{ "x":559, "y":81},{ "x":559, "y":77},{ "x":560, "y":75},{ "x":560, "y":73},{ "x":565, "y":69},{ "x":565, "y":61},{ "x":567, "y":58},{ "x":570, "y":58},{ "x":572, "y":61},{ "x":572, "y":64},{ "x":572, "y":67},{ "x":574, "y":70},{ "x":575, "y":72},{ "x":576, "y":73},{ "x":576, "y":75},{ "x":577, "y":77},{ "x":587, "y":80},{ "x":587, "y":81},{ "x":604, "y":76},{ "x":606, "y":69},{ "x":607, "y":65},{ "x":607, "y":64},{ "x":609, "y":61},{ "x":608, "y":59},{ "x":610, "y":57},{ "x":612, "y":56},{ "x":613, "y":55},{ "x":614, "y":50},{ "x":613, "y":46},{ "x":616, "y":44},{ "x":619, "y":44},{ "x":620, "y":45},{ "x":621, "y":51},{ "x":621, "y":53},{ "x":623, "y":55},{ "x":624, "y":56},{ "x":625, "y":57},{ "x":626, "y":58},{ "x":626, "y":60},{ "x":629, "y":64},{ "x":630, "y":68},{ "x":631, "y":72},{ "x":632, "y":80},{ "x":632, "y":83},{ "x":630, "y":84},{ "x":626, "y":87},{ "x":622, "y":89},{ "x":618, "y":91},{ "x":612, "y":91},{ "x":606, "y":91},{ "x":603, "y":89},{ "x":602, "y":87},{ "x":603, "y":85},{ "x":604, "y":81},{ "x":604, "y":80},{ "x":587, "y":86},{ "x":588, "y":93},{ "x":574, "y":104},{ "x":565, "y":101},{ "x":561, "y":102},{ "x":557, "y":101},{ "x":555, "y":100},{ "x":556, "y":98},{ "x":556, "y":93},{ "x":546, "y":96},{ "x":544, "y":97},{ "x":541, "y":97},{ "x":535, "y":96},{ "x":538, "y":107},{ "x":536, "y":108},{ "x":532, "y":111},{ "x":528, "y":111},{ "x":524, "y":112},{ "x":521, "y":113},{ "x":516, "y":113},{ "x":514, "y":112},{ "x":514, "y":109}]
        ]
    },
    "four": {
        "outer": [{ "x":294, "y":328},{ "x":295, "y":352},{ "x":294, "y":356},{ "x":294, "y":447},{ "x":302, "y":452},{ "x":302, "y":458},{ "x":304, "y":458},{ "x":306, "y":458},{ "x":410, "y":499},{ "x":679, "y":499},{ "x":677, "y":368},{ "x":294, "y":328}],
        "poly" : [
            [0,0]
        ],
        "inner": [
            [0,0]
        ]
    },
    "five": {
        "outer": [{ "x":0, "y":368},{ "x":11, "y":365},{ "x":13, "y":364},{ "x":219, "y":335},{ "x":221, "y":336},{ "x":221, "y":340},{ "x":219, "y":344},{ "x":220, "y":348},{ "x":220, "y":353},{ "x":217, "y":352},{ "x":213, "y":352},{ "x":211, "y":352},{ "x":201, "y":355},{ "x":201, "y":356},{ "x":202, "y":358},{ "x":201, "y":359},{ "x":152, "y":368},{ "x":90, "y":380},{ "x":24, "y":392},{ "x":1, "y":397},{ "x":0, "y":398},{ "x":0, "y":368}],
        "poly" : [
        [{ "x":246, "y":330},{ "x":226, "y":333},{ "x":226, "y":334},{ "x":228, "y":336},{ "x":227, "y":338},{ "x":229, "y":340},{ "x":229, "y":346},{ "x":229, "y":353},{ "x":229, "y":356},{ "x":258, "y":363},{ "x":255, "y":359},{ "x":251, "y":354},{ "x":247, "y":352},{ "x":243, "y":351},{ "x":241, "y":352},{ "x":236, "y":354},{ "x":235, "y":356},{ "x":235, "y":356},{ "x":234, "y":356},{ "x":231, "y":352},{ "x":233, "y":350},{ "x":237, "y":348},{ "x":243, "y":346},{ "x":248, "y":348},{ "x":253, "y":349},{ "x":257, "y":352},{ "x":261, "y":355},{ "x":263, "y":358},{ "x":263, "y":356},{ "x":265, "y":355},{ "x":266, "y":355},{ "x":266, "y":355},{ "x":266, "y":359},{ "x":266, "y":364},{ "x":266, "y":364},{ "x":267, "y":364},{ "x":269, "y":360},{ "x":267, "y":357},{ "x":268, "y":355},{ "x":269, "y":355},{ "x":271, "y":357},{ "x":271, "y":359},{ "x":272, "y":361},{ "x":272, "y":363},{ "x":272, "y":363},{ "x":274, "y":365},{ "x":295, "y":371},{ "x":295, "y":334},{ "x":293, "y":334},{ "x":291, "y":333},{ "x":291, "y":332},{ "x":288, "y":332},{ "x":286, "y":334},{ "x":284, "y":336},{ "x":284, "y":334},{ "x":285, "y":334},{ "x":286, "y":332},{ "x":278, "y":332},{ "x":277, "y":333},{ "x":273, "y":333},{ "x":270, "y":333},{ "x":267, "y":336},{ "x":259, "y":337},{ "x":255, "y":338},{ "x":251, "y":339},{ "x":249, "y":337},{ "x":247, "y":334},{ "x":247, "y":332},{ "x":247, "y":331},{ "x":246, "y":330}]
        ],
        "inner": [
            [0,0]
        ]
    },
    "six": {
        "outer": [{ "x":1, "y":457},{ "x":294, "y":377},{ "x":293, "y":447},{ "x":303, "y":451},{ "x":303, "y":457},{ "x":244, "y":485},{ "x":241, "y":484},{ "x":239, "y":483},{ "x":234, "y":483},{ "x":229, "y":485},{ "x":222, "y":488},{ "x":215, "y":491},{ "x":208, "y":493},{ "x":203, "y":496},{ "x":200, "y":497},{ "x":196, "y":498},{ "x":0, "y":498},{ "x":1, "y":457}],
        "poly" : [
            [0,0]
        ],
        "inner": [
            [0,0]
        ]
    },
    "seven": {
        "outer": [],
        "poly" : [
            [0,0]
        ],
        "inner": [
            [0,0]
        ]
    }
}