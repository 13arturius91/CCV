$(function() {

    /**
     * like a project
     */
    $(document).delegate('a#like-project', 'click', function(e) {

        e.preventDefault();

        /* param */
        var project_id = $(this).attr('data-item-id');
        var elem = $(this);
        var like_count = $(this).parent().siblings('div.stream-item-details').children('span.stream-item-likes-count').html()[0];
        var like_count_box = $(this).parent().siblings('div.stream-item-details').children('span.stream-item-likes-count');

        /* send data */
        $.ajax({
            url: 'ws/api.php',
            type: "POST",
            data:{
                do: 'like-project',
                project_id: project_id
            } ,
            success: function(data) {

                var status = JSON.parse(data);

                if (status.status == 1) {

                    /* increment like count */
                    like_count = +like_count + 1;

                    /* update like counter */
                    like_count_box.html(like_count + ' likes');

                    /* update like button */
                    elem.attr('id', 'unlike-project').html('unlike');

                } else {

                    alert(status.message)

                }

            },
            fail: function(data) {

                alert(data);

            }

        });

    });

    /**
     * unlike a project
     */
    $(document).delegate('a#unlike-project', 'click', function(e) {

        e.preventDefault();

        /* param */
        var project_id = $(this).attr('data-item-id');
        var elem = $(this);
        var like_count = $(this).parent().siblings('div.stream-item-details').children('span.stream-item-likes-count').html()[0];
        var like_count_box = $(this).parent().siblings('div.stream-item-details').children('span.stream-item-likes-count');

        /* send data */
        $.ajax({
            url: 'ws/api.php',
            type: "POST",
            data:{
                do: 'unlike-project',
                project_id: project_id
            } ,
            success: function(data) {

                var status = JSON.parse(data);

                if (status.status == 1) {

                    /* increment like count */
                    like_count = +like_count - 1;

                    /* update like counter */
                    like_count_box.html(like_count + ' likes');

                    /* update like button */
                    elem.attr('id', 'like-project').html('like');

                } else {

                    alert(status.message)

                }

            },
            fail: function(data) {

                alert(data);

            }

        });

    });
    /**
     * search for a user
     */
    $(document).delegate('#search-box', 'keyup', function(e) {

        var kw = $('#search-box').val();

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* search url */
        var search_url = api_url + '?do=find&kw=' + kw;

        if (kw != '') {

            $('.search-box-results').empty().html('<p style="text-align:center; margin-top: 50px; font-size: 115%; font-weight: bold;"> <em>searching...</em> </p>');

            $(document).load(search_url, function(data) {

                var status = JSON.parse(data);
                var data = status.payload;

                if (data != '') {

                    var string = '';

                    for (var i = 0; i < data.length; i++) {

                        var follow_string = '';

                        if (data[i].id == data[i].uid) {

                            follow_string = 'you';

                        } else {
                            /* 0 - follow request not confirmed, 1 - following, 2 - not following */
                            follow_string = (data[i].following == 0) ? 'cancel-request' : (data[i].following == 1) ? 'unfollow' : (data[i].following == 2) ? 'follow' : '';
                            console.log(data[i].following);
                        }

                        string += '<div style="background: #b7cde4; margin:5px 5px 5px 0; padding: 5px; ">' +
                            '<img width="32" height="32" style="margin-right: 5px;" src="pro_pics/' + data[i].image + '" >' +
                            '<span>' +
                            '<a href="#" style="text-decoration: none;" data-uid="' + data[i].id + '" class="stream-item-user-fullname" >' + data[i].fname + ' ' + data[i].lname + '(' + data[i].username + ') </a>' +
                            '<a href="javascript:;" data-uid="' + data[i].id + '" class="user-' + follow_string + '" >' + follow_string + '</a>' +
                            '</span>' +
                            '</div>';

                    }

                    $('.search-box-results').empty().append(string);

                } else {

                    $('.search-box-results').empty().html('<p style="text-align:center; margin-top: 50px; font-size: 115%; font-weight: bold;"> <em>No user has been found</em> </p>');

                }

            });

        } else {

            $('.search-box-results').empty().html('<p style="text-align:center; margin-top: 50px; font-size: 115%; font-weight: bold;"> <em>Start typing in the textbox to search</em> </p>');

        }

    });

    /* follow user */
    $(document).delegate('.user-follow', 'click', function(e) {

        /* don't navigate */
        e.preventDefault();

        /* params */
        var following_id = $(this).attr('data-uid');
        var elem = $(this);

        /* loading indicator */
        elem.html('<em>loading...</em>');

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* following url */
        var follow_url = api_url + '?do=follow-user&following_id=' + following_id;

        $(document).load(follow_url, function(data) {

            var status = JSON.parse(data);

            if (status.status == 1) {

                elem.html('request sent');
                elem.removeClass('user-follow');
                elem.addClass('cancel-request');

            }

        });

    });

    /* unfollow user */
    $(document).delegate('.user-unfollow', 'click', function(e) {

        /* don't navigate */
        e.preventDefault();

        /* params */
        var following_id = $(this).attr('data-uid');
        var elem = $(this);

        /* loading indicator */
        elem.html('<em>loading...</em>');

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* following url */
        var unfollow_url = api_url + '?do=unfollow-user&following_id=' + following_id;

        $(document).load(unfollow_url, function(data) {

            var status = JSON.parse(data);

            if (status.status == 1) {

                elem.html('follow');
                elem.removeClass('user-unfollow');
                elem.addClass('user-follow');

            }

        });

    });

    /* cancel follow request */
    $(document).delegate('.cancel-request', 'click', function(e) {

        console.log('cancel request');

        /* don't navigate */
        e.preventDefault();

        /* params */
        var following_id = $(this).attr('data-uid');
        var elem = $(this);

        /* loading indicator */
        elem.html('<em>loading...</em>');

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* following url */
        var follow_url = api_url + '?do=follow-user&following_id=' + following_id;

        $(document).load(follow_url, function(data) {

            var status = JSON.parse(data);

            if (status.status == 1) {

                elem.html('request sent');
                elem.removeClass('user-follow');
                elem.addClass('cancel-request');

            }

        });

    });

    /* confirm follow request */
    $(document).delegate('.confirm-request', 'click', function(e) {

        console.log('confirm request');

        /* don't navigate */
        e.preventDefault();

        /* params */
        var following_id = $(this).attr('data-uid');
        var elem = $(this);

        /* loading indicator */
        elem.html('<em>loading...</em>');

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* following url */
        var follow_url = api_url + '?do=confirm-request&following_id=' + following_id;

        $(document).load(follow_url, function(data) {

            var status = JSON.parse(data);

            if (status.status == 1) {

                elem.html('unfollow');
                elem.removeClass('cancel-request');
                elem.addClass('user-unfollow');

            }

        });

    });

    /* post a comment */
    var $form = $('#comment-post-form');

    $(document).delegate('.stream-comment-box-textarea', 'keydown', function(e) {

        if (e.keyCode == 13) {

            /* params */
            var project_id = $(this).attr('data-project-id');
            var comment_text = $(this).val();

            var elem = $(this);
            var stream_item_comments = $(this).parent().parent().parent();

            /* disable text area */
            elem.attr('disabled', 'disabled');

            /* send data */
            $.ajax({
                url: 'ws/api.php',
                type: "POST",
                data: {
                    do: 'add-comment',
                    project_id: project_id,
                    comment_text: comment_text
                },
                success: function(data) {

                    var status = JSON.parse(data);

                    if (status.status == 1) {

                        /* increment comment count */
                        //alert(status);

                        /* add comment to comment list */
                        loadComments(project_id);

                    } else {

                        alert(status.message)

                    }

                },
                fail: function(data) {

                    alert(data);

                }

            });

            return false;

        }

    });

    /* tabs */
    $(document).delegate("ul.profile-social-stream-navbar-ul > li a", 'click', function(e) {
        /* don't navigate */
        e.preventDefault();

        /* deactivate all other tabs */
        $("ul.profile-social-stream-navbar-ul > li").each(function() {

            $(this).removeClass('active');

        });

        /* activate selected tab */
        $(this).parent('li').addClass('active');

        /* get index */
        var index = $(this).attr('href').substr(1);

        /* hide previous tab */
        $('div.profile-social-stream-content > div').each(function() {

            $(this).hide();

        });

        /* load tab content */
        if (index == 'connections') {

            loadConnections();

        } else if (index == 'likes') {

            var userid = $(this).attr('data-uid');
            var last_id = '';
            var limit = 10;

            loadLikes(userid, last_id, limit)

        }

        /* show selected content tab */
        $('div.profile-social-stream-content > div#' + index).show();

    });

    /* load comments */
    $(document).delegate("a#comment-project", 'click', function(e) {
        /* don't navigate */
        e.preventDefault();

        /* get project id */
        var project_id = $(this).attr('data-item-id');

        /* load comments */
        loadComments(project_id)

    });

    /* create tab */
    $(document).delegate(".stream-item-user-fullname", 'click', function(e) {
        /* don't navigate */
        e.preventDefault();

        /* get name */
        var name = $(this).html();
        var uid = $(this).attr('data-uid');

        createNewTab('pole-custom', name, uid);

    });

    /* tab helper */
    function createNewTab(id, name, item_id) {

        /* delete old tab */
        $("ul.profile-social-stream-navbar-ul > li.custom-tab").each(function() {

            $(this).remove();

        });

        /* delete old tab content */
        $('div.profile-social-stream-content > div.custom-tab').each(function() {

            $(this).remove();

        });

        /* create new tab */
        $("ul.profile-social-stream-navbar-ul").append("<li class='custom-tab'><a href='#" + id + "'>" + name + "</a></li>");

        /* create new tab content pane */
        $('div.profile-social-stream-content').append("<div class='custom-tab loading' id='" + id + "'></div>");

        /* load tab data */
        $.ajax({
            url: 'ws/api.php',
            type: "POST",
            data:{
                do: 'get-user-projects',
                userid: item_id
            },
            success: function(data) {

                /* remove loading */
                $("div#" + id).removeClass('loading');

                /* load content */
                $("div#" + id).append(data);

                /* show tab */
                setTimeout(function() {

                    $('a[href=#' + id + ']').click();

                }, 350);

            },
            fail: function(data) {

                alert(data);

            }

        })

    }

    function loadConnections() {

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* following url */
        var following_url = api_url + '?do=list-following';

        /* load following */
        $(document).load(following_url, function(data) {

            var status = JSON.parse(data);
            var data = status.payload;

            var output = '';

            if (data != '') {

                for (var i = 0; i < data.length; i++ ) {

                    output += '<li><a href="#" class="stream-item-user-fullname" data-uid="' + data[i].id + '" >' + data[i].fname + ' ' + data[i].lname + '</a> (' + data[i].username + ') </li>';

                }

                $('.user-following-list').empty().append(output);

            }

        });

        /* followers url */
        var followers_url = api_url + '?do=list-followers';

        /* load followers */
        $(document).load(followers_url, function(data) {

            var status = JSON.parse(data);
            var data = status.payload;

            var output = '';

            if (data != '') {

                for (var i = 0; i < data.length; i++ ) {

                    output += '<li><a href="#" class="stream-item-user-fullname" data-uid="' + data[i].id + '" >' + data[i].fname + ' ' + data[i].lname + '</a> (' + data[i].username + ') </li>';

                }

                $('.user-followers-list').empty().append(output);

            }

        });

        /* pending requests url */
        var pending_requests_url = api_url + '?do=list-pending';

        /* load pending requests */
        $(document).load(pending_requests_url, function(data) {

            var status = JSON.parse(data);
            var data = status.payload;

            var output = '';

            if (data != '') {

                for (var i = 0; i < data.length; i++ ) {

                    output += '<li style="padding:5px 0 5px 0; border-bottom:1px solid #B5CDE7;">' +
                        '<a href="#" class="stream-item-user-fullname" data-uid="' + data[i].id + '" >' + data[i].fname + ' ' + data[i].lname + '</a> (' + data[i].username + ')' +
                        '<a href="#" class="icon ignore-request pull-right" data-uid="' + data[i].id + '">ignore</a>' +
                        '<a href="#" class="icon confirm-request pull-right" data-uid="' + data[i].id + '" style="margin:0 10px 0 0;">confirm-request</a>' +
                        '</li>';

                }

                $('.pending-requests').empty().append(output);

            }

        });


    }

    function loadLikes(userid, last_id, limit) {

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* following url */
        var following_url = api_url + '?do=get-user-liked-projects&userid=' + userid + '&last_id=' + last_id + '&limit=' + limit;

        /* load following */
        $(document).load(following_url, function(data) {

            if (data) {

                $('div#likes').empty().append(data);

            }

        });

    }

    function loadComments(project_id) {

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* comments url */
        var comments_url = api_url + '?do=list-comments&project_id=' + project_id;

        /* load following */
        $(document).load(comments_url, function(data) {

            var dat = JSON.parse(data);

            var string = '';

            if (dat.status == 1) {

                var payload = dat.payload;

                for (var i =0; i < payload.length; i++) {

                    string += '<div class="stream-comment-box" style="margin-bottom: 2px;"> ' +
                        '<span class="stream-item-username">' + payload[i].comment_user_fullname + ' &middot;</span>' +
                        '<span class="stream-item-username">' + payload[i].comment_timestamp + '</span>' +
                        '<span class="stream-comment-delete"></span>' +
                        '<p class="stream-item-comment-text" style="margin:0;">' + payload[i].comment_text + '</p>' +
                        '</div>';

                }

                $(".stream-item-comments-list-" + project_id).empty().append(string);
                $("form#comment-post-form-" + project_id).show();

            }

        });

    }

    function getPendingFollowRequestCount() {

        /* api endpoint */
        var api_url = 'ws/api.php';

        /* comments url */
        var comments_url = api_url + '?do=get-pending-count';

        /* load following */
        $(document).load(comments_url, function(data) {

            var dat = JSON.parse(data);

            var string = '';

            if (dat.status == 1) {

                var payload = dat.payload;

                if (payload[0].pending_count >= 0) {

                    $('.pending-requests-count').html(payload[0].pendig_count);

                }

            }

        });

    }

    getPendingFollowRequestCount();

});
