
(function(){
	/** The App **/
	var app = {
		// app settings
		settings: {},
        // debug mode
        debug: '',
        // plotted points
        points: [],
        // recent colors
        recentColor: [],
        colorIndex: '',
        // image layer
        imageLayer: {},
        // current
        current: null,
		// request params
		request: {
			targetImage: '',
			oldColor: '',
			newColor: []
		},
        // request number
        reqnum: 1,
        index: '',
        polygons: {},
		// kinetic stage
		stage: {},
		// the canvas
		canvas: {},
        // buffering canvas
        buffer: document.createElement('canvas'),
        // the canvas cache
        canvasCache: {},
		// jscrollpane panel
		panel: {},
        // error message
        error: null,
        // room image
        room: new Image(),
		// main layer
		layer: new Kinetic.Layer({listening: true}),
		// tooltip layer
		tooltipLayer: new Kinetic.Layer(),
        // shapes layer
        shapesLayer: new Kinetic.Layer(),
        // tooltip
        tooltip: {},
        // polygon group id
        gid: [],
        // adjusted flag
        adjusted: '',
        // image cache
        imgCache: '',
		/** utility functions **/
        showDialog: function(content){
            $('.modal-title').html('Attention!');
            $('.modal-body').html(content);
            $('#error-modal').modal('show');
        },
		getNewColor: function(evt){
            /** Redundant **/
            var picker = app.settings.app.boxa.firstChild();

		},
		getOldColor: function(evt){
            /** Redundant **/
            var shape = evt.targetNode;
            if(shape){
                app.canvas = shape.getCanvas();
                var ctx = app.canvas.getContext('2d');
                var mousePos = app.stage.getMousePosition();
                var x = mousePos.x;
                var y = mousePos.y;
                // the image data
                var p = ctx.getImageData(x,y,1,1);

                app.request.oldColor = app.rgbToHex(p[0], p[1], p[2]);

            }
		},
        plotThePoints: function(){
            if(app.debug == 'plotter') {
                app.stage.on('click', function(evt) {
                    var shape = evt.targetNode;

                    if (shape) {
                        var mousePos = app.stage.getMousePosition();
                        var x = mousePos.x;
                        var y = mousePos.y;

                        app.points.push(x,y);
                        console.log( app.points.toString() );
                    }

                });
            }

            return app;
        },
		getMousePos: function(canvas, evt){
            /** redundant **/
			var rect = canvas.getBoundingClientRect();
			return {
				x: evt.clientX - rect.left,
				y: evt.clientY - rect.top
			};
		},
		updateToolTip: function(tooltip, x, y, text){
			tooltip.getText().setText(text);
			tooltip.setPosition(x, y);
			tooltip.show();
		},
		rgbToHex: function(r, g, b){
            if (r > 255 || g > 255 || b > 255)
                throw "Invalid Color Component";

            return ("000000" + ((r << 16) | (g << 8) | b).toString(16)).slice(-6);
		},
        sortColors: function(colors) {
            for( var c = 0; c < colors.length; c++) {

                var rgb = hexToRgb(colors[c]);

                var r = rgb[0].toString();
                var g = rgb[1].toString();
                var b = rgb[2].toString();

                var hsv = rgbToHsv(r, g, b);

                colors[c].hue = hsv[0];
                colors[c].sat = hsv[1];
                colors[c].val = hsv[2];
            }
            return colors.sort(function(a,b){return a.hue - b.hue;});
        },
        updateRecentColors: function(color) {

            if(app.recentColor.length < 5) {

               app.recentColor.push(color);

            } else if (app.recentColor.length >= 5) {

                app.recentColor.splice(0,1);
                app.recentColor.push(color);

            }

            for(var j = 0; j < app.recentColor.length; j++) {
                var colr = app.recentColor[j];

                $('div [class="mini-box-' + j + '"]').css('backgroundColor', 'rgb('+ colr[0] +')' );
                $('div [class="mini-box-' + j + '"]').attr('data-name', colr[1] );
                $('div [class="mini-box-' + j + '"]').attr('data-code', colr[2] );
            }

        },
		changeThePallette: function(){
            var config = app.settings.app;
            var points;
            var colors = [];
            var dataString = '';
            config.indicator.show();

            $data = $.ajax({
                url: 'xml.php',
                context: document.body,
                cache: true,
                success: function(res){
                    var dat = (!res) ? 'Failed to load' : res;
                    points = $.parseJSON(dat);

                    for(point in points){
                        colors.push(point);
                    }
                    sorted = app.sortColors(colors);

                    for(var i = 0; i < sorted.length; i++){
                        var colorCode = points[sorted[i]][0];
                        var colorName = points[sorted[i]][1];
                        var col = sorted[i].replace("'","").substr(1,6);
                        var colorVal = hexToRgb( col );
                        var title =  colorName + ' ' + colorCode;

                        dataString += "<a title='" + title + "'data-name='" + colorName + "' data-code='" + colorCode + "' class='box' style='background: rgb(" + colorVal.toString() + ");'></a>";

                    }

                    app.panel.getContentPane().html(dataString);
                    $('a.box').miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
                    config.boxi.miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
                    config.indicator.hide();

                    app.panel.reinitialise();
                },
                error: function(res){
                    app.panel.getContentPane().html(
                        'Some items have failed to load,' +
                            'please try refreshing the page [F5]');
                }
            });
            app.panel.reinitialise();
		},
		resetColor: function(polyGroup, canvas){
            /**
             * Resets the color of the image on the stage,
             *
             * @param   polyGroup Object    the defined sections
             * @param   canvas    Element   the canvas containing the image
             *
             */

            // Define and init the variables
            var sx = sy = sw = sh = xc = yc = 0;
            var k= 0;
            sw = canvas.width;
            sh = canvas.height;

            // Get the 2d canvas context
            var ctx = canvas.getContext('2d');
            // Set the canvas composite mode
            //ctx.globalCompositeOperation = "lighter";
            // Get the image pixels from the canvas
            var imagedata = ctx.getImageData(sx, sy, sw, sh);
            // Get the pixel data
            var pixels = imagedata.data;

            // Display any errorors
            if(app.error){
                app.showDialog(app.error);
            }

            // White color for adjustment
            var white = rgbToHsv(255, 255, 255);

            // enter the loop with no errors
            if(!app.error) {
                // loop through the pixels
                for (k = 0; k < pixels.length; k += 4) {

                    // store the original pixel RGBA values
                    sR = pixels[k];
                    sG = pixels[k + 1];
                    sB = pixels[k + 2];
                    sA = pixels[k + 3];

                    // get the hsv representation of the original pixel RGB values
                    var oldHsv = rgbToHsv(sR, sG, sB);
                    // get the hsv representation of the new RGB values
                    newRgb = hsvToRgb(white[0], 0, Math.min(oldHsv[2], 100) );

                    max = Math.max(sR, sG, sB);
                    min = Math.min(sR, sG, sB);
                    if( (max == sB) && (sR <= 77)) {
                        newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.25) );
                    }

                    if( (app.current == 'bathroom-03') || (app.current == 'bathroom-01') ) {

                        if (max == sR) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.25) );
                        }

                    }

                    if(app.current == 'bedroom-06') {

                        /*if( (max == sR) && ( sG <= 165) && (sB <= 180) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.15) );
                        }*/
                        if( (max == sR) && ( sG <= 165) && (sB <= 165) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.25) );
                        }

                    }

                    //newRgb = hsvToRgb((white[0]) , (white[1]) , Math.min( (oldHsv[2] / 0.65), (oldHsv[2] / 0.75) ) );
                     // 181

                    // Calculate the x,y coordinates of the colors
                    xc = ( Math.floor(k / 4) % sw );
                    yc = ( Math.floor(Math.floor(k/sw) / 4) );

                    // Loop through the wall sections

                    // Store the various wall sections
                    var poly = (polyGroup.poly)? polyGroup.poly : [{x:0,y:0}] ;
                    var outer = polyGroup.outer;
                    var inner = (polyGroup.inner) ? polyGroup.inner : [{x:0,y:0}] ;

                    //
                    if(poly) {
                        // Loop through the polys
                        for(var i =0; i < poly.length; i++) {
                            // Check if the pixel is in the poly
                            var cord = isPointInPoly(poly[i], { x:xc, y:yc});
                            // If its in the poly
                            if(cord){
                                // Transform the pixel
                                pixels[k] = newRgb[0];     /* New red color */
                                pixels[k + 1] = newRgb[1]; /* New green color */
                                pixels[k + 2] = newRgb[2]; /* New blue color */
                                pixels[k + 3] = sA; /* Maintain the Alpha channel */

                            }

                        }

                    }

                    //
                    if(isPointInPoly(outer, { x:xc, y:yc}) ){

                        pixels[k] = newRgb[0];
                        pixels[k + 1] = newRgb[1];
                        pixels[k + 2] = newRgb[2];
                        pixels[k + 3] = sA;

                        for(var i =0; i < inner.length; i++) {
                            var hole = isPointInPoly(inner[i], { x:xc, y:yc});

                            if(hole) {
                                // repaint
                                pixels[k] = sR;
                                pixels[k + 1] = sG;
                                pixels[k + 2] = sB;
                                pixels[k + 3] = sA ;

                            }
                        }
                    }
                }
            }

            ctx.clearRect(sx, sy, sw, sh);
            ctx.putImageData(imagedata, sx, sy);

            // hide loading indicator
            app.settings.app.indicator.hide();


        },
        changeColor: function(polyGroup, newColor, canvas) {
            /**
             * Changes the color of the target section,
             *
             * @param   polyGroup Object    the section definition
             * @param   newColor  Array     the new color in [R, G, B] format
             * @param   canvas    Element   the canvas containing the image
             * @param   mode      String    the operation mode; transform (default), adjust
             *
             */

            // Define and init the variables
            var sx = sy = sw = sh = xc = yc = 0;
            var k= 0;
            sw = canvas.width;
            sh = canvas.height;

            // Get the 2d canvas context
            var ctx = canvas.getContext('2d');
            // Set the canvas composite mode
            //ctx.globalCompositeOperation = "source-atop";
            // Get the image pixels from the canvas
            var imagedata = ctx.getImageData(sx, sy, sw, sh);
            // Get the pixel data
            var pixels = imagedata.data;

            // Ensure that the new color is set
            if(newColor.length <= 0){
                app.error = "<h2>First select a color from the palette.</h2>";
            }

            // Display any errorors
            if(app.error){
                app.showDialog(app.error);
            }

            // enter the loop with no errors
            if(!app.error) {
                // loop through the pixels
                for (k = 0; k < pixels.length; k += 4) {

                    // store the original pixel RGBA values
                    sR = pixels[k];
                    sG = pixels[k + 1];
                    sB = pixels[k + 2];
                    sA = pixels[k + 3];

                    // get the hsv representation of the original pixel RGB values
                    var oldHsv = rgbToHsv(sR, sG, sB);
                    // get the hsv representation of the new RGB values
                    var newHsv = rgbToHsv(newColor[0], newColor[1], newColor[2]);
                    // console.log(newHsv[0], newHsv[1], newHsv[2]);

                    /* Color management code, */
                    /* Adjust the image transformation to match the palette  */
                    // TODO: Adjust the logic, base it on color tones/hues?

                    // check if the sum of RGB is below 300
                    var sum = (newColor[0] * 1) + (newColor[1] * 1) + (newColor[2] * 1);
                    if ( (sum < 300) && (sum > 210) ) {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.58) );
                    } else if (sum == 206)  {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.32 );
                    } else if (sum == 210)  {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.42 );
                    } else if (sum == 228)  {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.38 );
                    } else if (sum == 534)  {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.72 );
                    } else if (sum == 179)  {
                        newRgb = hsvToRgb((newHsv[0] * 2) , (newHsv[1] ) , (newHsv[2]) );
                    } else if (sum < 210 )  {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.69) );
                    } else if (sum < 180)  {
                        newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.78) );
                    } else {
                        newRgb =  hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) );
                    }

                    // Calculate the x,y coordinates of the colors
                    xc = ( Math.floor(k / 4) % sw );
                    yc = ( Math.floor(Math.floor(k/sw) / 4) );

                    // Store the various wall sections
                    var poly = (polyGroup.poly)? polyGroup.poly : [{x:0,y:0}] ;
                    var outer = polyGroup.outer;
                    var inner = (polyGroup.inner) ? polyGroup.inner : [{x:0,y:0}] ;

                    //
                    if(poly) {
                        // Loop through the polys
                        for(var i =0; i < poly.length; i++) {
                            // Check if the pixel is in the poly
                            var cord = isPointInPoly(poly[i], { x:xc, y:yc});
                            // If its in the poly
                            if(cord){
                                // Transform the pixel
                                pixels[k] = newRgb[0];     /* New red color */
                                pixels[k + 1] = newRgb[1]; /* New green color */
                                pixels[k + 2] = newRgb[2]; /* New blue color */
                                pixels[k + 3] = sA; /* Maintain the Alpha channel */

                            }

                        }

                    }

                    //
                    if(isPointInPoly(outer, { x:xc, y:yc}) ){

                        pixels[k] = newRgb[0];
                        pixels[k + 1] = newRgb[1];
                        pixels[k + 2] = newRgb[2];
                        pixels[k + 3] = sA;

                        for(var i =0; i < inner.length; i++) {
                            var hole = isPointInPoly(inner[i], { x:xc, y:yc});

                            if(hole) {
                                // repaint
                                pixels[k] = sR;
                                pixels[k + 1] = sG;
                                pixels[k + 2] = sB;
                                pixels[k + 3] = sA ;

                            }
                        }
                    }
                }
            }

            ctx.clearRect(sx, sy, sw, sh);
            ctx.putImageData(imagedata, sx, sy);

            // hide loading indicator
            app.settings.app.indicator.hide();

	    },
        getPoints: function(){
            var url = app.settings.app.baseURL +  app.settings.app.dataDir + '/' + app.current + '.js';
            var points;

            var cur = app.settings.app.image.split('/')[app.settings.app.image.split('/').length -1];
            app.current = cur.substr(0, cur.indexOf('.'));

            $data = $.ajax({
                async: false,
                global: false,
                url: 'data.php',
                data: {
                    r: 'js',
                    f:  app.current + '.js'
                },
                success: function(data){
                    points = $.parseJSON(data);
                },
                error: function(data){
                    app.error = error;
                }
            });

            return points;
        },
        getPolys: function(){
            var url = app.settings.app.baseURL +  app.settings.app.polyDir + '/' + app.current + '.js';
            var polys;

            var cur = app.settings.app.image.split('/')[app.settings.app.image.split('/').length -1];
            app.current = cur.substr(0, cur.indexOf('.'));

            $data = $.ajax({
                async: false,
                global: false,
                url: 'data.php',
                data: {
                    r: 'poly',
                    f:  app.current + '.js'
                },
                success: function(data){
                    polys = $.parseJSON(data);
                },
                error: function(data){
                    app.error = error;
                }
            });

            return polys;
        },
        saveImageData: function(data){
            /**
             * Save the image on the stage
             *
             * @type {*}
             */
            var config = app.settings.app;
            var ret = '';
            config.indicator.show();
            $data = $.ajax({
                url: config.palletteScript,
                type: 'post',
                data: {
                    r: 'svimg',
                    d: data
                },
                context: document.body,
                cache: true,
                success: function(res){
                    var dat = (!res) ? 'Failed to load' : res;
                    ret = dat;
                    config.indicator.hide();
                },
                error: function(res){
                    app.error += "Failed to save image";
                }
            });

            return ret;
        },
        shareImage: function(image, network) {
            /**
             * Share the saved image on the selected social media network
             *
             * @type {string}
             * @private
             */

            var _FB = "https://www.facebook.com/sharer/sharer.php?u="; // facebook
            var _TW = "https://twitter.com/share?url="; // twitter
            var _IN = "https://www.linkedin.com/shareArticle?mini=true&url="; // linkedin
            var _GP = "https://plus.google.com/share?url="  // google +

            var _image_url = app.settings.app.baseURL + '/tmp/' + image;
            var _share_title = 'Share Page';
            var _share_url = 'http://crownit.co.ke';

            if(network == 'facebook') { _share_url = _FB + encodeURI(_image_url); _share_title = "Share via Facebook"; };
            if(network == 'twitter') { _share_url = _TW + encodeURI(_image_url); _share_title = "Share via Twitter"; };
            if(network == 'linkedin') { _share_url = _IN + encodeURI(_image_url); _share_title = "Share via Linkedin"; };
            if(network == 'google') { _share_url = _GP + encodeURI(_image_url); _share_title = "Share via Google Plus";};

            window.open(_share_url, _share_title, 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=600');

        },
        saveToPdf: function(data) {
            /**
             * Save the image data to pdf and download it
             *
             * @param data
             */

            jsPDF.API.render = function(data){
                // the pdf header
                this.setFontSize(40);
                this.text(35, 25, "crown color visualizer");
                // pdf image
                this.addImage(data, 'JPEG', 10, 40);
                // the pdf header
                this.setFontSize(10);
                this.text(550, 20, "Palette");

                this.setProperties({
                    title: 'Crown Color Visualizer - Image Export',
                    subject: 'Image Export',
                    author: 'crownit.co.ke',
                    keywords: 'Powered by My-IT-Provider.com',
                    creator: 'crown color visualizer'
                });

            };

            var pdfdoc = new jsPDF();
            pdfdoc.render(data);
            pdfdoc.save('CrownColorVisualizer-ImageOutput.pdf');

        },
		loadIt: function(what) {
			var config = app.settings.app;
			config.indicator.show();
			$data = $.ajax({
				url: config.palletteScript,
				data: {
					r: what
				},
				context: document.body,
				cache: true,
				success: function(res){
					var dat = (!res) ? 'Failed to load' : res;
					app.panel.getContentPane().html(dat);
					config.boxa.miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
                    config.boxi.miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
					config.indicator.hide();

                    app.panel.reinitialise();
				},
				error: function(res){
					app.panel.getContentPane().html(
						'Some items have failed to load,' + 
						'please try refreshing the page [F5]');
                }
			});
			app.panel.reinitialise();
        },
        removePaint: function() {
            /**
             *  Restore the image on the stage
             *
             * @type {*}
             */
            var img = new Image();
            img.src = app.imgCache;
            app.imageLayer.setAttr('image', img);

            app.imageLayer.draw();

        },
		/** event bindings **/
        bindEvents: function(){

            app.stage.on('click touchend', function(evt){

                var shape = evt.targetNode;

                if (shape) {

                    var key = shape.getAttrs().key;

                    if(app.adjusted == 'ok')
                    {
                        var polyGroup = app.polygons[key];

                        app.changeColor(polyGroup, app.request.newColor, app.canvas);

                    }
                    else
                    {
                        var msg =  '<h2>Adjust the image to transform </h2>';
                        msg += '<a class="btn action-adjust" data-target="#" style="display: inline;" title="Adjust the image for painting"><i class="icon-adjust"></i></a>';

                        app.showDialog(msg)
                    }

                    if(!key)
                    {
                        app.showDialog('<h2>You cannot transform this section.</h2>');
                    }

                }

            });
            
            app.shapesLayer.on('mouseover', function(evt) {
                var shape = evt.targetNode;
                if (shape) {
                    shape.parent.setOpacity(1);
                    app.index = shape.parent._id;
                    app.shapesLayer.draw();
                }
            });

            app.shapesLayer.on('mouseout', function(evt) {
                var shape = evt.targetNode;
                if (shape) {
                    shape.parent.setOpacity(0);
                    app.shapesLayer.draw();
                    //app.tooltip.hide();
                    app.tooltipLayer.draw();
                }
            });

            /**
             * Reset the stage
             *
             */
            $('.action-refresh').live('click', function(){

                 app.removePaint();

            });

            /**
             * Share on social media
             *
             */
            $('.share-action').live('click', function(){

                var image, img, network;

                network = $(this).attr('id');

                try
                {
                    img = app.canvas.toDataURL('image/jpeg');

                } catch(e)
                {

                    console.log('Error: ' + e );

                };

                image = app.saveImageData(img);

                app.shareImage(image, network);

            });

            /**
             * Save the image to pdf
             *
             */
            $('.action-print').live('click', function(){
                var image, img;
                image = new Image();
                try {
                    img = app.canvas.toDataURL("image/jpeg", 1);

                } catch(e) {

                    console.log(e);

                };

                app.saveToPdf(img);

            });

            /**
             * Adjust the image for painting
             * Make all the walls black
             *
             */
            $('.action-adjust').live('click', function(){

                console.log('adjusting...');
                var polys = app.polygons;
                var canvas = app.canvas;
                for(poly in polys)
                {
                    console.log( poly );
                    app.resetColor(polys[poly],canvas);

                }
                var adjusted = canvas.toDataURL('image/png');
                app.imgCache = adjusted;
                app.adjusted = 'ok';
                //console.log( adjusted );

            });



            // TODO: merge the two bindings below
            app.settings.app.box.live('click', function(evt){
                evt.preventDefault();
                var rgbNC = $(this).css('backgroundColor');
                // set the newColor variable
                ncRGB = rgbNC.substring(4, rgbNC.indexOf(')')).split(',');
                app.request.newColor = ncRGB;
                // update recent colors
                var name = $(this).attr('data-name');
                var code = $(this).attr('data-code');
                var colObj = [ncRGB, name, code];

                // determine the main component, red, green, blue
                var r = ncRGB[0];
                var g = ncRGB[1];
                var b = ncRGB[2];
                var hue = '', reg = '';
                if( (r >= g) && (g >= b) ) {
                    var frac = ( (g - b) / (r - b) );
                    hue = ( 60 * frac );
                    reg = 'Red - Yellow';
                } else if ( (g > r) && (r >= b) ) {
                    var frac = 2 - ( (r - b) / (g - b) );
                    hue = 60 * frac;
                    reg = 'Yellow - Green';
                } else if ( (g >= b) && (b > r) ) {
                    var frac = 2 + ( (b - r) / (g - r) );
                    hue = 60 * frac;
                    reg = 'Green - Cyan';
                } else if ( (b > g) && (g > r) ) {
                    var frac = 4 - ( (g - r) / (b - r) );
                    hue = 60 * frac;
                    reg = 'Cyan - Blue';
                } else if ( (b > r) && (r >= g) ) {
                    var frac = 4 + ( (r - g) / (b - g) );
                    hue = 60 * frac;
                    reg = 'Blue - Magenta';
                } else if ( (r >= b) && (b > g) ) {
                    var frac = 6 - ( (b - g) / (r - g) );
                    hue = 60 * frac;
                    reg = 'Magenta - red';
                } else {
                    hue = 'None';
                    reg = 'None';
                }

                //log color components
                console.log(ncRGB[0], ncRGB[1], ncRGB[2]);
                var sum = (ncRGB[0] * 1) + (ncRGB[1] * 1) + (ncRGB[2] * 1);
                console.log(sum);
                console.log(reg + ' - ' + hue);


                app.updateRecentColors(colObj);
                // set darker and lighter shades
                var prevCol = $(this).prev().css('backgroundColor');
                var nextCol = $(this).next().css('backgroundColor')
                $('#dropper-min-dark').css('backgroundColor', prevCol);
                $('#dropper-min-light').css('backgroundColor', nextCol);
                //console.log(nextCol);
                // change the dropper color and details
                var code = $(this).attr('data-code');
                var name = $(this).attr('data-name');
                $('.sample-code').html(code);
                $('.sample-color').html(name);
                app.settings.app.dropper.css('backgroundColor', ('#'+ app.rgbToHex(ncRGB[0], ncRGB[1], ncRGB[2]) ) );//app.request.newColor));
                app.error = null;
            });

            $('#box').live('click', function(evt){
                evt.preventDefault();
                var rgbNC = $(this).css('backgroundColor');
                // set the newColor variable
                ncRGB = rgbNC.substring(4, rgbNC.indexOf(')')).split(',');
                app.request.newColor = ncRGB;
                // update recent colors
                //var nme = $(this).attr('data-name');
                //var cde = $(this).attr('data-code');
                //var colObj = [ncRGB, nme, cde];
                //app.updateRecentColors(colObj);
                // change the dropper color and details

                var code = $(this).attr('data-code');
                var name = $(this).attr('data-name');
                $('.sample-code').html(code);
                $('.sample-color').html(name);
                app.settings.app.dropper.css('backgroundColor', ('#'+ app.rgbToHex(ncRGB[0], ncRGB[1], ncRGB[2]) ) );//app.request.newColor));
                app.error = null;
            });

            return app;
        },
		/** Initialisation functions **/
        setupPanel: function() {
            var pn = $(app.settings.app.panel);
            pn.jScrollPane(app.settings.app.jscrollpane);
            app.panel = pn.data('jsp');

            return app;
        },
		setupAjax: function() {
            $.ajaxSetup({
               cache: true,
               async:  false
            });

            return app;
		},
		setupToolTip: function() {
            // tooltip

            var tip = new Kinetic.Label({
                opacity: 0.75,
                visible: false,
                listening: false
            });
            tip.add(new Kinetic.Tag({
                fill: 'black',
                pointerDirection: 'down',
                pointerWidth: 10,
                pointerHeight: 10,
                lineJoin: 'round',
                shadowColor: 'black',
                shadowBlur: 10,
                shadowOffset: 10,
                shadowOpacity: 0.5
            }));
            tip.add(new Kinetic.Text({
                text: '',
                fontFamily: 'Calibri',
                    fontSize: 18,
                    padding: 5,
                    fill: 'white'
                }));

                app.tooltip =  tip;
                app.tooltipLayer.add(app.tooltip);
                //app.stage.add(app.tooltipLayer);

            return app;
		},
        setupPoints: function(){
            var areas = app.getPoints();

            // draw areas
            for(var key in areas) {

                var area = areas[key];

                var group = new Kinetic.Group({
                    opacity: 0,
                    listening: true
                });

                for(var i = 0; i < area.points.length; i++) {

                    var shape = new Kinetic.Polygon({
                        points: area.points[i],
                        stroke: area.stroke,
                        strokeWidth: area.strokeWidth,
                        opacity: area.opacity,
                        key: key
                    });

                    group.add(shape);
                }
                app.shapesLayer.add(group);
            }

            return app;
        },
        setupPolys: function() {
          var poly = app.getPolys();

          app.polygons = poly;

          return app;
        },
		setupStage: function(config) {
			app.stage = new Kinetic.Stage(config);

            return app;

		},
        setupImage: function(){
            // TODO: handle indicator, image and palette loading
            var config = app.settings.app;
            config.indicator.show();
            $(app.room).load(function(){
                app.imageLayer = new Kinetic.Image({
                    x: 0,
                    y: 0,
                    width: 740,
                    height: 500,
                    image: app.room
                });


                app.layer.add(app.imageLayer);

                app.stage.add(app.layer);
                app.stage.add(app.shapesLayer);
                app.stage.add(app.tooltipLayer);
                config.indicator.hide();

                app.canvasCache = app.imageLayer.getCanvas();
                app.canvas = app.imageLayer.getCanvas();

            });
            app.room.src = app.settings.app.image;
            // cache the image
            app.changeThePallette();

            return app;
        },
		initialise: function(settings) {
			app.settings = settings;
            app.debug = settings.app.debug;

			app.setupPanel().
            setupStage(app.settings.kinetic).

            setupPoints().
            setupPolys().
            setupToolTip().
            setupImage().
            plotThePoints().

            bindEvents();
		}
			
	};
	
    // Make the app module global
    (!window.app) ? window.app = app : window.app;
	
})();
