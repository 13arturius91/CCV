/**
 * Crown Color Visualizer - Colors
 * Author/Developer: Francis Muturi <francmut@gmail.com> <https://twitter.com/TheFrancmut> <https://www.facebook.com/francmut>
 * Contributor/Developer: Simon Muraya <13arturius91@gmail.com> <https://twitter.com/SimonArturius> <https://www.facebook.com/sifusimon.arturius>
 * Copyright: 2013-2017 My IT Provider LTD
 *
 **/
(function(){
	/** The App **/
	var app = {
		// app settings
		settings: {},
        /* crown + ccv logo */
        logo: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAPwDmAwERAAIRAQMRAf/EAaIAAAAGAgMBAAAAAAAAAAAAAAcIBgUECQMKAgEACwEAAAYDAQEBAAAAAAAAAAAABgUEAwcCCAEJAAoLEAACAQMEAQMDAgMDAwIGCXUBAgMEEQUSBiEHEyIACDEUQTIjFQlRQhZhJDMXUnGBGGKRJUOhsfAmNHIKGcHRNSfhUzaC8ZKiRFRzRUY3R2MoVVZXGrLC0uLyZIN0k4Rlo7PD0+MpOGbzdSo5OkhJSlhZWmdoaWp2d3h5eoWGh4iJipSVlpeYmZqkpaanqKmqtLW2t7i5usTFxsfIycrU1dbX2Nna5OXm5+jp6vT19vf4+foRAAIBAwIEBAMFBAQEBgYFbQECAxEEIRIFMQYAIhNBUQcyYRRxCEKBI5EVUqFiFjMJsSTB0UNy8BfhgjQlklMYY0TxorImNRlUNkVkJwpzg5NGdMLS4vJVZXVWN4SFo7PD0+PzKRqUpLTE1OT0laW1xdXl9ShHV2Y4doaWprbG1ub2Z3eHl6e3x9fn90hYaHiImKi4yNjo+DlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+v/aAAwDAQACEQMRAD8A3+Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691Hmq6SneKOoqqeCSY6YY5po4nlNwtoldlaQ6mA4vyfbUk8ETBJXRXbgCQCfsB4/l1dY5HBKKSBxoCafb1I9u9U697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3XusAqqYztSiogNUq62phLGZ1Tg62h1eQLZhza3PtoTQmUwB18YCumo1U9aceraH066HR60x+3rP7d6r1737r3Xvfuvde9+691737r3XvfuvdEm+R3XX3W76PfOYrdtRbeymJxGy4shuLIV1BFtXLyZCvmpsi7QU1TGKKoapF5AUCSD9wBbP7xv8Ad7lHx+YI+Zb+SyXaZ7eKzElw7oLaUvIVkOlWGhtXxYAYdwAo3Ulcn7x4e3NtkCzG7jkaYrGqt4qaVBXJBqKcPMcM46OVio1ixeNiWt/iSxUFHGuRLiT79Up41Fb5FeRZPugNeoMwOq9z9feQ1igjsoUWTxgsSDxK110Ud9amur4q1Na8T1HU5LTuxXQS57f4c8PLhw6n+1XTXXvfuvde9+691737r3XvfuvdRayuosdTvV5CspaGkiKCSqrKiKlp4zI6xxh5p3SNC8jBRc8kgfX2zcXNvaRGe7kSKAUqzsFUVNBUkgCpIA+eOrxxSTOI4VZpD5AEn9gz1zpaqlrYI6qiqYKulmUtDU0s0dRBKoJUtHNEzxuoYEXBPI97hnhuYhPbuskLCoZSGU/YRUH8uvOjxMUkBVxxBFCPyPWHJZGhxFDVZPJVMdJQ0ULT1NTLfRFGn1NlDO7EkBVUFmYgAEkD271Tqmf50fzI16Q2nlcvSbooOr9rU8j0FNnqqmhyW7tw17JJJFQYHHSQ1gSrqoYmKxU8MtRGitK0saKxRZY7feblOLeyQvIcn0A9SeAHzP5Z6F3JnIvNHuBvK7DynaSXe4FdTUoEjQEAySyNRY0BIGpiKkhVqxUHT++SP80/tbtTI5Cp27lN55ClM0qUma7D3nuHMZRoyXs64ekywpsUyB/241q6hFB+ljp9jGPkxIYw97MS/mqCgHy1GpP+8jo/5v8AbbeOQt8fYOY9IvkVWBjqY3Vhho3ZVLLWqk6VIZWBAIPVX+5/lp8koMlJmsR3T2JtjJR809ZtPdWY21V02mWOpQQV2Hq6SvXx1EKSKTKSrorA3UEI7ja7KDtRP2kn/L0D7i3hijJUZp0K3Uf867+ah0bk6AbN+ancmdoqSQKuH7UylB3Vi5qVSENA1P21jt5yU9IYSY1FO8LwrbwtGyqVDksSGcIBQH06K2EYQu9AB59X0/Fn/hVL8laaXH4j5NdEdSdiY9mSF9z9fV24+ss/ZyAanLU1S2/cFOYySSaSipgQAugcv7OE5ZurmLxLMhnp8BNCfsPCv20+3oHXnOWx2Fx4N0ZUStNemqD5mh10/wBr1sV9Hfzo/iz29QY6fMYXsTr2oroIZxUV+Jx+6dvMk6I6tR5Xa+RrcpVx+okM2OiuoBAubAOSxyQyNFMCsqkggihBHEEdCaF47iFbi3dZIHUMrKahgcgg8CD1YJtP5PdBb2hgn272jtmoSoEZiWvlq8DNaW2gSU+fpMZUQt6uQ6qV/NrH3Tq9COhaot07Yydv4buLB5HVbT9hlqCsLXNhpFNPKTf/AA9+610/e/de697917r3v3Xuve/de6JDN1Jndj9j57tLK00X8HoN9ZrfVVufCy5DK7gbbdfUVdVJg1wVNA1bNHDS1YpanTqRKVHZdSKF942Schbpy1zhdc7XyL+74tzmvWuYTJLP9O7Oxh8BVLkBW8OSlQsYZhVRTqTF5gtdz2aLY4CfqGtUgEThVj8RQBr1k0FSNS+ZYgGhz0OWK+RvSOZCmi7DwyBr2/iMWSwx4DH1DMUNCV/Qfrb8f1F5Msvd3233AA2+7W4r/vwSRev+/UT0/wAHqOgvPyhzLb/2lpIf9Lpf/jjN0tqXsvrmtBNHv7ZdTYKzCDdODlZQ/wCnWqVxZCf6EA+xHBzlyhcitvuu2vj8NzCafbR8fn0XPsm8x/2lpcr9sTj/AJ96m/352TYn++O1rAEk/wB4MTYAfUk/d2AHtT/Wblulf3hY0/5rxf8AQfTf7r3P/lGn/wCcb/5umup7S66pRd96bdmAANqLJU+RPIYiy49qpifT/T+n9R7RT87co24q+42jD+hIsn8o9XSiPYd6kNFtZh/plK/8ep0Fu/Plb0p1zhqvcG6N34zDYOjVzUZ3cFfjtqYCnKKzsavMbpq8RS08aqhJbngX5Hsvg592vc7oWHLlvfbnuDfDHbQSOzH/AEpAfjjCnpYOV9wjTxb5obeLzLuMfsqP2kdU0fJX/hR38MekoslQbDqq/v8A3jSrNHR7b6slWTbDVaI7U4zXa2XpYttR4yoZQGnw1Nm5Yr38Te5z5F9nfdbnCRLverJNh2UkH/GTquGXzpAKOD/RlWH/AE3RHuVxstgDFbTG6ufVRRB+fA/aC32darny7/ngfO75X9g4nc6dhydLbG2pmUy+z+per5J6PaVNPArRQVG9ZMkaur7Lr3p2ImXLiXG62c09FTK5T3lHH93z2wueW5+WuZNvi3a2u00zPcir+oMJUqbdlOUeErKuP1CRXoP2+/7pZXi3thK0M6HGn/AQahgfMMCD6dWWfCP/AIUUZbYtPi9pd/4qs21BCEjm3Ltihq91bCrXaVvJU5LZMssm59p3jIeV8PU133E5JEESEIMO+avuce5ft5I997Ebyu4bBqLDadyKhkHErBc9sTVYmgb6QgU1yytUmQoedeXuYAE5stfBvaU+pgrn5smTgf8ANT5Ko62bfj5/NM+NHyBoqZ9o702tuSskgSWeHZO5sVm6+mBiExlym1K6fF7rwK+O7GKemeRLc35Ihncuc+beSpBa+6PLO77NMDp8Xwi9sxrSqSnSjKTw8N5vLJrXpX/VW0vx4nL1/bXScdJOlwPQgVNf9ME+zo72J7q6vzKq1Ju7HwlrejIpV4plJ/BORp6ZDb+oJH+Ps2sPcbkrcVDQX8Kk+UgaI/8AVRVH7CR8+im45Y322JEls5p/DR/+Ok9LOn3XtarCGl3LgKkPfQafM46YPpvq0eOpbVaxvb+nsQxb7sk9PAvLR68NM0Zr9lG6LX27cI8SQTLT1Rh/hHU8ZbFtbTksedXAtWU5uf6C0nPtWL6xPCaLP9Nf8/TP09wOKP8A7yf83UuOeCUExTRSgfUxyLIB/r6Cbe31kjcVRgR8jXpsqwwQQegC7+2t/pBxm09nY2sxD59N0wbopsHlZKlKfK4/D4fOU9YlXJSRzy0VMGr1KTPG0bTqkfBbUsV+6myf1ss7Dl6zkgO6i+W5WGUsFljiimVwxQMUXvFHKlS4VME6lFfKl6domuNxmWQWhgMRdaVVndCKVIBPbkA101PlQrPqPY1X19tEYOtlozPPlcjlmosa9RJi8SMg6N/DcdJVLHPLTRGMyFmRNUsjm35Ij5C5ZuOVNh/dty0fivPJLojLGOLxCD4cZajFRTUSQKszGnmS3mDdI923D6qMNoEapqagZ9P4mAwCeFKmgA6CL5Vb/wARsXr7cu49x5D+GbT2bt7Mbt3NWG7LDjsJjqnJVk7xr6pvtKKmdlQcs5sATb2Lbq5is7aS6nOmCNCzH0VRUn9g6py/se5czb5Z8u7NGZt3v7qK3hQfilmdY0WvlVmAJOAMnHXzq/ld292X8vO2s52TuyuqMfts1ddSbH23MddNtLajVTPQ4+joIWWKWumgVJK2qJD1U41X8axogi9ovdzkXf3TlW9kj2re5HoGlasdwSaKFlwqy0wInKgmgjZ2Yr19FPsf90uDkjkq25Z2oQ2auiPd3jqWmvJ9IDyhO1vDBqIEdkRI6aaszsxZZ9nda7RoK7P9g7gnxm2qKHzZbK12QpsZj6JP83FIzeAyFpZpFjVFdpJHZVQMxCnKKfljl+xge83ed1tUFWZmVFH8q5OAKkk0AzxBP3vPugcjQ+zu4+4Cbm8XMuxRfUCS6lggt5YagTW1SikSy1BtlMrNJcLHAgrMSSE9j/Kj4hmrqMbtXbncOeiSVYTn6GfCYfHzReRvJV0NLuD7jKTjxqNKT01IWDG+ggH3Em5797ZtceFBFukkIOXjaNKjzKiUMT9jKv5dcG913DZ3heKwWUz07WOE48aHuI/IdLDoHr/qX5S5SpxHVXcdJtnsKGOSoxvWfamEmw2YzFPDDNUVdTi87g6jL4vJpSRwlnipYp6pIVaWSKNByJOSfazlj3QuntuTd+jtuYVBKWW4RGOSQAEsUmhMiPpAqQiM4UF2RVHWJfv794C59i7S03Xe+W913LkmaoutwsmhZbNy6LEkkMjIx8XUaPJJDFq0Rq7u5CGmp/h58gdqTiOfZS5iBGVVrMBmcPkYpjq0jxUhrYMrzwfVTrwf9e0gJ7A+6WwP+vt4ngX8cM0LqfnpLrJTzygx+dMbZvve+wfNsVbXeWtbp/8AQrm2uImHyLiN4SfKiytn8q3B/wAvfof5MZDPS9bTdQ77leaKfMbap6jD1EEiimVp8zTRSVPjgFM0R+4QalGoSEatXuEvcvYhbul+kbLdIfDnIFUxhDrHaSPhJBII0iuOsi/Yn3H2rd5pOWor6CaGRDNarr/Up8UqhD3KtKSBSFIOs07utm7oj4Wd/wAP2Uu49r0Wz6b0M02fzmLaQKttV6HDVGXr0f8AoHiS5/IHPuJusmGYdW1da9SUGwqSE1Vc2Zyiot5/D9tRwMFF/t6cvI7spvZ3b+hCqffuqE16F/37rXXvfuvde9+691737r3XFraST9ACf9tz70eHWxx6+Yd84N/fKX4SfNj5I9EbN777iwG2di9p52TY+Hffe46rEwde7lMO7euvFhshX1eIVf7jZzHqdEIQ6bAAAKMx+Xvab2U9yOUbHf8AduV+X7i7ubceLKLK3SRpVJSassaLIT4quctWpJ4knpCeY+ZNtnaGC+ulRThfEcgDy7SSOFPLoJsP/NI+euC8RoPkPnJPDD4E/iu0euM9dBp9UgzuzciJp/QP3X1SfX1cm6G6+6D93G7r4nLFutWr+nc30Ofl4V0lB/RFF+WOlie4HOEYoL1yKUykTf8AHkP7ePT/AFP827+YXVU8lLJ8h6mKOTTqej6z6bx9SNEiyDx1lD13TVcN2QX0OupbqbqSDS2+5/8AdztJBLDy4upeGq93JxwplXvGU/mDQ5GQOvPz/wA3OpVrw0PpHCP5iMHoNdzfzGfnPuyOWPKfKPt6kSe/kG2NzzbKNiIlKo2zY8C0SkQjhSBy3+qa4x2r7vXslsxDWXLG0sV4eNCLj1/5SDLXj5/L0FC+fmzmS4xJez/7VtH/ABzT1zymLm3otBk+wsnuDe+4RRw/eZndm48/nspNUyxxvVu9Zk8lUzjyTgkjVbgf098yt6+8T7lbdzFuKcl7ku28ttezfTwW1taRxRwCRhCqgQZ0x6QWNSxqSanr6A+SPuVewSckbPBzfy5Ffczpttv9XPLcXniS3JiQ3DtpuFA1SliFACoKKoAHRxfiX/L6w3fWQi3XunAyYLqjH1MsVTkoKqphye6K2lYLLiMD5J5DFTxSHTU1rIY47GKPVKH8Uec1ffH97tjjNrbcwzvujDAMNrRAfxN+hx9FrU8TQUrC/vp7TfdN9sLVtj2XlLb7nniVAVjM9+Y7ZG4Sz0uhViMxwghmw70TTrHT5Mfysdu4KSn3R8f9oz5zETPSUeU2HNk66qy+MmkaOnTKYfIZHJCTJY2eRgaiGV/NSkmQM8JYQEnLn32PfCcG237mOdJRUrL4FqFbz0sBb4PoRg8MH4o19neVfup7mrbL7pcr7fbX6hnjvRLfLFIBVjHKiXFI5AMRso0SCiELIAZR4+PP8rTpTaWCkyvdm08Tvfd2ZojFJgVr8t/d3a0FQgL09NNS11NNk84oNpKsMIoW9NODp88hDzF99T7wF9MIdo5jvILSNwwYRWwdypxWkGE/oGuofHUHSI390eWvYXd90+j9tuVrXbtihYjxmkuXnuDkaiss7rDH5ooHifidlJ8NK4+9f5V/cmzu9tubc6SpchuXrbfGaMu3N6TtUKOtUhkFVVU+/wDIUULSY9MLCNdLXIg/iCqqRL90TD76Ie1P39PbjmL2rvN79zZIrHnfaralxZrpruJI0o1hG5AkMzYkgLf4uSWdvApJ1hvzB7Sb3Z7/AB2uxK0u13D9khr+j5kTEDGkfC1O/gBr7ehg+cu/+9fgvkeiNidHfIvvLGrP17k6jdGRyfZW7s1T7nz1LkxQy5qXbmezOZ29iNcUrLFT0kEccMaxi7tGsnsg+6VPyx95Sz5q5r9w+X9ifRvEa2sMVlbxfSwNFrEKzRRRTS0IBaSV2Z2LHtVigUe4sF1yTLt+37PdXQrbMZGaR28Rw1NRVmZV+QUAAU4kV6LLiv5xH8yDDRvFRfJXIOryeQnI9a9M5mUNpVLRzZjrqumijso9CsFvc2uSfeUUn3V/YSVg55fjUgU7bu+QfsS6UE/MivUfDnXmf/lKJ+1Ij/hTp8l/nW/zOpadaVflDkKSNZElVsb1Z0diqjUgZVBrMZ1lSVZjs5uhfQxsSCQLGNj9232V259dpsig0/FdXrj9j3LD86V6Szc0b7cf2s5P+0jH+BR0E+8f5pH8xbfUFRS535ofIanpKtZUqqPa3ZGe2JR1MM/nE1NPS7Gqdu081JKlS6tCymIpZNOlVAHO3+2ft/tYAs9nsBppTVEshFKUNZNZrjjWtc9FUl9dymryOSfnT/BTre//AJKnT26tgfEnrLcXYma3Fuff+6dkY7d+59wbty2Tzm4qvN9nTf34qKLKV+Ynqa77zbu36nFY0qzkqtNY83J5t8yX9vzV72czcy2iRptdnKNttgiqECW9ElKBRSjvH4oI4+KfLHUl3YNhyzYbYSfFkBnkrxq3w1+wMR/terlvZl0HOqUP53tduSn+JLbR2rBNXbi7s7Z2T1Vi8XS1NNTVdZSrS53f2VIepmgUUk0OxVpJbsqMtSUc6Ws0Y+7e4yWPKTRREh7mdIjTjp7pD+R0UPyNPPrPL+7p5c2DdPvCf1p5o0DZOWdkvNyZnQvGsmqGyjLABqspvDKgoSGjDAVWo1Iv9lE+Rv8Az6/K/wDn123/APXr3in13x/19/aX/o8wf847j/rV0uv5Vv8ALf8AjN/NZ+YHye6f+WeX3JV9efDjYnV2RTo7au86vZdb2V2J29Vbu++3hm8xgKiDPVO2Oq8HtmlpTBQywn+JZ+F5KgRk08+ZfttzFzZunIVtZ77fz3W3JK5t0kOoxxr+mF1nvcKVbQHZhGp0oFXHXF7+8599N25v90Lf2g2y4kXk7YbW3uJY1Zgtxe3kCXKyyLjUIbSaFIQwJjaS4IIMhA1/f5+v8v3o7+Wr/MW3x8cvjvufNZzq+p2DsLszD4Dc2Wgz+5OtqjfFLkJavYGXzcSQVGUWh/hy19BLVRJWfwrI0qztUSK1VOL+uXvVOOBz2a2vm8TuTbmUrsJn8FkKTK4bMYyplpMhjMlQzJUUdbR1MLLLBUU88YZWBuCPauwvr3a72LcduleC/gkV45EJV0dTVWVhkEEVB6L912rbd92y42beYIrrabqF4poZFDxyRupV0dTUMrKSCD19CL+Xd2Dt75A/FHpbvyTHUz7v3htqen3ZUyKj/bby2tmcntLdTY2l0LHiaOrzuDqKimjVRItLNGC7rZjmtN7vc2+4mwW8+5TiO1eIB4oR4aO6djs4Bq2plLaWJQVGlR1xR5s9jOU/Z33C3PYdqhaVre41QyzHxJFhlVZoVUkUBSORUZ1Adip1MTUdWy/GLOVW3vkJ07X0jMktRv8A29g3KmxNLuetTbdcp5Hpeiy0gP8AgT7j3mmBLnly9jfgLd2/NBrH81HUi+zu4zbV7p7BcwEh23SCE/6W4cW7/tSVgfl1st+8YOuvPXvfuvde9+691737r3Xvfuvde9+691737r3Wgl/wpp6OfaPyq667so6JIKPsbZtdsbOyxIbzbh67ro63E5Kvc8ipymyt3Y+mi/DRYs2/SSckPuoc1tdWO+8iXbH6rar9Zogf+Ue6WqgfJXjZj85R6jqnNNkFitN0j/s7iKjf6dDQ/wCGn+161oveXfQQ697917oa8T8e+9p6jG1bdH9wzYyaajqGqIOst7TxS0MjxyGaF4cIwmjkpzqUqSGB49hPmLeLd9hv4dou7Q7ybSdYQZol/W8NhGCWai9+kEtgeeOhJyidqh5r2ybmHWvL6bhbNckKzEW4mQzEKvcxEeohVyeAz0YPILnMTXVGLymyOxcdkqURNU0Ff19u+irYFqIxLTvNSVOHiqIVniIdCyjUpuOPfJaL7q3vJNGJUs7Eo3pf2R+0VExGOBoT137b7/8A92BWKjerw0PEbdf5+ebcGh+YB+XVh/wb+W26+uaqs6hr+p+7OxcFkYspuXbuF2B1bu3c28sLUUv238akosDDjoqiu29USVELVDK6rR1Egfnzt7APOn3KveHclG6xW+2QXC0R2k3GzSNga6at4tAwoafxAU/COsWffv37+7L7n3cPMnK2+y2/NSqsUol2/cFiniX4WLJauyyxA0B0HWlFJGhelH3V8oPmu+54ux6PoX5F9S9Zde1EmeOOy3R/a2O29PiKEM1dWdl5qr2vQUdbj6ij1CaN5IaOkQho7TJ9yzOz/ct57W1O3yRbTdbjONOoblYFgx4CFROSCDwNCzeeDp6Wcn+7H3JNh5VuNp5i3S93He7yArLdNtl8pgJyDZg258ExtQiQ6nkIIkPht4QGbsT+aRsHBdBLvfbu090R9wZ2Vtt4DrzP7b3BQ0dBuWekMkebrMvV42hpMttenDCaFadxWVrWg0Qnyyw39v8A7gnulu3uPDsXPCQ2PI8Sie5vIriCYvCHobeFY5HK3MlCpLjw4VrKS/ZHJiFz17pcj7MLoclX/wC90DlbdzBPb6gRUPLHMiMoStGVSwZhpRiveAF+C38xLfv956vqT5Yy7hatz8tZndmdgZbbVTQVOP8AJJNLW4TcdFj8XTFNvvMD9jXLB46N7wSsIDG0E2/eq+4/ti7XDzt7A2SK1tGsV3tkUhcyqoCpcWxkdiZgP7eEvWYUljHihxLHnt/7rSmdts5vmqrsWjnYABSclJNIAC/wNTt+E9tNJNf5j/c0vyl7dwlb1hsTemR2R17hq/a+L3b/AHdzZh3hUzZSSqyOWx1L/DlelxEdRH4qbyHzTKhkdY9QjXJf7lfsrvfsd7cXMXOlxBHzJvNyl1JahlJtFWMJHFI4Yh5iCWk09iEhFL6SxA3ufzVZ81b2jbYrGytkMayGv6hLVLAUqF8lrkjJArQVsZbbW48AsD53b+bwqVLSLTNlsVX41ahogplWBqyCEStGHXUFva4v9feZkVxBNUQujkcdJBp+w9RoQRxHTJ7e610Y74g9I1XyP+T3RvSVPBJUU3YHYu38XnhF5PJT7PpKn+Mb3yCeIrITi9n42uqeGX/NfqX6gF+4vM6cmci7rzQxAks7KR46+cxXTCuf4pWRfPjwPS/a7T67cYbT8MkgB/0tat+wV6+rf1Bt2DbWwcLRwU0VIKuNsl9vBEsEMUVVpFDFFCiqkUcOMigQKAAAtgAPfLrkewax5diaWpuLhmncniTIagmuSdGmpPE9D3mC5Fzuj6f7OMBB9i8f+NV6E32LuiXqsj+Z1HPRbM+Oe5lOmgwfyY2XS5SRRc01Ln9v7rx33cl1KLSo50SG+q8q6QT9Ma/vNxSDlbab1f7KLe4g3yDwzrX7K4PzI6zH+5q8VxzHzXtBzc3PJ920Y/iaGe2k0jz1Edw8u016Li1LTOCrU8DA/UNDGQf9cFSD7xl1uOBP7epkE0qmoZgftPXzif5n8vyS+AH8zjtDtLo/sbtH4/7m3eIMzsjsDqvdm5uvs1V7eFDjNu7hxEWf23X42pr8PksptwT1dBI7089LUQCeJ1YasvPu+b/bbr7fptIlD7ltd1PbzLqqw1SvNESOIDRSLQniVb0IGN33sttuB7sNzWWMlhv222N3E9dXctrFbXEbH+OOeCTUuCqsmKFSabN8b63v2bu7cG/+yd47q7B33uzJT5ndW9t8bhy+7N3bly9TY1OV3BuTPVlfmczkqgqNc9TNJK9uWPucusZekr7917rbr/l27s7f6L+I3UezaXcFbgmnoM3ux8PU4zDVAok3puLK7lo42gyONqZoZmxmSgaRJCXjlZgdP6VCs3uXzdst3JZbFfGLb42IVfDida/iPfG1atU1/Z1w9+8z7lXW+e+G+3PL9yp2mCeO2QqEZWa2hjhlYFgwYGZJNLDtK6SK8TcX8G+5O6+wfl78b9ovuOPJU9f3JsPIZKE7ewKSHB7dztLuTcCxvQ42jkiH8ExNQTJe8YGofTlZY+63P25zja7y8WW1uDocGCAHQwIcApGpFVrniOI6IvYje+Z9995eWNsWbWG3u0kYeGlfDglWeX4VFP042z5cfLreC9n3Xd/r3v3Xuve/de697917r3v3Xuve/de697917rW2/wCFInQ47G+I+T3rR0X3GW66rcH2PjXjQ+W+261ttbsDTKV006bL3hLVOjalc0Cm2pVKrfabmM8l/eJ2dpW0bZzHZTbfIfLxkpLCf9M0iQxL59zZpUdH0sA3Pky6iGZ7KZZl9dDdrfkBrY/l18/730+6i/odvi71c3d3yV+P3TgpZayPtLunrDYFVBEhcmh3ZvTC4TISyWsI6enoa2SSV2ISOJGdiFBIJuYdx/dGwXu6VANvaSyA/NI2YfmSAB6nHTkSa5VT1YD+fW9N/P1+cPyB+H1f8D8R8Yc5m6XsjePc26d1ybJ2/DlMlF2Vhtg43bOAput9ybYwpFXubbe7sl2MkQx4HkmqYEkptNTTxyRYgezHKOy80JvMvMCIbCK0RPEag8JpC7GVHbCMgirq4AEhu1iCe7hPJCYxF8Rbh608v59G26r+WPYOR/l2fLX55b/6vz3x93tlMP8AIbsqg6m3UJX3b1/mei9hL0vi9tZpM1hts5H+PVu9Om5Zvt6+ho5I560RFAgViGdx5bso+eds5NsrhL20VraIzJ8Ei3Enjl10s40iOcCqswIWta9OpK30z3DDS2TTzFBT/COg3/kd/OLsH55/GyHfHbG1Gpt2fHrF4ToXI9p1u4JsvkO09ypiMZn957qyME2OpP4ZXZDBUO2Kiv1T1LT5KSplBjjZE9r/AHd5RsuTd+Nptslba+ZrgQhdIhTUVjQZNQGMoXAooUZNT1Wxna4i1OMrivr6/wCTpFfEr5e9nbq/lCfLP5u95bzq9+puir+ZPY3WcG4qKhhx2O2HhancOyevuuMXRLj8VE+B/vPtuWkp4pkL66to3bjSqvmXljb7b3O23lLaIhCYxYxS6SamRgskkpNT3aGBJH8NR1WGZjZvPIa11EfZwA6Pt3H8Xtq/IxfgZuXf1TiU2X8cuy9nd31OMy0yQxbh3ljuuMhtPq7EQ+T9p5oew90Y+vWNzpqXoVp7M06qQZtfMNzsX75t7IN9Xf2724K/hQyh5m/5xIy18tWry6UvEJPDLfCpr/Kg/megz+RfVlTsz557I+eu4oVo+pfjR8GPkRHvHLU9KTX1+ZTcWBzmI24ky08slWsu2pc3VQxoWkjqIFVY2NQfZhse4rdcmzcmwGu5bhvFtoBOAullZuOO/wAME8CDx7eqSJpnFwfgWM1/1ft6Cv5o9pdv/GrYn8rP4udRbxm2d2T3L8nfjb0/vjL4impstXVHU3XmLx9b3TJSrk/4jPUQ1c8VCa2ZpGeejlmjlmtO7kx5U27a9/vOYuYdziEtha7fdTxqxKgTSkiCtKUp3aRTDAEDFOqTs8SxRIaMzKD9g49Uy/8ACsXtRZc38OekqSdQ9Bi+1O1M/TXBZ1y9XtTaW0JwosY1iOEzikm+vWLW0m8qfdt26kW6buw+JoYVP+lDu/8Ax6PpFuz5RPtP+r+fWnd7yi6J+tkz/hNT8a37N+THbPe2QoHlxXS2w8btHb9Y0QCUu/e48hVYmCtppn4kkodibezkMyICUWvjZyoZQ+K33sd1k/qRacp27Um3S9TXT/fUJBNR6eI0bAn+A/Ogn5U0xbg144qIY2I+2n+ao/Prf6hhjp4YoIUEcUEaQxIvCpHGoREA/oqqB7xajjSKNYoxSNQAB6ACgHS1mZ2Ltlian7T1k936r0Wr5e9LVPf3x57G63xTRxbnrcZT5vZlTI/hEG8NsV1Nn9vr9xZvto6+tx4pJZLHRDUObe4891eU5OdOQtx2G2FdxaHxIOAPjwkSxKCcDWy+GScAOepf9h/cKH2v91dp5uvQTs8czQ3agVra3CNBOdP4iiP4qrUVdFHRG+j6TBbu2Rgt118f3mfaH7HcOOqqdqZMFufHWps3i5sZNeWGakrkbSJxqMbI2lbj3zZsuY5t4sVmRWgkBKSIQQ6SIdMiODQqVYHFAfXrKD3Em3HY+YbnZbY6Ns1a4JFbUZreTuhkEgwQyEVKY1BlqadEd/mffyfuoP5o2x6HB5+tfY/Y+2tVXtbsPGUUFRkcLKsUVOziGSSnirYamnijp6ikmcQVkKRqWglhp6qmUbNzpzf7Y7rLzjyKsNxubw6JrKdzHb36rUojyBWMMqEnwpwraa6XDRsykBbnufL+78ovyjzrHI9gsjSWVxFTx7K4f4ioOJbeWgM8JIrTWhWQKetJPsH/AITB/wAyDb29q3bHXU3SfbeIhyUlNBuDE9hxbSmp6COrNLJX5zEbwoMYKOSl4aeGgqsqLG0Mk/sb2H96j92q3sZG53Tfdj3q3Zo5bd7UXa+OlQ0cM9nJMkqllKpI4hVqVOhSD1D28+yfNu1wre272tztshXQ6uyMVcVUtHIqsppkr3EcMnqwf4d/8Jm9y9D7n2Z3T8591bC3vDisotdiOjevpcpuDa8mapFFXij2VuvLY7BxZWjgljaVsRQ0s1HVvCFnq5afyU00Vb3/AHm3KPvI25cjeyNruVlMluC1/erHDK8TEpKbOCOSUoVJRfGldJFEgKRK9HSNudeVt75T26C7uTGUndkJQk+GwFVBJAFXGqhFaaT8urlO6fiRtHsIVWd2aKPZu72DyuIIPHtzNTWvbI0NMhOPqZXAvU0yXJLNJFKxBBb7YfeI5i5N8PaeZPE3LlsUAq1bmBf+FOx/UUDhFIaCgCSRqKHnl70fdQ5U9wvG37lHwto5warNRaWlyxz+tGgrE7HJmiWpJZpIpWNQb3+St8LN7bf7n3v8g+zduzYWh61x+R2NsAVSq6Zjd+5qKKPPbgw1ZE0kFXjMLtGqko2kQlJJcsUDa6eZF6X+1O77Lznt683bFMtxtRBRGAIIk/GrKwDKyA0KkD4gRUUJBH3PPu982cpe4e485c92T2km0xtbWoahEs86gSTwupKvEluzR61JDNOVqGikUbN/uYuulnXvfuvde9+691737r3Xvfuvde9+691737r3RP8A5zdW0nbXx33ptesx0+UpaygrMVk6Gkgapq6zb+6sfWbSz9JFDGrSSo9BmzK6gXtDf8e4u92F3Oy2K15r2JGbfdj3K1vYdIJbVFKvAAEkAlXNM9lfLoXcmTQfvR9vu2As7y3kiapoMrXNfsKj/TdaEsn8hf5pCSQQ53p94g7CJ5KvtuKR4wx0NJEvUEyxuy2JUO4B4ufr7yqT+8G9rCgMnLnOwegqBYWhAPmATuAqPQ0FfQcOi0+1m8VxfbXT/ms//WrofPiv/Ka+efxT+Q3U/wAidsUnx43hn+pd1U+68Ttzd1Z3a+38lWU9NVUsa1pxPVOPyEUtL90ZqeSOUeKpjjdldVMbE/MX36vaPmPZLnY7jYeeoobmMoWTb7PUASDiu4EZpQgjIJGOPV4vbDeYpBIL3aiQf9/P/wBaur+M/wB9/wAxfP792/23WfDP4D1/bey9vZvbOxews9uL5F5vObPxW4pIJc1SYSpk6cpazHU+WemRar7WemlniBjaTQSDDEP3iPZyGyfbFtfcYbZK6vJEu3WKq5X4Sw/eJBIrioIBzSvRgfb/AHssHN1tOsDB8aT/AK1dAD8iM1/NN+RPxF7P+KOa6t+KW16ftqLJ/wB6ewcBu75EvlEqtydkL2Ru2TGYSv6kqKSmpM3XT1VItO88iU1LUaQX0AE62P7znsjsfM1vzJFtHP0htiNEbbbY0osXhJVhuAJKijVoKkeVem5PbzfJITEbzahXz8aT1r/vroH/AOXvsn+at/Ls6T3H0P1tsD4Xb82fuPf+Y7Fqa3fcXyKXPfxrP4Hbu3MpTVVVtzY2Goclj3x21qNYhNTGeMBkMjx+NIzPnb71nsfzxu0e83+ze4UN1HCsQEdjY6dKszAgNfMQau1aGh40BqTS39ud8tkMa3m0kE1zNJ/1q6XnyyoP5mfyW+I+T+G20ukfhR8c+rdw/wACo8/S9RN3hjKNcBhtwU27pdubW2+nTtHhdp4zMboooqir8Mczyw+WEkiaV2R8tfek9ltg5mXmm52v3DvtxTUVM1hZk6mUpqdv3iWcqhIFSKGh8gOrTe3W9yw+CLvaVQ+kz/8AWrpX9y9ifzlu3uu+uOt4tr/D7YGK6+7I6Y7DlyOza75MU+X3SOkt1YXe23dr5mpr9l1iQYPKbm21j6it+2WGdxTBFdVdgUu1feT9gtsvp787J7gTSTwTxUfb7Aqnjo0bOoF8O4I7Ba1GakVHW39vd+dQv1m0ihB/tpPLP++usvyU7B/nB/J3ZW3Otd2bH+Hm0th03ZXXm+d94jYmR+SOPr+yNvbA3Tjt2/6OMzls51/uD+GbY3Hk8TT/AHzUsK1EscQjLNC8sUmth+8p7Dcv3cl/bbN7gy3ht5Y42k2+wIiaRCniqq3y1dQTpqaAmvEAj0vt7v0qhTebUFqCaTSZp5f2XTF31W/zR+9Pk58Tvk1X9S/EbE5X4jy9rV+zNiVG5fkXmdpbizna22KTbFdmtwNH1XhsstXt+HHU1TQiCdFWpgBbUjOjPbN9572S2fl/cuX02n3AaPc/BDyDbrFXVYXLhV/3YMtGqQ1RwPrQ9ek9u98klSU3m1VStB40nn/za6r3/mCfAz+ZP/MV70x/fHbFP8b9p5zEdf4DrfFbf2TW97rgKDAbfy24s5D4Rn+scxkPuKrLbprJpbzaNUnpUc3G/JX30/ZnkbZ22bbdh57lhadpS0m32WoswVfw36igVFAx5dJrj2z3u5k8R73agaUxM/8A1q6I3/wwx81v+d305/539uf/AGnPYu/5OC+1P/TO87/9y+0/72PTH+tZvH/Kbtf/ADmf/rV1t0fyLvhlXfDT4mZvYm9Z8RX9q7h7e3NvrftZhqbNrjo1nwuG25s/GYyt3Dg9u5TIYykwOI+5BeljSKvqqpF1Aa2jvmf3j2T3y3deY9ittxs9tskWFYb2JYZw1GYyFEklSjlyFZZGqEANGBAR3WwXXLX+KXMkMryLXVE2pCKjFSFNRTIIHHzB6u19lHSDr3v3Xuve/de6ru736R3x1rvLO949HYJt0YnczpXdtdS0epK3J10JJl3xsmJA6tuCWEn7ykRC9W93VZZHPjwd9+fY3erDeLn3M9t7f6lLn9TcdvT45HGWu7VR8UrDM0QGp2q6h3YgZZ+1/uVy5zZy/be23uRciyvbMaNs3N6FI0PCzuyaUgBp4UpNIx2koq93Y7U2zRdd5PKYmtam3RU0axHCV6ikzmOydWBAkM9IXYMcOZi8jxPJFrQgOSQPfM3n77xfJf8AUPdNy5Zvl/rLEjW8VtIGhuo53bwtZicKxEJJlZlqo0BCwc6elw5E3y65vgstwi1bGkmrxkOuGSNe4kOOHi0AUMFahBK0BPRfutFLZqulJJIxsiknm5kq6Vrlv6/t/wCx98hedXJ2+MNlmnBr5/C3+fqXedSF22KMcPHH8kYf5ehE3ntuDdu2srg5ggerp2ajlfgU9fD+7RT6gCyqlQo125KFh+fZT7d843PIfOVjzNblvDgmAlUfjgftmSlQCShJWuA4VuIHUI81bDDzLsFztEtNciHQT+GRco32BgK04qSPPouHR/xm3x29n1jqaGt27s7G18lLuHctXTmNVeklZKvGYVJgEyOW1oYzp1RU5OqU/pR+/HsV7G8w+9lxbbtYh4OQJAkj3+nseNgGC2uoUmkdSKEVSOoaTyVsI4tpuzcvb3CtG0blXqMhlNGX7QRQ+nV3W0tqYLY+28PtTbVDHjsJg6OOioaVOSES7STzyH1VFXVTM0s0rXeWV2ZiST77HcsctbNyfsFryzy/CsG0WcQjjQegyWY8Wd2Jd3OXdmY5J6FEUaQxiOMUQDpRez7pzr3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de697917r3v3Xuve/de6L/2Ptb4/bmyNTFvbKbLxW44yoqqobnxW38+jmMeM1yitp5Kl/ERp+5jkstrcW94K/eB9tPuJe42/3Nr7xblyftfuDGR40w3iy2vc1YoNJuQLiJpW0FdP1cUtF06RQL1LfJm++7myWaScswbncbM1dK/TS3EBFc6OxgorWvhsua1zXoM9udO9E46srZcD2vTVgmijWSnO7dn1zQIH1Bi9LBC2ktwNQ+n5J594F80fca/u79ydS3vNaWdmGJVX5i5aY1IyNZjQEenbUDiTx6G+8+43ureW0Ue67A8ZViQ30l2gY09GY+Xof2dCph9n9MYmRZZM/g8vKpBVsvuXFTxqbk80tPLS0kqn6WkRx7k32u+6/wD3UvtreJuV7zpyhzPuMZBRt65p2e4iU1JzaW01pZyqa003FvMtAMaqkgLceYvcncEMaWl1bxnj4NtKp/3pgzj/AGrDocKT7T7aH7H7f7TQPt/tPH9t4/x4fD+1o/pp499fNi/cf7ltP6s/Sf1d+mj+l+l8P6bwNA8LwPC/S8HRp8Pw+zRTTinUXS+L4rePq8fUdWqurVXu1VzWta1zXj1I9mvTfXvfuvde9+691737r3Xvfuvde9+691//2Q==",
        /* iso logo */
        isologo: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgAJQBIAwERAAIRAQMRAf/EAaIAAAAGAgMBAAAAAAAAAAAAAAcIBgUECQMKAgEACwEAAAYDAQEBAAAAAAAAAAAABgUEAwcCCAEJAAoLEAACAQMEAQMDAgMDAwIGCXUBAgMEEQUSBiEHEyIACDEUQTIjFQlRQhZhJDMXUnGBGGKRJUOhsfAmNHIKGcHRNSfhUzaC8ZKiRFRzRUY3R2MoVVZXGrLC0uLyZIN0k4Rlo7PD0+MpOGbzdSo5OkhJSlhZWmdoaWp2d3h5eoWGh4iJipSVlpeYmZqkpaanqKmqtLW2t7i5usTFxsfIycrU1dbX2Nna5OXm5+jp6vT19vf4+foRAAIBAwIEBAMFBAQEBgYFbQECAxEEIRIFMQYAIhNBUQcyYRRxCEKBI5EVUqFiFjMJsSTB0UNy8BfhgjQlklMYY0TxorImNRlUNkVkJwpzg5NGdMLS4vJVZXVWN4SFo7PD0+PzKRqUpLTE1OT0laW1xdXl9ShHV2Y4doaWprbG1ub2Z3eHl6e3x9fn90hYaHiImKi4yNjo+DlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+v/aAAwDAQACEQMRAD8AvX/l9/y+fib3X8UOlt6b66W65rdy1nWXVzZDJr1z1tWVWZqsh1rtTMV+Vy1dl9l5bIV2SrshWSSSSyy/v/Qe8M/bL222rniwvdw3e43KKSDcDCBazxj8Eb0pJHJikn5Z6m3nHmzcNgvEtbJLUyFR/bCTP6oyf1c1r+fyHWHuHov+Wb1ZmchtTAfHTbva+78XU0VFlMBsXrfpSSPF1U9fHiaikmyFX19IMxlMTU1kUlXSY+GukooT5JGPuUI/u9cnTAH95b/X/mvH/wBsuOg0fcjef992n+8yf9begsg6t+F2Br6ZOyv5e2T2fhalYpBmIOs+qp2CSn1R0dPmensLT5Kpp1/3UawAn/Nke9zfd25Pj/5afMA/5vx/9svXpPcbeuHh2tfsl/629Ho6v/l6fy4e49uw7p2D1Z1pmsQ8klNVmHrHqOOuxeQTwGpw+dxs3XkGQw2dg858sUkTIbcH3r/gfeVPK85hP/URbf8AbL1X/XI5hIqLW1/ZL/1t6i9j/wAtL4fbTq9l4banxr6x3Jnd7Zytw9HHltn9SbdxlFFjsFk89PWVtY/VGSnBenxvihgiivwf6ge9D7v/ACqc/WcwU/5rx/8AbJ14e5PMB/4i23+8y/8AW3oCe1fhV8VulMDV7n7L+Nnxj2tiaelkqlFfkOoocjXwwTw08pxuMfpGKpyIpzNF/mRb8+6f8D3ypWn1u/8A/OZP+2Xrx9yd/HG2tv8AeZf+tvS221/Lr+Pe78euY2v8V/jFuPDS1EtPDldv5/p7L0MlRSAR1dPFWUnRk9OKqmnIM0J5IPtYPu5cnH/lp8wY/wCHp/2ydb/1yt8/33a/7zL/ANbesPwe6w6s66+dOz6rrLrHZPWcO5viT8k6vKUO0tubYwdXUw0vcPw8lxNJnqvbOHwkOZmxCZKdIppKeFdcsxiRVZlUBeylkdq9z992dJJprayjuYEeQkkrDeJGK1PxaVFe0eWej/nu4mu+ULG6uFC3Ms6O9KDuaAt5f6b/AA06vi95Y9Qz1rVY75G5v4t/ya+tt9bOeGLsDdexOh+stjVE8viWkzm/OqtjUi5RQJIak1VDjKWrSExTcTMfeP3sCTFs24fPcD/JEr/k/LqSfckNJucQNfozaD9viL/q+3oWs9upfgj1vsHZ2ydt7cr/AJIdm7Sxm495buq6N8vFs2nyQhFRgNvCsDVz0VPlJ5aShjEv+UXlqKnySBbFnv8Ae7lz7e21jsPLSeLzTf2/hLT9StfL7KCvUh+wPstH7p3W6cxc03Ih5O224wPKQA0qfUlsKOFRnpz+NvyV+SfZe7M9B2JnNt7n6V2/S5Cu7nym/Nt0eKwe0dv09NMtXT+WGnp5Fzmo+nGv59TWEgP09w77Pe6/vLvvMt5Nv8tp/U3b4P8AGFlj0BM+Y9Pn+zqT/e32a9mOXOWLU8szX1xzhd3ERtooZdWsgcPD/wBCPGooccfTplrdxzfEb56fF+PrOfEVHxY+ZeJrtqQ4/GweKaDdFbLBX7dcVlRIWmxOCr5oXxJ8f3FDT5KopfN49Bjzm22eK5tIryykM22zQReH9o/y149YUzRT2wli3KHwd0gnm8WH5eXpUEUPp1cJ2R/zMP44a1s399dzcf0I6z3V/wAU9mHTXVff8xH45YL5TdpdedPYH5cY3459hbp6k7DrdzbOn2vgd3ZPtPprB5/bdXX1mCo9wS4+joIdmburqabI1EFQJ/tqqNnHiVve+ro4Iye3ot38mnu7snuXP/KOHGbph7i+KnW3YH+j3YXyirNrY/Z9f8lu+8Nk8jS9t7w2bt7HBqDH9RbF29DhtsY2sp7RZWogqKxpFaYiBDvFnqsDTy6qtzpvQf8ARP8AV8uh4+LUHi+bnVDf6r4f/J630+h7e+Ff+H+HvG/2rTT7wcy/81Lz/tOHUqc1MTyTt/8AzY/45c9XN+8neoq6oP6B2HVbv/lnfHFqXbu3NzvgNhdF7pkxu5cJhM/j4qfHdR7KU5eGLcME1BjpcOajyNV3/Zvz7xL5Nvd627ka/vNilpPDuJk/4xHT/N/xfU4bnbbHf82xWG9U8Sa0lMI/4Z4q1qa/nX5HqR89Oudw9n0HXXyQ67opt27YfaFFhd0HbOrNNgqzH1X31Dm48fEpqK3F0+QqZaSXxcQnj8i8c/eK5c3Ld4Nu91uWnG4XaQxGWOP9TNKEE+RPHP8Ag6nj7tnOfL3Kc28+z3Pg+lWSakM0mceDq1AUNaEsCB/Loue6+2u6vlk23entgbBp8DSZGojrd2YTaWJWiwW495TQCnye896ZDww00NBCIIpfHNLZRcjyDx+4v5h5h9w/euCy5b5e2aWxsZPCFxJDD4Z1+Qennk9SNyvyN7Z+xFpuvuFv97BuXMB8b6bxp4ZhXNSiEU0jjQnJ41x0PHzX60yWzu3P5RW1cNj8vuOi2B3XQYrLZTE4zK19HTxUcW0UqK/JSUdHNFiKeoqYpvDNUiCEjm3vpLyttkuzbRZbTJIJr21soYyBxrGTQH1NKD8uud/M+/zcy79f71NT6m7mEn7FCgn7adXA9o3/ANI3x7MYFzvXdvjS39est1+n6f8AG/YkkrRq8c9EI61x/wCcD1l8WP5ku4fjbRbG+WvZnxk+S+wu+t6fFjZG+tuYvctFj85T9lZDcWxe9uns5Ljnxb4bIbr2/wBdZY4uuqXp6F46Vln101bFoWWMYsVCVoan+fSKaQwAk8B/q/1Dq2n4I98/CDC7H6Z+KfxEraij692d1buaLq7BLt7I4ehG1Oot81PV2/pK2tyVPi6mu3fT79hnr8qJIRWSpWx5F9MVZG8ia8sLtCLiVhQDgP8AV/L8+rG8RRUg06QHxpiMfzV6WY/7u+G3yhkHH4/0vfCWP/oj3jX7Yx6PdjmH/TXf/aaOpc5nNeS7D7Lb/qzP1cT7yS6i7qtH+W1hqfI/BP460lbTU9VSVfQ3TkVVR1EfmirIqvp7YPmgqIZjZaSqFiCPyB7gX2etof6v3pEH1Bndx9h0R8Ps6HnuI8v76huLX/F763iTj5/qTf4Qf59KmLqHsrpdsnUdA1mNyuHzeR/iVRtDdlcxx9FEFmmraLEy1VhCMj98f34JvVT08XMvtQ/KHNHJO2y/1D1TeNNpmjkGfX9MefDy+fDpbfc58r82blEvPs88F1Bb18WMmkuciT0znPDHHoPu6IvkhuroDcOJ2vsHNbA7gq924V8dSdQZXF4VcztvF5Kkq62HJ7hmrsfVUuOy8Rkjqlpas1kMAEtN5ATHKNeUN23OC3mG72Rin8fjFgg+nDHy4D/IFN4g2uO/+m2y7uL3bK8JiCPl/hPr0VPZu3fnvj997DyO8cT3sdq0HadDkN9w/wClHG5ba+exmQyYizMn8IpK+jrI8Rt2ggpvtsfTRfZZO1T9xR09R/lHs43beLY7ZOPp5qEfz+fSa2gjeevi+B/qpjq0PuSbcFNVdU7o29tXP7rXb+Vzk1fjcDSx5LKUKZ3YG4dv0NVX0b11E1TT0uSrYoZ/FN57i1729iUGACG54D/N/q9B/mDCrf8A01GuAT6/n/h/1ceqSu88D8oYN3/307Xy/XnS1Zvx8e7be2bhN0ba2rnN2bbdaifetFT7ixufnqd2TwVNLTzzSSmf7NtHk/d9mlnPat/bRU6aNjI2BXHr0OXxCoPkptytwO+IcDszsfqzc+b/AIzVb8wO1N/bt3/lds+BKLLU+wtx5PM4ugwH8ZzuOjFZFoEF6cAkc3ruF3tYhJi8enVhYSsK/Kny6MZ0NjK7E/NHoulymOq8Tkk+G/ykgr8bkEiir8dW03a/wbSroK2GF3iSqpal3STxs6awbMfoMa+QIPC9094P9C7/AO0uHqX+YnL8n2df4rf/AKsy9Wze8guo16oL+GW5PmdB8SvjTT9fdM9NZTa0XQfT8WMyzfJnt7AVtfj4+ttuJQ12f29gviXuShxObrKDwyVtLSZOtgpp/wBt6if/ADvvDjYTzd9E39XxeiDQlfBN0fwRcDGBj7R9nl1O27Ly/wDUU3ZoPq/EehnWGtPFxjXWnHz41rnoy0m6v5gH9vojpz/Nc+X5Zd4E+Dj6/wDOFg9Nv9h7NY29wqGibj/vc3/WvpJTk7zbbv8AeYv+guos26vntrH3XQ/TV7/7u+Wfd/8AT8W+Fmr/AG3t6JvcCh+mS5r8nm/619WA5XpgxU+yL/oLrM+6vnxen19EdKf5w/aaPln3gF83F/tbfCwnVf8Ap/h7e1+6P++t+/OW6/609MyDkah+rO30+yL/ACN/h8usMG6vnx/yj9EdMX8sF/H8s+8f8/b/ACa//OFn1t/sfdJW9xsfUJc/73N/1r6ZiHI1R9M231/0sX/QXSL3zur5Z/7iP9J/RHxe/VU/wT/SB8su0L6/8m+9/hf94fhbfSP2fN4fz9PdYm9yqf4sm80+Tz/9a+nwOUKZbb6f6WH/AKC6f9obq+a3936P+4PRHx6/ur5a/wDh39zvln27/A/N95P/ABP7L+CfCz+HW/iPl8/i/wB3297mb3Gr/jSXVfm8/wD1r63TlCnxbdT/AEsH/QX+rHl1z6Aync2S+eXW3+mHYvW+y6yH4m/Kv+Gf3C7W3d2fWZKaTuX4W/xg7lO7OmeopsJV0k8cejR/EJKiWWfz+CWJvMfe1Zuv673f14f94fRz+JrLatf1EGrUHGrVq1aiT6YI4E/NwgHL0Rt2YxePDSqoMeFLXSVYnR/CKU45xm3r3kZ1F3X/2Q==",
        // debug mode
        debug: '',
        // plotted points
        points: [],
        // recent colors
        recentColor: [],
        // color data
        colorData: {
            name: '',
            code: ''
        },
        colorIndex: '',
        // image layer
        imageLayer: {},
        // current
        current: null,
        /* stage details */
        stageDetails: [],
		// request params
		request: {
			targetImage: '',
			oldColor: '',
			newColor: []
		},
        shareURL: '',
        // request number
        reqnum: 1,
        index: '',
        polygons: {},
		/* kinetic stage */
		stage: {},
		/* the canvas */
		canvas: {},
        /* original canvas cache */
        canvasCache: {},
        /* adjusted canvas cache */
        canvasWhite: {},
		/* jScrollPane panel - holds the color palette */
		panel: {},
        // error message
        error: null,
        // room image
        room: new Image(),
		// main layer
		layer: new Kinetic.Layer({listening: true}),
		// tooltip layer
		tooltipLayer: new Kinetic.Layer(),
        // shapes layer
        shapesLayer: new Kinetic.Layer(),
        // tooltip
        tooltip: {},
        // polygon group id
        gid: [],
        // adjusted flag
        adjusted: '',
        // image cache
        imgCache: '',
        // username - active session test
        username: '',
		/** utility functions **/
        showDialog: function(content){
            $('.modal-title').html('Attention!');
            $('.modal-body').html(content);
            $('#error-modal').modal('show');
        },
        /* add the color fix function */
        colorFix: function(newColor) {
            /* get the sum of the r,g,b values */
            var sum = (newColor[0] * 1) + (newColor[1] * 1) + (newColor[2] * 1);
            /* fixed color variable */
            var newRgb = [];
            /* get the hsv representation of the original pixel RGB values */
            var oldHsv = rgbToHsv(sR, sG, sB);
            /* get the hsv representation of the new RGB values */
            var newHsv = rgbToHsv(newColor[0], newColor[1], newColor[2]);
            /* fix the color */
            if ( (sum < 300) && (sum > 210) ) {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.58) );
            } else if (sum == 359)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.78 );
            } else if (sum == 206)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.32 );
            } else if (sum == 210)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.42 );
            } else if (sum == 228)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.38 );
            } else if (sum == 303)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.38 );
            } else if (sum == 541)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.85 );
            } else if (sum == 620)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.86 );
            } else if (sum == 534)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) * 0.72 );
            } else if (sum == 179)  {
                newRgb = hsvToRgb((newHsv[0] * 2) , (newHsv[1] ) , (newHsv[2]) );
            } else if (sum < 210 )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.69) );
            } else if (sum < 180)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.78) );
            } else if ((sum >= 380) && (sum <= 409) )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.72) );
            } else if ((sum >= 410) && (sum <= 429) )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.72) );
            } else if (sum == 308)  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.56) );
            } else if ((sum >= 430) && (sum <= 439))  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.70) );
            } else if ((sum >= 535) && (sum <= 565))  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.78) );
            }  else if ((sum >= 310) && (sum <= 339))  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.72) );
            } else if ((sum >= 340) && (sum <= 370))  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.40) );
            } else if ( (sum >= 420) && (sum <= 450) )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.79) );
            } else if ( (sum >= 450) && (sum <= 479) )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.82) );
            } else if ( (sum >= 480) && (sum <= 509) )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.86) );
            } else if ( (sum >= 510) && (sum <= 540) )  {
                newRgb = hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2] * 0.88) );
            } else {
                newRgb =  hsvToRgb((newHsv[0]) , (newHsv[1]) , (oldHsv[2]) );
            }

            /* return fixed color */
            return newRgb;

        },
		getOldColor: function(evt){
            /** Redundant **/
            var shape = evt.targetNode;
            if(shape){
                app.canvas = shape.getCanvas();
                var ctx = app.canvas.getContext('2d');
                var mousePos = app.stage.getMousePosition();
                var x = mousePos.x;
                var y = mousePos.y;
                // the image data
                var p = ctx.getImageData(x,y,1,1);

                app.request.oldColor = app.rgbToHex(p[0], p[1], p[2]);

            }
		},
        plotThePoints: function(){
            if(app.debug == 'plotter') {
                app.stage.on('click', function(evt) {
                    var shape = evt.targetNode;

                    if (shape) {
                        var mousePos = app.stage.getMousePosition();
                        var x = mousePos.x;
                        var y = mousePos.y;

                        app.points.push(x,y);
                        //console.log( app.points.toString() );
                    }

                });
            }

            return app;
        },
		getMousePos: function(canvas, evt){
            /** redundant **/
			var rect = canvas.getBoundingClientRect();
			return {
				x: evt.clientX - rect.left,
				y: evt.clientY - rect.top
			};
		},
		updateToolTip: function(tooltip, x, y, text){
			tooltip.getText().setText(text);
			tooltip.setPosition(x, y);
			tooltip.show();
		},
		rgbToHex: function(r, g, b){
            if (r > 255 || g > 255 || b > 255)
                throw "Invalid Color Component";

            return ("000000" + ((r << 16) | (g << 8) | b).toString(16)).slice(-6);
		},
        sortColors: function(colors) {
            for( var c = 0; c < colors.length; c++) {

                var rgb = hexToRgb(colors[c]);

                var r = rgb[0].toString();
                var g = rgb[1].toString();
                var b = rgb[2].toString();

                var hsv = rgbToHsv(r, g, b);

                colors[c].hue = hsv[0];
                colors[c].sat = hsv[1];
                colors[c].val = hsv[2];
            }
            return colors.sort(function(a,b){return a.hue - b.hue;});
        },
        updateRecentColors: function(color) {

            if(app.recentColor.length < 5) {

               app.recentColor.push(color);

            } else if (app.recentColor.length >= 5) {

                app.recentColor.splice(0,1);
                app.recentColor.push(color);

            }

            for(var j = 0; j < app.recentColor.length; j++) {
                var colr = app.recentColor[j];

                $('div [class="mini-box-' + j + '"]').css('backgroundColor', 'rgb('+ colr[0] +')' );
                $('div [class="mini-box-' + j + '"]').attr('data-name', colr[1] );
                $('div [class="mini-box-' + j + '"]').attr('data-code', colr[2] );
            }

        },
        adjustImage: function() {
        	
            if( app.adjusted != 'ok' )
            {
                var polys = app.polygons;
                var canvas = app.canvas;
                for(poly in polys)
                {
                    app.resetColor(polys[poly], canvas);

                }

                var adjusted = canvas.toDataURL('image/png');
                app.imgCache = adjusted;
                app.adjusted = 'ok';

                return true;
            }
            else
            {
                /* do nothing */
                return false;
            }

        },
		changeThePalette: function() {
            var config = app.settings.app;
            var points;
            var colors = [];
            var dataString = '';
            config.indicator.show();

            $data = $.ajax({
                url: 'colors.php',
                context: document.body,
                cache: true,
                success: function(res){
                    var dat = (!res) ? 'Failed To Load' : res;

                    var dataString = dat;

                    app.panel.getContentPane().html(dataString);
                    $('a.box').miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
                    config.boxi.miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });

                    app.panel.reinitialise();
                },
                error: function(res){
                    app.panel.getContentPane().html(
                        'Some items have failed to load,' +
                            'please try refreshing the page [F5]');
                }
            });

            app.panel.reinitialise();

		},
		resetColor: function(polyGroup, canvas){
            /**
             * Resets the color of the image on the stage,
             *
             * @param   polyGroup Object    the defined sections
             * @param   canvas    Element   the canvas containing the image
             *
             */

            // Define and init the variables
            var sx = sy = sw = sh = xc = yc = 0;
            var k= 0;
            sw = canvas.width;
            sh = canvas.height;

            // Get the 2d canvas context
            var ctx = canvas.getContext('2d');
            // Set the canvas composite mode
            //ctx.globalCompositeOperation = "lighter";
            // Get the image pixels from the canvas
            var imagedata = ctx.getImageData(sx, sy, sw, sh);
            /* cache the image data */
            var dataCache = imagedata;
            // Get the pixel data
            var pixels = imagedata.data;

            // Display any errors
            if(app.error){
                app.showDialog(app.error);
            }


            // White color for adjustment
            var white = rgbToHsv(255, 255, 255);

            // enter the loop with no errors
            if(!app.error) {
                // loop through the pixels
                for (k = 0; k < pixels.length; k += 4) {

                    // store the original pixel RGBA values
                    sR = pixels[k];
                    sG = pixels[k + 1];
                    sB = pixels[k + 2];
                    sA = pixels[k + 3];

                    // get the hsv representation of the original pixel RGB values
                    var oldHsv = rgbToHsv(sR, sG, sB);
                    // get the hsv representation of the new RGB values
                    newRgb = hsvToRgb(white[0], 0, Math.min(oldHsv[2], 100) );

                    max = Math.max(sR, sG, sB);
                    min = Math.min(sR, sG, sB);
                    if( (max == sB) && (sR <= 77)) {
                        newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.25) );
                    }

                    if( (app.current == 'bedroom-01') || (app.current == 'bedroom-05') ) {

                        if ( (max == sR) && (sG < 70) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.05) );
                        }
                        if ( (max == sG) && (sR < 100) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.45) );
                        }

                    }

                    if( (app.current == 'living-room-03') ) {                   	

                        if ( (max == sR) && (sG < 70) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.35) );
                        }
                        if ( (max == sG) && (sR > 100) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.35) );
                        }
                        if ( (max == sR) && ( (sR - sG) < 6 ) ) {
                            newRgb = hsvToRgb(white[0], 0, (oldHsv[2] * 1.35) );
                        }

                    }

                    // Calculate the x,y coordinates of the colors
                    xc = ( Math.floor(k / 4) % sw );
                    yc = ( Math.floor(Math.floor(k/sw) / 4) );

                    // Loop through the wall sections

                    // Store the various wall sections
                    var poly = (polyGroup.poly)? polyGroup.poly : [{x:0,y:0}] ;
                    var outer = polyGroup.outer;
                    var inner = (polyGroup.inner) ? polyGroup.inner : [{x:0,y:0}] ;

                    //
                    if(poly) {
                        // Loop through the polys
                        for(var i =0; i < poly.length; i++) {
                            // Check if the pixel is in the poly
                            var cord = isPointInPoly(poly[i], { x:xc, y:yc});
                            // If its in the poly
                            if(cord){
                                // Transform the pixel
                                pixels[k] = newRgb[0];     /* New red color */
                                pixels[k + 1] = newRgb[1]; /* New green color */
                                pixels[k + 2] = newRgb[2]; /* New blue color */
                                pixels[k + 3] = sA; /* Maintain the Alpha channel */

                            }

                        }

                    }

                    //
                    if(isPointInPoly(outer, { x:xc, y:yc}) ){

                        pixels[k] = newRgb[0];
                        pixels[k + 1] = newRgb[1];
                        pixels[k + 2] = newRgb[2];
                        pixels[k + 3] = sA;

                        for(var i =0; i < inner.length; i++) {
                            var hole = isPointInPoly(inner[i], { x:xc, y:yc});

                            if(hole) {
                                // repaint
                                pixels[k] = sR;
                                pixels[k + 1] = sG;
                                pixels[k + 2] = sB;
                                pixels[k + 3] = sA ;

                            }
                        }
                    }
                }

                /* set the canvas cache to the adjusted/reset image data */
                app.canvasCache = imagedata;

                ctx.clearRect(sx, sy, sw, sh);
                ctx.putImageData(dataCache, sx, sy);
            }

            // hide loading indicator
            app.settings.app.indicator.hide();


        },
        changeColor: function(polyGroup, newColor, canvas) {
            /**
             * Changes the color of the target section,
             *
             * @param   polyGroup Object    the section definition
             * @param   newColor  Array     the new color in [R, G, B] format
             * @param   canvas    Element   the canvas containing the image
             * @param   mode      String    the operation mode; transform (default), adjust
             *
             */

            // Define and init the variables
            var sx = 0, sy = 0, sw = 0, sh = 0, xc = 0, yc = 0;
            var k= 0;
            sw = canvas.width;
            sh = canvas.height;

            /* source canvas */
            var tempCan = document.createElement('canvas');
            var tempCtx = tempCan.getContext('2d');
            var tempData = tempCtx.createImageData(sw, sh);
            var tempPixels = tempData.data;

            /* cached imageData */
            var cachedPixels = app.canvasCache.data;

            /* destination */
            var ctx = canvas.getContext('2d');
            var imageData = ctx.getImageData(sx, sy, sw, sh);
            var pixels = imageData.data;

            /* composite mode */
            //ctx.globalCompositeOperation = "source-in";

            // Ensure that the new color is set
            if(newColor.length <= 0){
                app.error = "<h2>First Select A Color From The Palette.</h2>";
            }

            // Display any errors
            if(app.error){
                app.showDialog(app.error);
                return false;
            }

            // enter the loop with no errors
            if(!app.error) {
                // loop through the pixels
                for (k = 0; k < pixels.length; k += 4) {

                    // store the original pixel RGBA values
                    sR = cachedPixels[k];
                    sG = cachedPixels[k + 1];
                    sB = cachedPixels[k + 2];
                    sA = cachedPixels[k + 3];

                    /* fix the color */
                    var newRgb = app.colorFix(newColor);
                    // Calculate the x,y coordinates of the colors
                    xc = ( Math.floor(k / 4) % sw );
                    yc = ( Math.floor(Math.floor(k/sw) / 4) );

                    // Store the various wall sections
                    var poly = (polyGroup.poly)? polyGroup.poly : [{x:0,y:0}] ;
                    var outer = polyGroup.outer;
                    var inner = (polyGroup.inner) ? polyGroup.inner : [{x:0,y:0}] ;

                    tempPixels[k] = pixels[k];
                    tempPixels[k + 1] = pixels[k +1];
                    tempPixels[k + 2] = pixels[k +2];
                    tempPixels[k + 3] = sA

                    //
                    if(poly) {
                        // Loop through the polys
                        for(var i =0; i < poly.length; i++) {
                            // Check if the pixel is in the poly
                            var cord = isPointInPoly(poly[i], { x:xc, y:yc});
                            // If its in the poly
                            if (cord) {
                                // Transform the pixel
                                tempPixels[k] = newRgb[0];     /* New red color */
                                tempPixels[k + 1] = newRgb[1]; /* New green color */
                                tempPixels[k + 2] = newRgb[2]; /* New blue color */
                                tempPixels[k + 3] = sA; /* Maintain the Alpha channel */

                            } else {

                                // repaint
                                tempPixels[k] = pixels[k];
                                tempPixels[k + 1] = pixels[k +1];
                                tempPixels[k + 2] = pixels[k +2];
                                tempPixels[k + 3] = sA;

                            }

                        }

                    }

                    //
                    if(isPointInPoly(outer, { x:xc, y:yc}) ){

                        tempPixels[k] = newRgb[0];
                        tempPixels[k + 1] = newRgb[1];
                        tempPixels[k + 2] = newRgb[2];
                        tempPixels[k + 3] = sA;

                        for(var i =0; i < inner.length; i++) {
                            var hole = isPointInPoly(inner[i], { x:xc, y:yc});

                            if(hole) {
                                // repaint
                                tempPixels[k] = pixels[k];
                                tempPixels[k + 1] = pixels[k +1];
                                tempPixels[k + 2] = pixels[k +2];
                                tempPixels[k + 3] = sA;

                            }
                        }
                    }
                }
            }

            ctx.clearRect(sx, sy, sw, sh);
            ctx.putImageData(tempData, sx, sy);

	    },
        getPoints: function() {
            var url = app.settings.app.baseURL +  app.settings.app.dataDir + '/' + app.current + '.js';
            var points;

            var cur = app.settings.app.image.split('/')[app.settings.app.image.split('/').length -1];
            app.current = cur.substr(0, cur.indexOf('.'));

            $data = $.ajax({
                async: false,
                global: false,
                url: 'data.php',
                data: {
                    r: 'js',
                    f:  app.current + '.js'
                },
                success: function(data){
                    points = $.parseJSON(data);
                },
                error: function(data){
                    app.error = error;
                }
            });

            return points;
        },
        getPolys: function() {
            var url = app.settings.app.baseURL +  app.settings.app.polyDir + '/' + app.current + '.js';
            var polys;

            var cur = app.settings.app.image.split('/')[app.settings.app.image.split('/').length -1];
            app.current = cur.substr(0, cur.indexOf('.'));

            $data = $.ajax({
                async: false,
                global: false,
                url: 'data.php',
                data: {
                    r: 'poly',
                    f:  app.current + '.js'
                },
                success: function(data){
                    polys = $.parseJSON(data);
                },
                error: function(data){
                    app.error = error;
                }
            });

            return polys;
        },
        saveImageData: function(data) {
            /**
             * Save the image on the stage
             *
             * @type {*}
             */
            var config = app.settings.app;
            var ret = '';
            config.indicator.show();
            $data = $.ajax({
                url: config.palletteScript,
                type: 'post',
                data: {
                    r: 'svimg',
                    d: data
                },
                async: true,
                cache: false,
                success: function(res) {

                    var ret = '';

                    setTimeout(function() {

                        ret = (!res) ? 'Failed to load' : res;

                        app.shareURL = ret;

                        //console.log( app.shareURL);

                    }, 2000);

                },
                error: function(res) {

                    app.error += "Failed to save image";

                }

            });

            config.indicator.hide();

        },
        saveToProfile: function(img, filename, colors) {
            /**
             * Save the image on the stage to the profile page
             *
             * @type {*}
             */
            var config = app.settings.app;
            var ret = '';
            config.indicator.show();
            $data = $.ajax({
                url: config.palletteScript,
                type: 'post',
                data: {
                    r: 'svprof',
                    d: img,
                    f: filename,
                    c: colors
                },
                context: document.body,
                cache: true,
                success: function(res) {
                    var dat = (!res) ? 'Failed to load' : res;
                    ret = dat;
                    config.indicator.hide();
                    /* setup and show the dialog */
                    // TODO: Add profile link to footer
                    $('.modal-header').text('Success');
                    $('.modal-footer').html('<a href="profile.php" class="btn btn-primary">Profile</a><button type="button" class="btn btn-default" data-dismiss="modal">close</button>');
                    app.showDialog('<h2>The photo has been saved to your profile.</h2>');

                },
                error: function(res) {
                    app.error += "Failed to save image";
                }
            });

            return ret;
        },
        shareImage: function(image, network) {
            /**
             * Share the saved image on the selected social media network
             *
             * @type {string}
             * @private
             *
             */

            var _FB = "https://www.facebook.com/sharer/sharer.php?u="; // facebook
            var _TW = "https://twitter.com/share?url="; // twitter
            var _IN = "https://www.linkedin.com/shareArticle?mini=true&url="; // linkedin
            var _GP = "https://plus.google.com/share?url=";  // google +

            var _image_url = image;
            var _share_title = 'Share Page';
            var _share_url = '';

            if(network == 'facebook') { _share_url = _FB + encodeURI(_image_url); _share_title = "Share via Facebook"; };
            if(network == 'twitter') { _share_url = _TW + encodeURI(_image_url); _share_title = "Share via Twitter"; };
            if(network == 'linkedin') { _share_url = _IN + encodeURI(_image_url); _share_title = "Share via Linkedin"; };
            if(network == 'google') { _share_url = _GP + encodeURI(_image_url); _share_title = "Share via Google Plus";};

            window.open(_share_url, _share_title, 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=600');

        },
        saveToPdf: function(data) {
            /**
             * Save the image data to pdf and download it
             *
             * @param data
             */

            jsPDF.API.render = function(data){
                /* header */
                this.addImage(app.logo, 'JPEG', 10, 15);
                this.setFontSize(25);
                this.text(65, 26, "Crown Color Visualizer");

                /* image */
                this.addImage(data, 'JPEG', 10, 40);

                /* watermark */
                this.setFontSize(8);
                this.setFontStyle('bold');
                this.setTextColor(250,248,248);
                this.text(15, 45, "http://www.crownit.co.ke");

                /* */
                var x1 = 183;
                var x2 = 188;

                /* username */
                this.setFontSize(10);
                this.setTextColor(15,15,12);
                this.text(100, 180, ("username: " + app.username));

                /* date */
                var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth();
                var curr_year = d.getFullYear();
                var date = curr_date + "-" + m_names[curr_month] + "-" + curr_year;
                this.text(180, 180, date);


                if ( app.stageDetails.length != 0 ) {

                    /* colors */
                    this.setFontSize(10);
                    this.setTextColor(15,15,12);
                    this.text(10, 180, "Color Palette");

                    for (var i = 0; i < app.stageDetails.length; i++) {

                        var key = app.stageDetails[i];

                        var data = app.stageDetails[key];

                        var end = 'cont';

                        var name = data.name;
                        var code = data.code;
                        var color = data.color;

                        /* one */
                        this.setDrawColor(0);
                        this.setFillColor( (color[0] * 1), (color[1] * 1), (color[2] * 1));
                        this.rect(10, (x1 * 1), 10, 10, 'F');
                        this.text(25, (x2 * 1), (name + " "));
                        this.text(58, (x2 * 1), (code + " "));

                        x1 = ( (x1 * 1) + 11);
                        x2 = ( (x1 * 1) + 5);

                        if (i == app.stageDetails.length) {

                            end = 'end';

                        } else {

                            end  = 'cont';
                        }

                    }

                }


                /* footer */
                this.addImage(app.isologo,'jpeg',15,270);
                this.setFontSize(12);
                this.setTextColor(150,150,65);
                this.text(35, 276, "Crown Paints Kenya Ltd © All Rights Reserved");

                /* set file properties */
                this.setProperties({
                    title: 'Crown Color Visualizer - Image Export',
                    subject: 'Image Export',
                    author: 'crownit.co.ke',
                    keywords: 'Powered by My-IT-Provider LTD - http://www.my-it-provider.com',
                    creator: 'Crown Color Visualizer'
                });

            };

            var pdfdoc = new jsPDF();
            pdfdoc.render(data);
            pdfdoc.save('CrownColorVisualizer-ImageOutput.pdf');

        },
		loadIt: function(what) {
			var config = app.settings.app;
			config.indicator.show();
			$data = $.ajax({
				url: config.palletteScript,
				data: {
					r: what
				},
				context: document.body,
				cache: true,
				success: function(res){
					var dat = (!res) ? 'Failed to load' : res;
					app.panel.getContentPane().html(dat);
					config.boxa.miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
                    config.boxi.miniTip({fadeIn: 500, delay: 0, fadeOut: 500 });
					config.indicator.hide();

                    app.panel.reinitialise();
				},
				error: function(res){
					app.panel.getContentPane().html(
						'Some items have failed to load,' + 
						'please try refreshing the page [F5]');
                }
			});
			app.panel.reinitialise();
        },
        removePaint: function() {
            /**
             *  Restore the image on the stage
             *
             * @type {*}
             */
            var img = new Image();
            img.src = app.imgCache;
            app.imageLayer.setAttr('image', img);

            app.imageLayer.draw();

        },
		/** event bindings **/
        bindEvents: function() {

            /* color selector */
            $('.boxd').on('click', function(e) {

                e.preventDefault();

                var index = $(this).attr('data-palette');

                $('.panel').find('div[id=shade_cont]').each(function() {

                    $(this).hide();

                });

                var shade_conts = $('.panel').find('div[id=shade_cont]');

                if (index == '0-15') {

                    $(shade_conts[0]).show();

                } else if (index == '16-35') {

                    $(shade_conts[1]).show();

                } else if (index == '36-70') {

                    $(shade_conts[2]).show();

                } else if (index == '71-162') {

                    $(shade_conts[3]).show();

                } else if (index == '163-252') {

                    $(shade_conts[4]).show();

                } else if (index == '253-268') {

                    $(shade_conts[5]).show();

                } else if (index == '269-279') {

                    $(shade_conts[6]).show();

                } else if (index == '280-318') {

                    $(shade_conts[7]).show();

                } else if (index == '319-360') {

                    $(shade_conts[8]).show();

                } else if (index == 'white') {

                    $(shade_conts[9]).show();

                } else if (index == 'cream') {

                    $(shade_conts[10]).show();

                } else if (index == 'gray') {

                    $(shade_conts[11]).show();

                }else if (index == 'black') {

                    $(shade_conts[12]).show();

                }

                
                app.panel.reinitialise();

            });

            app.stage.on('click touchend', function(evt) {

                var shape = evt.targetNode;

                if (shape) {

                    var key = shape.getAttrs().key;

                    if(!key)
                    {
                        app.showDialog('<h2>You cannot transform this section.</h2>');
                        return false;
                    }

                    /* set stage details */
                    var det = key; //console.log(key);
                    var index = app.stageDetails.indexOf( det );

                    if( index == -1)
                    {
                        app.stageDetails.push( det.toString() );
                        app.stageDetails[det] = {};
                        app.stageDetails[det].name = app.colorData.name;
                        app.stageDetails[det].code = app.colorData.code;
                        app.stageDetails[det].color = app.colorData.color;
                    }
                    else
                    {
                        app.stageDetails.splice(index, 1);
                        app.stageDetails.push( det );
                        app.stageDetails[det] = {};
                        app.stageDetails[det].name = app.colorData.name;
                        app.stageDetails[det].code = app.colorData.code;
                        app.stageDetails[det].color = app.colorData.color;
                    }

                    if(app.adjusted == 'ok')
                    {
                        var polyGroup = app.polygons[key];
                        var colorStr = app.colorData.name + ' - ' + app.colorData.code;

                        // Track the color
                        _gaq.push(['_trackEvent', 'Colors', colorStr, app.current]);

                        /* show indicator */
                        app.settings.app.indicator.show();

                        /* delay blocking function */
                        setTimeout(function() {

                            /* blocking function */
                            app.changeColor(polyGroup, app.request.newColor, app.canvas);

                            /* hide indicator */
                            app.settings.app.indicator.hide();

                        }, 500);

                    }
                    else
                    {

                        return false;

                    }

                }

            });
            
            app.shapesLayer.on('mouseover', function(evt) {
                var shape = evt.targetNode;
                if (shape) {
                    shape.parent.setOpacity(1);
                    app.index = shape.parent._id;
                    app.shapesLayer.draw();
                }
            });

            app.shapesLayer.on('mouseout', function(evt) {
                var shape = evt.targetNode;
                if (shape) {
                    shape.parent.setOpacity(0);
                    app.shapesLayer.draw();
                    //app.tooltip.hide();
                    app.tooltipLayer.draw();
                }
            });

            /**
             * Reset the stage
             *
             */
            $('.action-refresh').on('click', function(){

                app.stageDetails = [];
                app.removePaint();

            });

            /**
             * Share on social media
             *
             */
            $('.share-action').live('click', function() {
                /* show loading */
                app.settings.app.indicator.show();

                var image, img, network;

                network = $(this).attr('id');

                try {
                    img = app.canvas.toDataURL('image/jpeg');

                    app.saveImageData(img);

                } catch(e) {

                    console.log('Error SM: ' + e );

                }

                /* hide loading */
                app.settings.app.indicator.hide();

                app.shareImage(app.shareURL, network);

            });

            /**
             * Save the image to pdf
             *
             */
            $('.action-print').on('click', function(){
                /* check for active session */
                if (app.username != '') { //console.log('user');
                    var image, img;
                    image = new Image();
                    try {
                        img = app.canvas.toDataURL("image/jpeg", 1);
                    } catch(e) {
                        console.log(e);
                    };
                    app.saveToPdf(img);
                } else {
                    /* login first */
                    $('a#login').trigger('click');
                    return false
                }
            });

            /**
             * Save the image to the user's profile
             *
             */
            $('.action-save-profile').live('click', function(){
                var image, img ;
                var filename = '';
                var colors = '';

                image = new Image();
                try {
                    img = app.canvas.toDataURL("image/jpeg", 1);

                } catch(e) {

                    console.log(e);

                }

                for(var i = 0; i < app.stageDetails.length; i++)
                {
                    key = app.stageDetails[i];
                    colors += key + ', ' + app.stageDetails[key].name + ', ' + app.stageDetails[key].code + ';' ;

                }
                key = '';

                filename = app.current + '.png';

                app.saveToProfile(img, filename, colors);

            });

            // TODO: merge the two bindings below
            app.settings.app.box.live('click', function(evt){

                evt.preventDefault();
                var rgbNC = $(this).css('backgroundColor');

                if(rgbNC == 'rgba(0, 0, 0, 0)'){

                }else {
                    // set the newColor variable
                    ncRGB = rgbNC.substring(4, rgbNC.indexOf(')')).split(',');
                    app.request.newColor = ncRGB;
                    // update recent colors
                    var name = $(this).attr('data-name');
                    var code = $(this).attr('data-code');
                    var colObj = [ncRGB, name, code];

                    // determine the main component, red, green, blue
                    var r = ncRGB[0];
                    var g = ncRGB[1];
                    var b = ncRGB[2];
                    var hue = '', reg = '';
                    if ((r >= g) && (g >= b)) {
                        var frac = ( (g - b) / (r - b) );
                        hue = ( 60 * frac );
                        reg = 'Red - Yellow';
                    } else if ((g > r) && (r >= b)) {
                        var frac = 2 - ( (r - b) / (g - b) );
                        hue = 60 * frac;
                        reg = 'Yellow - Green';
                    } else if ((g >= b) && (b > r)) {
                        var frac = 2 + ( (b - r) / (g - r) );
                        hue = 60 * frac;
                        reg = 'Green - Cyan';
                    } else if ((b > g) && (g > r)) {
                        var frac = 4 - ( (g - r) / (b - r) );
                        hue = 60 * frac;
                        reg = 'Cyan - Blue';
                    } else if ((b > r) && (r >= g)) {
                        var frac = 4 + ( (r - g) / (b - g) );
                        hue = 60 * frac;
                        reg = 'Blue - Magenta';
                    } else if ((r >= b) && (b > g)) {
                        var frac = 6 - ( (b - g) / (r - g) );
                        hue = 60 * frac;
                        reg = 'Magenta - red';
                    } else {
                        hue = 'None';
                        reg = 'None';
                    }

                    //log color components
                    var sum = (ncRGB[0] * 1) + (ncRGB[1] * 1) + (ncRGB[2] * 1);
                    //console.log(sum + ' - ' + ncRGB[0] + ncRGB[1] + ncRGB[2] + ' - ' + reg +  ' - ' + hue);


                    app.updateRecentColors(colObj);
                    // set darker and lighter shades
                    var prevCol = $(this).prev().css('backgroundColor');
                    var nextCol = $(this).next().css('backgroundColor')
                    $('#dropper-min-dark').css('backgroundColor', prevCol);
                    $('#dropper-min-light').css('backgroundColor', nextCol);
                    //console.log(nextCol);

                    // change the dropper color and details
                    var code = $(this).attr('data-code');
                    var name = $(this).attr('data-name');
                    app.colorData.name = name;
                    app.colorData.code = code;
                    app.colorData.color = [ncRGB[0], ncRGB[1], ncRGB[2]];
                    $('.sample-code').html(code);
                    $('.sample-color').html(name);
                    app.settings.app.dropper.css('backgroundColor', ('#' + app.rgbToHex(ncRGB[0], ncRGB[1], ncRGB[2]) ));//app.request.newColor));
                    app.error = null;

                }
            });

            $('#box').live('click', function(evt) {

                evt.preventDefault();

                var rgbNC = $(this).css('backgroundColor');

                if(rgbNC == 'rgba(0, 0, 0, 0)'){

                }else {
                    // set the newColor variable
                    ncRGB = rgbNC.substring(4, rgbNC.indexOf(')')).split(',');
                    app.request.newColor = ncRGB;
                    // update recent colors
                    //var nme = $(this).attr('data-name');
                    //var cde = $(this).attr('data-code');
                    //var colObj = [ncRGB, nme, cde];
                    //app.updateRecentColors(colObj);
                    // change the dropper color and details

                    var code = $(this).attr('data-code');
                    var name = $(this).attr('data-name');
                    $('.sample-code').html(code);
                    $('.sample-color').html(name);
                    app.settings.app.dropper.css('backgroundColor', ('#' + app.rgbToHex(ncRGB[0], ncRGB[1], ncRGB[2]) ));
                    app.error = null;
                }

            });

            return app;
        },
		/** Initialisation functions **/
        setupPanel: function() {
            var pn = $(app.settings.app.panel);
            pn.jScrollPane(app.settings.app.jscrollpane);
            app.panel = pn.data('jsp');

            return app;
        },
		setupAjax: function() {
            $.ajaxSetup({
               cache: true,
               async:  false
            });

            return app;
		},
		setupToolTip: function() {
            /* tooltip

            var tip = new Kinetic.Label({
                opacity: 0.75,
                visible: false,
                listening: false
            });
            tip.add(new Kinetic.Tag({
                fill: 'black',
                pointerDirection: 'down',
                pointerWidth: 10,
                pointerHeight: 10,
                lineJoin: 'round',
                shadowColor: 'black',
                shadowBlur: 10,
                shadowOffset: 10,
                shadowOpacity: 0.5
            }));
            tip.add(new Kinetic.Text({
                text: '',
                fontFamily: 'Calibri',
                    fontSize: 18,
                    padding: 5,
                    fill: 'white'
                }));

                app.tooltip =  tip;
                app.tooltipLayer.add(app.tooltip);
                //app.stage.add(app.tooltipLayer);

            return app; */
		},
        setupPoints: function() {
            var areas = app.getPoints();

            // draw areas
            for(var key in areas) {

                var area = areas[key];

                var group = new Kinetic.Group({
                    opacity: 0,
                    listening: true
                });

                for(var i = 0; i < area.points.length; i++) {

                    var shape = new Kinetic.Polygon({
                        points: area.points[i],
                        stroke: area.stroke,
                        strokeWidth: area.strokeWidth,
                        opacity: area.opacity,
                        key: key
                    });

                    group.add(shape);
                }
                app.shapesLayer.add(group);
            }

            return app;
        },
        setupPolys: function() {
          var poly = app.getPolys();

          app.polygons = poly;

          return app;
        },
		setupStage: function(config) {
			app.stage = new Kinetic.Stage(config);

            return app;

		},
        setupImage: function() {
            
            var config = app.settings.app;
            config.indicator.show();
            $(app.room).on('load', function() {

                //console.log('starting');

                app.imageLayer = new Kinetic.Image({
                    x: 0,
                    y: 0,
                    width: 740,
                    height: 500,
                    image: app.room
                });

                app.layer.add(app.imageLayer);

                app.stage.add(app.layer);
                app.stage.add(app.shapesLayer);
                app.stage.add(app.tooltipLayer);

                app.canvasCache = app.imageLayer.getCanvas();
				app.canvas = app.imageLayer.getCanvas();				

                /* show indicator */
                app.settings.app.indicator.show();

                /* delay blocking function */
                setTimeout(function() {

                    /* blocking function */
					$('.action-clear').on('click', function() { app.adjustImage(); });
					
                    /* hide indicator */
                    app.settings.app.indicator.hide();

                    /* variation, render vs palette */
                    var diag = 'Please note that light conditions in the image might cause some slight variation between the color selected in the palette and the rendering. Start by clicking this button in the right panel <div class="btn-group"><a class="btn action-clear" title="Remove Colors From Image"><i class="icon-cog"></i></a></div>';
                    app.showDialog(diag);

                }, 500);

            });

            app.room.src = app.settings.app.image;

            app.changeThePalette();

            return app;

        },
		initialise: function(settings) {
			app.settings = settings;
            app.debug = settings.app.debug;
            app.username = settings.app.username;

			app.setupPanel().
            setupStage(app.settings.kinetic).

            setupPoints().
            setupPolys().
            setupImage().
            plotThePoints().

            bindEvents();
		}
			
	};
	
    // Make the app module global
    (!window.app) ? window.app = app : window.app;
	
})();
