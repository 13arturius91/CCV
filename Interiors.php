﻿<?php
    require 'includes/header.php';
?>
        
        
        <div class="black-background">
            <div class="container">
            <div class="header-title-content text-left box_bleck" >				
							<div style="padding:5px;">	
							
							<a href="javascript:history.back()" class="btn btn-mini">Back</a>
							
							<a class="btn btn-mini" href="index.php">Home</a>
													  
							<a class="btn btn-mini" id="gallery">Color Inspiration</a>

                            <a class="btn btn-mini btn-info text-white text-extrabold" href="ccv.php">Launch Visualizer</a>
							 
							<!--<a class="btn btn-mini" id="link">Video Tutorial</a>-->
							 
							<?php

                              $page = (isset($_SESSION['isadmin'])) ? 'admin.php' : 'profile.php';
                              $name = (isset($_SESSION['isadmin'])) ? 'admin' : 'profile';

                              echo (isset($_SESSION['username'])) ? '<a class="btn btn-mini" style="float:right;" href="' . $page . '">' . $name . '</a>' : '<a class="btn btn-mini btn-success text-extrabold" style="float:right;" id="login">JOIN OUR NETWORK</a>';

                            ?>  
							 						
							<h1 style="font-size:16px; font-family: 'Open Sans' !important; color:white; padding-left:400px; font-weight:bold;">INTERIOR SCENES</h1>
							
								
							</div>		
              </div>
            <div id="step1" class="tf-header-slider">
                <div class="flexslider">
                  <ul class="slides">
                    <li>
                        <a href="#"> <img src="images/img/slide5.png" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Living rooms</h2> 
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a href="#"><img src="images/img/slide6.png" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Bedrooms</h2>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a href="#"><img src="images/img/slide7.png" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Bathrooms</h2>

                            </div>
                        </div>
                        
                    </li>
                   
                  
                    
                   
                  </ul>
                </div>
            </div>
            </div>
        </div>
        
<!-- middle -->
        <div class="white-background">
            <div class="container ">
                <div id="step2" class="our-work">
                  <div class="content">
                        <div class="row">
                            <div class="col col_1_4"><a href="Livingrooms.php">
                            <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                <img src="images/tuts/ico1.jpg" alt="" />
                                <h2 class="text-bold text-white">Living Rooms</h2>
                                <p>Transform your living room</p>
                                </div>
                            </a></div>
                                    
                                    <div class="col col_1_4"><a href="Bedrooms.php">
                                        <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                          <img src="images/tuts/ico2.jpg" alt="" />
                                          <h2 class="text-bold text-white">Bedrooms</h2>
                                        <p>Transform your Bedroom</p>
                                        </div>
                                    </a></div>
                                    
                                    <div class="col col_1_4"><a href="Washrooms.php">
                                        <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                        <img src="images/tuts/ico3.jpg" alt="" />
                                        <h2 class="text-bold text-white">Bathrooms</h2>
                                        <p>Transform your Bathroom</p>
                                        </div>
                                    </a></div>
                                    
                                    <div class="col col_1_4"><a href="Kitchens.php">
                                        <div class="inner"><h1 style="font-size:14px; color:#FFF; padding-left:30px;"> click to view</h1>
                                        <img src="images/tuts/ico5.jpg" alt="" />
                                        <h2 class="text-bold text-white">Kitchens</h2>
                                        <p>Transform your Kitchen</p>
                                        </div>
                                    </a></div>
                                    
                                                                        
                      </div>
                                <div class="clear"></div>
                            </div>
                        </div>
        <?php
                    require 'includes/footer.php';
        ?>