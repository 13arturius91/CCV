<?php
/**
 * The Color Name Array
 */
$two = getArrayTwo();
/**
 * The color code array
 */
$three = getArrayThree();
/**
 * The color palettes
 */
$palettes = array("palette-1.xml", "palette-2.xml", "palette-3.xml", "palette-4.xml", "palette-5.xml", "palette-6.xml", "palette-7.xml");
foreach ($palettes as $pal) {

    $one = getPalette($pal);
    $one = array_reverse($one);

    $num = count($one);
    /** **/
    if ($pal != 'palette-5.xml') { $display = 'none'; } else { $display = 'inline'; }
    echo "<div id='shade_cont' class='$pal' style='float: lef; display: $display;'>";
    for ($i = 0; $i < count($one); $i++) {

        $nm = array_keys($two, $one[$i]);
        $name = $three[ $nm[0] ];
        $col = $one[$i];

        $open = strpos($name, '(');
        $close = strpos($name, ')');

        $code = substr($name, $open);
        $codea = str_replace('(', '', $code);

        // color name
        $thing = substr($name,0, $open );
        $thing = str_replace(' ', '-', trim($thing) );
        // color
        $color = '#' . substr($col, 3, 9);
        $rgb = hex2rgb($color);
        $rgbStr = $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'];
        // code
        $codeb = str_replace(')', '', $codea);

        // title
        $title = stripcslashes($thing) . ' ' . $codeb;

        // output
        $output = '<a data-name="' . stripcslashes($thing) . '" data-code="' . $codeb  .'" href="#" class="box" style="background: rgb(' . $rgbStr . '); color: black !important; text-shadow: 1px 0px 2px white !important;">'.$title.'</a>';

        echo $output;
    }
    echo "<div style='clear:both;'></div></div>";
}
exit;

function getArrayThree(){

    $swatchLib = simplexml_load_file('palette-orig.xml');

    $num = count($swatchLib);

    $array_three = array();

    foreach($swatchLib as $swatch) {

         $name = $swatch['Name'];

         array_push($array_three, "$name" );

    }
    return $array_three;
}

function getArrayTwo(){

    $swatchLib = simplexml_load_file('palette-orig.xml');

    $num = count($swatchLib);

    $array_two = array();

    foreach($swatchLib as $swatch) {

         $kol = $swatch['Color'];

         array_push($array_two, "$kol" );

    }
    return $array_two;
}

function getArrayOne() {

    $swatchLib = simplexml_load_file('palette-2.xml');

    $num = count($swatchLib);

    $array_one = array();
    //echo "{";
    foreach($swatchLib as $swatch) {
         /*
         $name = $swatch['Name'];
         $one = str_replace('R','',$name);
         $two = str_replace('G','',$one);
         $three = str_replace('B','',$two);
         echo "<div style='border:margin:2px; float:left; width:25px; height:25px; 1px solid #000;padding:4px;background:rgb($three);'></div>";
         */
         $kol = $swatch['Color'];

         array_push($array_one, $swatch['Color']);

    }

    return $array_one;
}
/* palette-brown = palette-1 */
/**/
/* palette-red = palette-5 */
/* palette- = palette-4 */
/* palette-gray = palette-3 */
/* palette- = palette-6 */
/**/
function getPalette($which) {

    $swatchLib = simplexml_load_file($which);

    $num = count($swatchLib);

    $array_palette = array();

    foreach ($swatchLib as $swatch) {
        /*
        $name = $swatch['Name'];
        $one = str_replace('R','',$name);
        $two = str_replace('G','',$one);
        $three = str_replace('B','',$two);
        */
        $kol = $swatch['Color'];

        array_push($array_palette, $swatch['Color']);

    }

    return $array_palette;

}

function hex2rgb( $colour ) {
    if ( $colour[0] == '#' ) {
        $colour = substr( $colour, 1 );
    }
    if ( strlen( $colour ) == 6 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
    } elseif ( strlen( $colour ) == 3 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
    } else {
        return false;
    }
    $r = hexdec( $r );
    $g = hexdec( $g );
    $b = hexdec( $b );

    return array( 'r' => $r, 'g' => $g, 'b' => $b );

}
exit;

?>