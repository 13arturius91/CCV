﻿<?php
    require 'includes/header.php';
?>
<title>Textures</title>

        
        
        <div class="black-background">
            <div class="container">
            <div class="tf-header-slider">
                <div class="flexslider">
                  <ul class="slides">
                    <li>
                        <a href="#"> <img src="images/img/slide9.png" alt="" /></a>
                      <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Quartz</h2> 
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a href="#"><img src="images/img/slide10.png" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Rough & Tough</h2>
                            </div>
                        </div>
                    </li>
                    
                    <li>
                        <a href="#"><img src="images/img/slide11.png" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Metallica</h2>
                            </div>
                        </div>
                    </li>
                   
                    <li>
                        <a href="#"><img src="images/img/slide12.png" alt="" /></a>
                        <div class="details">
                            <div class="text-details">
                                <h2 class="text-bold text-white">Marbles</h2>
                            </div>
                        </div>
                    </li>
                  
                    
                   
                  </ul>
                </div>
            </div>
            </div>
        </div>
        
<!-- middle -->
        <div class="white-background">
            <div class="container ">
                <div class="our-work">
                  <div class="content">
                        <div class="row">
                            <div class="col col_1_4"><a href="#">
                            <div class="inner">
                                <img src="images/tuts/ico1.jpg" alt="" />
                                <h2 class="text-bold text-white">Metallica</h2>
                                <p>
                                Transform your Interior</p>
                            </div>
                            </a></div>
                                    
                                    <div class="col col_1_4"><a href="#">
                                        <div class="inner">
                                          <img src="images/tuts/ico2.jpg" alt="" />
                                          <h2 class="text-bold text-white">Variations</h2>
                                        <p>Transform your Exterior</p>
                                        </div>
                                    </a></div>
                                    
                                    <div class="col col_1_4"><a href="#">
                                        <div class="inner">
                                        <img src="images/tuts/ico3.jpg" alt="" />
                                        <h2 class="text-bold text-white">Undercoats</h2>
                                        <p>View Textures Gallery</p>
                                        </div>
                                    </a></div>
                                    
                                    <div class="col col_1_4"><a href="#">
                            <div class="inner">
                                <img src="images/tuts/ico4.jpg" alt="" />
                                <h2 class="text-bold text-white">Marbles</h2>
                                <p>
                                Transform your Office</p>
                            </div>
                            </a></div>
                                    
                      </div>
                                <div class="clear"></div>
                            </div>
                        </div>
        <?php
            require 'includes/footer.php';
        ?>