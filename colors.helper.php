<?php

$req = isset($_GET['do']) ? $_GET['do'] : 'null';
$img = isset($_POST['image']) ? $_POST['image'] : 'null';

if ($req == 'update') {

    showUpdate();
    exit;
}

$file = 'color_data';

if ($img != 'null') {

    $image_name = $img;
    $dat = '';

    foreach ($_REQUEST as $k => $v) {

        $dat .= $v;

    }

    file_put_contents($file, ($dat  . "\n"), FILE_APPEND | LOCK_EX);

    exit;

}

/* get the images */
if ($handle = opendir('assets')) {

    $files = array();

    while (false !== ($entry = readdir($handle))) {

        if (strpos($entry, '-min.') == false) {

            $file = 'assets/' . $entry;

            array_push($files, $file);

        }

    }

    closedir($handle);

}

/*
$select = '<select id="images">';
$i = 0;
while (@$files[$i]) {

    $select .= '<option value="assets/' . $files[$i] . '" data-image="assets/' . $files[$i] . '">' . $files[$i] . '</option>';

    $i++;

}

$select .= '</select>';

echo $select;
*/

$images = array();
$i = 0;
while (@$files[$i]) {

    $images[] = 'assets/' . $files[$i];

    $i++;

}


$data = json_encode($files);

?>

<script src="/libs/jquery.min.js"></script>
<script src="colors.json.js"></script>

<script>
$(function() {

    /* build color array */
    var color_data = [];
    var color_hex = [];
    var color_json = colors.payload;

    for (var i = 0; i < color_json.length; i++) {

        var str = color_json[i].name + ' ' + color_json[i].code;
        color_data.push(str);

        var hx = color_json[i].hex;
        color_hex.push(hx);

    }

    /* convert hex number to decimal */
    function hex_to_dec(c,start) {

        var c1 = get_dec(c.substring(start,start+1));
        var c2 = get_dec(c.substring(start+1,start+2));
        return (c1*16)+c2*1;

    }

    function get_dec(hex) {
        if(hex=='F')return 15; if(hex=='E')return 14; if(hex=='D')return 13; if(hex=='C')return 12;
        if(hex=='B')return 11; if(hex=='A')return 10; return hex;
    }

    /* convert decimal to hex */
    function d2h(d) {

        return (+d).toString(16).toUpperCase();

    }

    function colorMatch( color_to_match ) {

        var c1 = color_to_match;
        var r1, g1, b1, r2, g2, b2;
        var color_match = [];

        r1 = hex_to_dec(c1, 0); g1 = hex_to_dec(c1, 2); b1 = hex_to_dec(c1, 4);

        for (var i = 0; i < color_hex.length; i++ ) {

            var c2 = color_hex[i];
            r2 = hex_to_dec(c2, 0); g2 = hex_to_dec(c2, 2); b2 = hex_to_dec(c2, 4);

            var diff1, diff2, diff3;

            diff1 = Math.abs(r1-r2); diff2 = Math.abs(g1-g2); diff3 = Math.abs(b1-b2);

            var result = Math.round( 100 - (diff1+diff2+diff3)/(3*255) * 100 );

            color_match.push(result);

        }

        var index = getMax(color_match);

        /* var index = color_match.indexOf(Math.max.apply(Math, color_match)); */

        console.log('>> ', index, result, color_match);

        return index;

    }

    function getMax( array ) {

        var ind = Math.max.apply( Math, array );

        return array.indexOf(ind);

    }

    console.log(color_hex);

    var images = <?php echo $data  ?>;
    var counter = 0;
    var current = images[counter];
    var canvas = document.getElementById('stage');
    var context = canvas.getContext('2d');


    $(canvas).click(function(e) {

        var canvasOffset = $(canvas).offset();
        var canvasX = Math.floor(e.pageX - canvasOffset.left);
        var canvasY = Math.floor(e.pageY - canvasOffset.top);

        var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
        var pixels = imageData.data;
        var pixelRedIndex = ((canvasY - 1) * (imageData.width * 4)) + ((canvasX -1) * 4);
        var pixelcolor = "rgb(" + pixels[pixelRedIndex] + ',' + pixels[pixelRedIndex + 1] + ',' + pixels[pixelRedIndex + 2] + ")";


        console.log(pixelcolor);

        /* hex color */
        hexrgb = rgbToHex(pixels[pixelRedIndex], pixels[pixelRedIndex + 1], pixels[pixelRedIndex + 2]).toUpperCase();

        var ind = colorMatch(hexrgb);

        var stri = color_data[ind];
        var hex = '#'+color_hex[ind].trim();

        console.log(stri);

        var string = "<li><div class='bix' style='background:" + pixelcolor + ";' ></div><div class='bix' style='background:"+hex + ";' ></div>" + stri + " "
            + "<input name='item-" + Math.round(Math.random() * 100 + Math.random() * 100) + "' type='hidden' value='" + stri + hex + "'> <div class='clear'></div> </li>";

        $('ul#data').append(string);

    });

    /* rgb to hex */
    function rgbToHex(r, g, b) {
        if (r > 255 || g > 255 || b > 255)
            throw "Invalid Color Component";

        return ("000000" + ((r << 16) | (g << 8) | b).toString(16)).slice(-6);
    }

    /* next button */
    $(document).delegate('button#next', 'click', function() {

        var num = $('ul#data > li');

        if (num.length > 0) {

            saveData();

            counter++
            current = images[counter];

            loadImage(current);

            console.log(current);

        } else {

            alert('Colors Kwanza');

        }

    });

    /* load image */
    function loadImage(image) {

        var img = new Image();

        $(img).load(function() {

            context.drawImage(img, 0, 0, 280, 189);

        });

        img.src = image;

        $('input#data-name').val(image);
        $('span#data-image').empty().html(image);
        $('ul#data').empty();

    }

    /* save data */
    function saveData() {
        console.log('saving data ...');

        var req = $('form#data-form').serialize();

        $.ajax({
            url: "colors.helper.php",
            data: req,
            type: "POST",
            success: function(data) {

            },
            error: function(data) {

            }
        });

    }

    /* remove item */
    $(document).delegate('ul#data li', 'click', function() {

        $(this).remove();

    });

    loadImage(current);

});
</script>

<style>
    ul#data {
        width: 300px;
        margin: 5px 0 5px 0;
        padding: 0;
        list-style: none;
    }
    button {
        margin: 0px;
    }
    .bix {
        height: 25px; width: 25px;
        float: left;
        margin: 0 2px 3px 0;
    }
    iframe {
        background: #ffffff;
    }
    .clear {
        clear: both;
    }
</style>

<div style="width:295px; float: left;">

    <button id="next">Next &raquo;</button> <br><br> <span id="data-image"></span>

    <canvas id="stage" width="280" height="189"></canvas>

    <form id="data-form" style="margin: 0px; padding: 0px;">
        <input type="hidden" id="data-name" name="image" value="">
        <ul id="data">
        </ul>
    </form>

    <button id="next">Next &raquo;</button>

</div>

<div style="float: left; width: ; border: none; margin-left: 5px; background: #333; padding:0;">

    <iframe height="95%" width="100%" style="margin:5px; width: 330px; padding: 0;" src="http://localhost/ccv/colors.helper.php?do=update"></iframe>

</div>

<?php

/* update preview iframe */
function showUpdate() {

    echo "updating ...<br>";
    echo time();

}

?>